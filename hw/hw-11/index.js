/**
1. Що таке події в JavaScript і для чого вони використовуються?

Коли користувач якось взаємодіє з нашою програмою, відбувається подія. 
Всі події можна відслідковувати та обробляти. Для реагування на події 
ми використовуємо функції-обробники. Це робить програму динамічною.


2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.

click
mouseover / mouseout
mousedown / mouseup
contextmenu
mousemove


3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?

contextmenu - клік на елемент правою кнопкою миші. Це меню залежить від операційної системи
та місця куди клікнули. Можна створити нову папку, відкрити параметри ...

*/


/**
1. Додати новий абзац по кліку на кнопку:
По кліку на кнопку <button id="btn-click">Click Me</button>, 
створіть новий елемент <p> з текстом "New Paragraph" і додайте 
його до розділу <section id="content">


2. Додати новий елемент форми із атрибутами:
Створіть кнопку з id "btn-input-create", додайте її на сторінку 
в section перед footer.
По кліку на створену кнопку, створіть новий елемент <input> і 
додайте до нього власні атрибути, наприклад, type, placeholder, 
і name. та додайте його під кнопкою. 
*/

const clickMeBtn = document.querySelector('#btn-click');
const mainSection = document.querySelector('#content');

clickMeBtn.addEventListener('click', () => {
    clickMeBtn.insertAdjacentHTML('afterend', '<p>New Paragraph</p>');
});

const createInputBtn = document.createElement('button');
createInputBtn.id = 'btn-input-create';
createInputBtn.innerText = 'Create input';
createInputBtn.classList.add('create-input-btn');
mainSection.append(createInputBtn);

createInputBtn.addEventListener('click', (event) => {
    const input = document.createElement('input');
    input.classList.add('input-styles');
    input.type = 'email';
    input.placeholder = 'Your email';
    input.name = 'email';
    event.target.after(input);
})



