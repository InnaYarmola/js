/* 
Теоретичні питання
1. Опишіть своїми словами що таке Document Object Model (DOM)

DOM це представлення структури HTML сторінки у вигляді дерева, де кожен 
елемент є вузлом. За допомогою JS до кожного вузла можна дібратися та 
маніпулювати.


2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

innerHTML - дає доступ до коду
innerText - дає доступ тільки до тексту всередині


3. Як можна звернутися до елемента сторінки за допомогою JS? 
Який спосіб кращий?

Перший спосіб:
document.getElementById(id)
document.getElementsByClassName(className)
document.getElementsByTagName(tagName)
document.getElementsByName(name)

Другий спосіб: 
document.querySelector(selector)
document.querySelectorAll(selector)

Перший більш застарілий, але він швидший і більш точний.
Другий сучасніший, там можна використовувати складні селектори, 
але трохи повільніший. Нема кращого способу, потрібно використовувати відповідно
до поставлених задач.


4. Яка різниця між nodeList та HTMLCollection?

nodeList - статична, повертається через querySelector, може містити будь-який тип вузла,
не має методів масиву, але має forEach

HTMLCollection - жива колекція, повертається через getElement(s)By, 
містить тільки елементи



Практичні завдання
 1. Знайдіть всі елементи з класом "feature", запишіть в змінну, 
 вивести в консоль.
 Використайте 2 способи для пошуку елементів. 
 Задайте кожному елементу з класом "feature" вирівнювання тексту по - 
 центру(text-align: center).
 
 2. Змініть текст усіх елементів h2 на "Awesome feature".

 3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець 
 тексту елементу знак оклику "!".
 */

const featureElements = document.getElementsByClassName('feature');
console.log(featureElements);

const featureClassElements = document.querySelectorAll('.feature');
console.log(featureClassElements);

featureClassElements.forEach(element => {
    element.style.textAlign = 'center';
});

const h2Elements = document.querySelectorAll('h2');
console.log(h2Elements);

h2Elements.forEach(elem => elem.innerText = 'Awesome feature');

const featureTitleElements = document.querySelectorAll('.feature-title');
console.log(featureTitleElements)

featureTitleElements.forEach(elem => elem.innerText += '!');



