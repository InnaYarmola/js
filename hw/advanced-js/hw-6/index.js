/**
Теоретичне питання
Поясніть своїми словами, як ви розумієте поняття асинхронності у 
Javascript

JS однопоточний, тому виконувати задачі з затримкою він не може. Асинхронні 
задачі (timeout, interval, promise, async await)виконуються в браузері.
В JS код виконується зверху вниз, по черзі, тобто синхронно.
Саме асинхронність - це коли задачу треба виконати не одразу, а через деякий час.
Цей час може бути встановленим нами (таймаут), фбо невідомим(отримання даних з серевера).

_____________________________________________________________________


Завдання
Написати програму "Я тебе знайду по IP"

Технічні вимоги:

Створити просту HTML-сторінку з кнопкою Знайти по IP.
Натиснувши кнопку - надіслати AJAX запит за адресою 
https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ 
та отримати інформацію про фізичну адресу.
під кнопкою вивести на сторінку інформацію, отриману з останнього 
запиту – континент, країна, регіон, місто, район.
Усі запити на сервер необхідно виконати за допомогою async await.
 */

const button = document.querySelector('button');
const container = document.querySelector('.container');

class Card {
    constructor(data) {
        this.data = data;
    }

    render() {
        container.innerHTML = ''

        const {timezone, country, region, city, regionName } = this.data;

        container.insertAdjacentHTML('beforeend', `
        <span>Ви знаходитесь:</span>
        <span>Континент - ${timezone.split('/')[0]}</span>
        <span>Країна - ${country}</span>
        <span>Регіон - ${region}</span>
        <span>Місто - ${city}</span>
        <span>Район - ${regionName}</span>
        `)
    }
}

const getIp = async () => {

    try {

        const { data: { ip } } = await axios.get('https://api.ipify.org/?format=json');

        const { data } = await axios.get(`http://ip-api.com/json/${ip}`);

        return data;

    } catch (err) {
        console.log(err)
    }
}

button.addEventListener('click', async () => {

 const data =  await  getIp();

 new Card(data).render()

})