'use strict';

/**
 Наведіть кілька прикладів, коли доречно використовувати в коді 
 конструкцію try...catch.


 Конструкція try...catch використовується в програмуванні для обробки 
 помилок. Наприклад нам дані приходять ззовні і нам треба перевірити чи валідні
 вони і чи є ці дані взагалі. Фбо нам потрібро перевірити чи є такий об'єкт і властивіть в ньому,
 і обробити кейс з помилкою. Або, наприклад ми використовуємо сторонні бібліотеки і 
 перевіряємо чи працює той код.

 */


/**
Виведіть цей масив на екран у вигляді списку (тег ul – список має бути 
згенерований за допомогою Javascript).

На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати 
цей список (схоже завдання виконувалось в модулі basic).

Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність 
(в об'єкті повинні міститися всі три властивості - author, name, price). 

Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка 
із зазначенням - якої властивості немає в об'єкті.

Ті елементи масиву, які не є коректними за умовами попереднього пункту, 
не повинні з'явитися на сторінці.
 */


const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70,
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70,
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70,
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40,
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const booksContainer = document.createElement('div');
booksContainer.id = 'root';
document.body.append(booksContainer);

class ValueNotFoundError extends Error {
    constructor(value) {
        super(`${value} - not found`);
        this.name = 'ValueNotFoundError';
    }
}

class Book {
    constructor(container, author, name, price) {
        this.container = container;
        this.author = author;
        this.name = name;
        this.price = price;

        // const args = [this.author = 'author', this.name = 'name', this.price = 'price'];
        // console.log(args)

        // if (!this.author || !this.name || !this.price) {
        //     throw new ValueNotFoundError('author');
        // }

        if(!author) {
            throw new ValueNotFoundError('author');

        } else if (!name) {
            throw new ValueNotFoundError('name');

        } else if (!price) {
            throw new ValueNotFoundError('price');
        }
    }

    render() {
        this.container.insertAdjacentHTML('beforeend', `
            <ul>
               <li>${this.author}</li>
               <li>${this.name}</li>
               <li>${this.price}</li>
            </ul>
        `);
    };
};

books.forEach(el => {

    try {
        new Book(booksContainer, el.author, el.name, el.price).render();

    } catch (err) {

        console.log(err);  
    };
});




