'use strict';

/**

Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript

В батьківському об'єкті описані загальні змінні, методи для дочірніх об'єктів. Батьківський об'єкт є прототипом для дочірніх.
Вкладеність може буть будь-яка. Якщо мо хочемо викликати метод на якомусь дочірньому об'єкті, то спочатку він шукається всередині 
об'єкта, якщо не знаходить, іде вище і вище по ієрархії аж поки не знайде, або не поверне undefind.

Для чого потрібно викликати super() у конструкторі класу-нащадка?

constructor створює і наповнює this
super ініціалізує this, тобто надає нам доступ до this в об'єкті нащадка.

Завдання
Створити клас Employee, у якому будуть такі характеристики - name (ім'я), 
age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися 
під час створення об'єкта.

Створіть гетери та сеттери для цих властивостей.

Створіть клас Programmer, який успадковуватиметься від класу Employee, і який 
матиме властивість lang (список мов).

Для класу Programmer перезапишіть гетер для властивості salary. Нехай він 
повертає властивість salary, помножену на 3.
Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль. */


class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
        return this._name;
    }

    get age() {
        return this._age;
    }

    set age(value) {
        this._age = value;
        return this.age;
    }
    get salary() {
        return this._salary;
    }

    set salary(value) {
        this._salary = value;
        return this.age;
    }
}

class Programmer extends Employee {

    constructor(name, age, salary, lang) {
        super(name, age, salary, lang);
        this.lang = lang;
    }

    get salary() {
        return super.salary * 3;
    }
}

const developerJs = new Programmer('John', 23, 1500, 'JS');
const developerC = new Programmer('Jenny', 39, 2000, 'C#');
const developerPython = new Programmer('Dylan', 45, 3000, 'Python');
console.log(developerJs);
console.log(developerC);
console.log(developerPython);


