'use strict';

class Card {
    constructor(name, email, title, text, id) {
        this.fullName = name;
        this.email = email;
        this.title = title;
        this.text = text;
        this.id = id;
    }

    container = document.createElement('div');
    buttonsContainer = document.createElement('div');
    deleteBtn = document.createElement('button');

    deleteHandler() {
        this.deleteBtn.addEventListener('click', () => {

            axios.delete(`https://ajax.test-danit.com/api/json/posts/${this.id}`)
            .then(({ status }) => {

                if (status === 200) {
                    this.container.remove()
                }
            })
            .catch(err => {
                throw new Error(err)
            })
        })
    }

    render() {
        this.container.className = 'card';

        this.container.insertAdjacentHTML('beforeend', `
        <div class='nameContainer'>
             <span>${this.fullName}</span> 
             <span>${this.email}</span> 
        </div>
             <h1>${this.title}</h1>
             <p>${this.text}</p>
        `);

        this.buttonsContainer.className = 'buttonsContainer';
        this.deleteBtn.innerText = 'DELETE';
        this.deleteBtn.className = 'btn';
        this.deleteBtn.classList.add('deleteBtn');
        this.buttonsContainer.append(this.deleteBtn);
        this.container.append(this.buttonsContainer);
        document.querySelector('.postsContainer').append(this.container);

        this.deleteHandler();
    }
}

axios.get('https://ajax.test-danit.com/api/json/users')

    .then(({ data }) => {

        const nameArr = data.map(({ id, name, email }) => {
            const userData = { id, name, email };
            return userData;
        })

        axios.get('https://ajax.test-danit.com/api/json/posts')
            .then(({ data: post }) => {

                nameArr.forEach(({ id, name, email }) => {

                    post.forEach(({ id: postId, userId, title, body }) => {
                        if (id === userId) {
                            new Card(name, email, title, body, postId).render();
                        }
                    })
                })
            })
            .catch(err => {
                throw new Error(err)
            })
    })
    .catch(err => {
        throw new Error(err)
    })








