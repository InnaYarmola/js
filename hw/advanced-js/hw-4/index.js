'use strict';

/**
Поясніть своїми словами, що таке AJAX і чим він корисний при 
розробці Javascript.

AJAX - технологія, яка дозволяє отримувати або надсилати дані на сервер 
без перезавантаження сторінки. Програми стають більш інтерактивні та швидкі.



Завдання
Отримати список фільмів серії Зоряні війни та вивести на екран список 
персонажів для кожного з них.

Технічні вимоги:

Надіслати AJAX запит на адресу 
https://ajax.test-danit.com/api/swapi/films
 та отримати список усіх фільмів серії Зоряні війни

Для кожного фільму отримати з сервера список персонажів, 
які були показані у цьому фільмі. Список персонажів можна 
отримати з властивості characters.
Як тільки з сервера буде отримана інформація про фільми, 
відразу вивести список усіх фільмів на екрані. Необхідно вказати 
номер епізоду, назву фільму, а також короткий зміст (поля episodeId, 
    name, openingCrawl).
Як тільки з сервера буде отримано інформацію про персонажів 
будь-якого фільму, вивести цю інформацію на екран під назвою фільму.
 */

const container = document.querySelector('.container');

fetch('https://ajax.test-danit.com/api/swapi/films')
.then(res => res.json())
.then(data => {

    data.forEach(({name: film, episodeId, openingCrawl, characters}) => {

        const div = document.createElement('div');
        div.className = 'film-container';

        const episode = document.createElement('span');
        episode.innerText = `Episode - ${episodeId}`;

        const filmTitle = document.createElement('h1');
        filmTitle.innerText = film;

        const plot = document.createElement('span');
        plot.innerText = openingCrawl;

        const actorsList = document.createElement('ul');

        div.append(episode, filmTitle, plot, actorsList);

        container.append(div);

        characters.forEach(el => {
           fetch(el)
           .then(res => res.json())
           .then(({name: character, films}) => {

            films.forEach(el => {
                fetch(el)
                .then(res => res.json())
                .then(({ name }) => {

                    if (film === name) {
                        actorsList.insertAdjacentHTML('beforeend', `
                        <li>${character}</li>`);
                    }
                })
            })
           })
        });
    });
})
.catch(err => {
     console.log(new Error(`ERROR: ${err}`));
})


