'use strict';

/* Теоритичні питання:
1. В чому відмінність між setInterval та setTimeout?

setInterval - функція буде виконуватись з певним інтервалом, 
поки її не відмінять.
Потрібно обов'язково її чистити.

setTimeout - функція виконається один раз. Чистити рекоменловано.

2. Як припинити виконання функції, яка була запланована для виклику з 
використанням setTimeout та setInterval?

clearInterval(intervalId)
clearTimeout(timeoutlId)

Практичне завдання 1: 

-Створіть HTML-файл із кнопкою та елементом div.

-При натисканні кнопки використовуйте setTimeout, щоб змінити 
текстовий вміст елемента div через затримку 3 секунди. Новий текст 
повинен вказувати, що операція виконана успішно.

Практичне завдання 2: 

Реалізуйте таймер зворотного відліку, використовуючи setInterval. 
При завантаженні сторінки виведіть зворотний відлік від 10 до 1 в 
елементі div. 
Після досягнення 1 змініть текст на "Зворотній відлік завершено".
*/

const btn = document.querySelector('.timer-btn');
const timerDiv = document.querySelector('.display');
const timerSpan = document.createElement('span');
const countdownDiv = document.querySelector('.countdown-container');

btn.addEventListener('click', () => {
    timerSpan.innerText = '';
    if (timerDiv.className.includes('completed')) {
        timerDiv.classList.remove('completed');
    }
    timerDiv.classList.add('inProcess');

    setTimeout(() => {
        timerDiv.classList.add('completed');
        timerSpan.innerText = 'Операція виконана успішно';
        timerSpan.classList.add('display-content');
        timerDiv.append(timerSpan);
    }, 3000);
});

let x = 11;
const countdownSpan = document.createElement('span');
countdownSpan.classList.add('display-content');
countdownDiv.append(countdownSpan);

window.addEventListener('DOMContentLoaded', () => {

    countdownDiv.classList.add('inProcess');

    const intervalId = setInterval(() => {
        x--;
        countdownSpan.innerText = x;
        if (x === 1) {
            clearInterval(intervalId);
            countdownDiv.classList.add('completed');
            countdownSpan.innerText = 'Зворотній відлік завершено';
        }
    }, 1000);
});

