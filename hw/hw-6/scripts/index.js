/**
 * Теоретичні питання
 * 
1. Опишіть своїми словами, що таке метод об'єкту

Це функція, яка передана як властивість в об'єкт.


2. Який тип даних може мати значення властивості об'єкта?

Будь-який.


3. Об'єкт це посилальний тип даних. Що означає це поняття?

Коли ми створюємо об'єкт, під нього виділяється область пам'яті 
і туди записуються дані. Кили ми захочемо скопіювати цей об'єкт - то копія 
буде мати посилання на цю область пам'яті. Це і ок і не ок. 
Добре те, що завдяки цьому не забивається оперативна пам'ять, так як 
кожен раз не треба завантажувати весь об'єкт, а тільки перейти за посиланням.

Мінуси - при зміні в будь-якому з скопійованих об'єктів зміни відбудуться у всіх,
тому треба вдаватись до глибокого копіювання.
 */


/**
 * 1. Створіть об'єкт product з властивостями name, price та discount.
 *  Додайте метод для виведення повної ціни товару з урахуванням знижки. 
 * Викличте цей метод та результат виведіть в консоль.
 */

const product = {
    name: 'tShirt',
    price: 50,
    discountInPersent: 10,

    getPriceWithDiscount() {
        let priceWithDiscount = this.price - ((this.price * this.discountInPersent) / 100);
        return priceWithDiscount;
    },
};

console.log(product.getPriceWithDiscount());

/**

2. Напишіть функцію, яка приймає об'єкт з властивостями name та age, 
і повертає рядок з привітанням і віком,
наприклад "Привіт, мені 30 років". Попросіть користувача ввести своє ім'я та вік
за допомогою prompt, і викличте функцію з введеними даними. Результат виклику функції 
виведіть з допомогою alert.

 */

const name = prompt('Enter your name');
const age = prompt('Enter your age');

const user = {
    name,
    age,
}

const getGreeting = (obj) => {

    const greeting = `
    Hello, my name is ${obj.name}.
    I am ${obj.age} years old.`

    return greeting;
}

alert(getGreeting(user));


/**3.Опціональне. Завдання:
Реалізувати повне клонування об'єкта.
 */

const basket = {

    fruits: {
        apples: 6,
        bananas: 7,
    },

    vegetables: {
        tomato: 3,
        cocomber: 2,
    }

}

const deepCloneObj = (obj) => {

    const newObj = {};

    for (let key in obj) {
    
        newObj[key] = obj[key];

        if (typeof obj[key] === 'object') {
            newObj[key] = deepCloneObj(obj[key]);
        }
    }

    return newObj;

}

const newObj = deepCloneObj(basket);

console.log(basket);
console.log(newObj);

