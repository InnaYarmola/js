/* 
Теоретичні питання
1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?
insertAdjasentHTML - для статичнийх елементів
createElement() - для динамічних елементів

2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.

const navElem = document.querySelector('.navigation');
navElem.remove();

3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?

createElement():

node.append()
node.prepend()
node.before()
node.after()
node.replaceWith()

elem.insertAdjacentHTML():

"beforebegin" – вставити html безпосередньо перед elem,
"afterbegin" – вставити html в elem, на початку,
"beforeend" – вставити html в elem, в кінці,
"afterend" – вставити html безпосередньо після elem


Практичні завдання
 1. Створіть новий елемент <a>, задайте йому текст "Learn More" і 
 атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу.
 
 2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і 
 додайте його в тег main перед секцією "Features".

 Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і 
 додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і 
 додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і 
 додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і 
 додайте його до списку вибору рейтингу.
 */

const learnMore = document.createElement('a');
learnMore.innerText = 'Learn More';
learnMore.href = '#';
const footer = document.querySelector('footer');
footer.append(learnMore);

const selectElem = document.createElement('select');
selectElem.id = 'rating';
const main = document.querySelector('main');
main.prepend(selectElem);

let option;

for (let i = 1; i <= 4; i++) {
    option = document.createElement('option');
    option.value = i;
    option.innerText = `${i} Stars`
    selectElem.append(option);

    if (i === 1) {
        option.innerText = `${i} Star`;
    }
}

