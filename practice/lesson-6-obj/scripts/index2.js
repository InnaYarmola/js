'use strict';

/**
 * примітивні типи даних копіюються за заначенням, а 
 * об'єкти - за посиланням
 */

// const user = { 
//     name: 'Inna',
//     age: 34,
//     marriage: true,
// }

// const user2 = user;

// console.log(user);
// console.log(user2);

// user.age = 1;

// console.log(user);
// console.log(user2);

// user2.name = 'Vova';

// console.log(user);
// console.log(user2);

// console.log(user === user2);


// const user = {
//     name: 'Inna',
//     age: 34,
//     marriage: {
//         status: true,
//         mate: 'male',
//         name: 'Slav',
//     },
// }

// console.log(user);

// const user2 = Object.assign({}, user);
// console.log(user2);

// user.age = 50;
// user.marriage.status = false;

// console.log(user);
// console.log(user2);


// const user1 = {
//     name: 'Inna',
//     age: 34,
//     marriage: true,
// }

// console.log(user === user1);

// const sayHi = () => console.log(`Hello, ${user.name})`);

// const sayHi = function() {
//     console.log(`Hello, ${this.name})`);
// }

// const user = {
//     name: 'Inna',
//     age: 34,
//     marriage: {
//         status: true,
//         mate: 'male',
//         name: 'Slav',
//     },
//     sayHi () {
//         console.log(`Hello, ${this.name}), ${this.age}, married with ${this.marriage.name}`);
//     }

// sayHi: () => {
//     console.log(`Hello, ${user.name})`);
// }

// sayHi,
// }

// user.sayHi();

// this - це обєкт перед точкою, який 
// використовується для виклику метода.

// const animalHi = function() {
//     console.log(this.voice);
// };

// const dog = {
//     voice: 'bark-bark',
//     animalHi,
// };

// const cat = {
//     voice: 'miau-miau',
//     animalHi,
// };

// dog.animalHi();
// cat.animalHi();

// const f = function () {
//     console.log(this);
// }

// f()

// const user = {
//     name: 'John',
//     sayHi: () => console.log(this.name),
// }

// user.sayHi();

// const user = {
//     name: 'John',
//     age: 32,

//     logInfo () {

//         const sayName = () => {
//             console.log(this.name);
//         }

//         const sayAge = () =>  {
//             console.log(this.age);
//         }

//         sayName();
//         sayAge();


//     }
// }

// user.logInfo();

// Створіть метод в об'єкті "Група", який повертає 
// середній вік всіх студентів у групі.


// const getAverageMArk = function () {

//     let count = null;
//     let totalMark = null;
//     let averageMark = null;

//     for (let key in this.mark) {
//         count++;

//         totalMark += this.mark[key];
//     }
//     return averageMark = totalMark / count;
// }

// const pe26 = {

//     student1: {
//         name: 'Ivan',
//         lastName: 'Ivanov',
//         age: 32,
//         mark: {
//             chemistry: 3,
//             math: 4,
//             lang: 3,
//             history: 5,
//         },
//         getAverageMArk,
//     },

//     student2: {
//         name: 'Petro',
//         lastName: 'Petrov',
//         age: 30,
//         mark: {
//             math: 5,
//             lang: 4,
//             history: 3
//         },
//         getAverageMArk,
//     },

//     student3: {
//         age: 50,
//         mark: {
//             math: 3,
//             lang: 5,
//             history: 5,
//         },
//         getAverageMArk,
//     },
//     student4: {
//         age: 10,
//         mark: {
//             math: 5,
//             lang: 2,
//             history: 5,
//         },
//         getAverageMArk,

//     },

//     getAverageAge() {

//         let count = null;
//         let totalAge = 0;

//         for (let key in this) {

//             if (typeof this[key] === 'function') {
//                 continue;

//             } else {

//                 count++;

//                 // console.log(pe26[key].age);

//                 totalAge += this[key].age;
//             }

//         }

//         let middleAge = totalAge / count;

//         return parseInt(middleAge);
//     },

//     getAverageMark() {

//         let totalMark = 0;
//         let count = 0;
//         let averageMark = 0;

//         for (let key in this) {

//             let marks = this[key].mark;

//             for (let key2 in marks) {

//                 count++;
//                 totalMark += marks[key2];
//             }   
//         }

//         return averageMark = totalMark / count;
//     },

// };

// console.log(pe26.getAverageMark());
// console.log(pe26.getAverageAge());
// console.log(pe26.student1.getAverageMArk());
// console.log(pe26.student2.getAverageMArk());
// console.log(pe26.student3.getAverageMArk());
// console.log(pe26.student4.getAverageMArk());

/**
Створіть два об'єкти, які містять однакові властивості. 
Присвойте значення одного об'єкта іншому за допомогою 
посилання, а потім виведіть обидва об'єкта. Якщо один зміниться, 
перевірте, чи змінюється інший.
 */




// const hasProperty = (obj, property) => obj.hasOwnProperty(property);

// console.log(hasProperty(user, 'age'));

/**
 * Спробуйте заморозити об'єкт за допомогою "Object.freeze". 
 * Спробуйте внести зміни в заморожений об'єкт та виведіть результат.
 */


// const user = {
//     age: 10,
//     name: 'John',
//     hobbies: {
//         football: true,
//         knitting: false,
//     },

// };

// Object.defineProperty(user, 'age', {
//     enumerable: false,
// });

// for (let key in user) {
//     console.log(key)
// }

// const user2 = Object.create(user);

// console.log(user);
// console.log(user2);

// Object.freeze(user);



// Object.seal(user);


// console.log(user)

// user.age = 12;

/**Створіть об'єкт, який має властивість "розмір". Додайте "getter" та "setter" методи, 
 * які дозволяють отримувати та встановлювати розмір об'єкта. */

// const size = 'M';

// const tr = {

//     color: 'black',

//     _size: size,

//     get size() {
//         return this._size;
//     },

//     set size(value) {

//         if (value !== 'M') {
//             confirm('Are you sure?');

//             if (confirm) {
//                 this._size = value;

//             }
            
//         }

//     }
// }

// console.log(tr);


// const deepCloneObj = (obj) => {

//     const newObj = {};

// for (let key in obj) {

//     newObj[key] = obj[key];

//     if (typeof obj[key] === 'object') {
//         newObj[key] = deepCloneObj(obj[key]);
//     }
// }

//     return newObj;
// }

// const user2 = deepCloneObj(user);

// console.log(user);
// console.log(user2);

// user.hobbies.football = false;

// console.log(user);
// console.log(user2);


// console.log(Object.keys(user));
// console.log(Object.values(user));
// console.log(Object.entries(user));


// const user2 = user;

// console.log(user);
// console.log(user2);

// user.age = 10;

// console.log(user);
// console.log(user2);





// Запитати користувача ім'я, прізвище, вік (перевірити на
// коректну цифру
// і що число між 15 і 100), чи є діти, якого кольору очі.
// Усі ці дані записати в об'єкт.
// Перебрати об'єкт і вивести у консолі фразу
// `${ім'я властивості}: ${значення}` і так з усіма властивостями

// const name = prompt('Enter your name');
// const lastName = prompt('Enter your last name');

// let age = null;

// while ((age < 15 || age > 100) || isNaN(age)) {

//     age = prompt('Enter your age');
// }

// const haveChildren = confirm('Do you have children?');
// const eyeColor = prompt('Enter your eyeColor');

// const user = {
//     name,
//     lastName,
//     age,
//     haveChildren,
//     eyeColor,
//     showInfo() {
//         for (let key in this) {

//             if (typeof this[key] === 'function') {
//                 continue;

//             } else {
//                 console.log(`${key} - ${this[key]}`)
//             }
//         };
//     },

//     getFullName() {
//         let fullName = `${this.name} ${this.lastName}`;

//         return fullName;
//     },
// }

// user.showInfo();
// console.log(user.getFullName())





/**
 * Завдання 6.
 *
 * Написати функцію-помічник для ресторану.
 *
 * Функція має два параметри:
 * - Розмір замовлення (small, medium, large);
 * - Тип обіду (breakfast, lunch, dinner).
 *
 * Функція повертає об'єкт із двома полями:
 * - totalPrice - загальна сума страви з урахуванням її розміру
 * та типу;
 * - totalCalories — кількість калорій, що міститься у блюді
 * з урахуванням його розміру та типу.
 *
 * Нотатки:
 * - Додаткові перевірки робити не потрібно;
 * - У рішенні використовувати референтний об'єкт з цінами
 * та калоріями.
 *
 */

// const priceList = {

//     sizes: {
//         small: {
//             price: 15,
//             calories: 250,
//         },
//         medium: {
//             price: 25,
//             calories: 340,
//         },
//         large: {
//             price: 35,
//             calories: 440,
//         },
//     },

//     types: {
//         breakfast: {
//             price: 4,
//             calories: 25,
//         },
//         lunch: {
//             price: 5,
//             calories: 5,
//         },
//         dinner: {
//             price: 10,
//             calories: 50,
//         },
//     },

//     calcPriceAndCalories(size, type) {

//         let totalPrice = null;
//         let totalCalories = null;

//         const sizePrice = this.sizes[size].price;
//         const typePrice = this.types[type].price;

//         totalPrice = sizePrice + typePrice;

//         const sizeCalories = this.sizes[size].calories;
//         const typeCalories = this.types[type].calories;

//         totalCalories = sizeCalories + typeCalories;

//         const totalPriceAndCalories = {
//             totalPrice,
//             totalCalories,
//         };

//         return totalPriceAndCalories;
//     },
// };

// console.log(priceList.calcPriceAndCalories('small', 'breakfast'));
// console.log(priceList.calcPriceAndCalories('medium', 'dinner'));
// console.log(priceList.calcPriceAndCalories('large', 'lunch'));




/*
  * Написати функцію-фабрику, яка повертає об'єкти користувачів.
  *
  * Об'єкт користувача має три властивості:
  * - Ім'я;
  * - Прізвище;
  * - Повне ім'я - тільки для читання, виводить рядок 
  * Ім'я + Прізвище;
  * - Професія - має бути одне з "developer", "QA", "designer"
  * , "PM" - 
  * при зміні якості
  * у вже існуючому об'єкті ця перевірка має зберігатися.
  *
  * Функція-фабрика в свою чергу має три параметри,
  * які відображають вищеописані властивості об'єкта.
  * Кожен параметр функції має значення за замовчуванням: null.
  */

// const createUser = (name, lastName, profession, value) => {

//     const user = {
//         name,
//         lastName,
//         profession,

//         _fullName: fullname,

//         get fullName () {
//             return `${this.name} ${this.lastName}`;
//         },


//     }

//     return user;
// };

// const user = createUser('Inna', 'Yarmola', 'Developer', 'Hello');

// console.log(user);



