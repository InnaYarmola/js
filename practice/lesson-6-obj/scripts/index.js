'use strict';

// let userInfo = {
//     name: 'Inna',
//     age: 33,
//     city: 'Kyiv',

//     // greeting: function() {
//     //     console.log(`Hello, ${this.name}`);
//     // }

//     greeting() {
//         console.log(`Hello, ${this.name}))`)
//     }

// }

// let user = Object.create(userInfo);
// console.log(user);

// user.hobby = 'football';

// console.log(user);

// for (let key in user) {
//     console.log(`${key} - ${user[key]}`);
// }

// console.log(user.hasOwnProperty('name'))

// userInfo.greeting()

// for (let key in userInfo) {
//     console.log(`${key} = ${userInfo[key]}`);
// }

// userInfo.sayHello = function() {
//     console.log(`Hello, ${this.name}!`)
// }

// userInfo.sayHello();


// const sayHello = function() {
//     console.log(`Hello, ${this.name}!`);
// }

// userInfo.greeting = sayHello;

// userInfo.greeting();
// console.log(userInfo);




// console.log(userInfo.name);
// console.log(userInfo['name']);

// userInfo.name = 'Vova';

// console.log(userInfo.name);
// console.log(userInfo);

// userInfo.pet = 'dog';

// console.log(userInfo);

// delete userInfo.city;
// console.log(userInfo);

// console.log('age' in userInfo);



// const person = function (name, age) {
//     this.name = name;
//     this.age = age;
// }

// const userOne = new person('Inna', 30);
// console.log(userOne);



// let user = {

//     name: 'Inna',
//     lastName: 'Yarmola',
//     age: 30,


// }

// Object.defineProperty(user, 'name', {
//     configurable: false,
//     writable: false,
//     value: 'Inna'
// })

// console.log(Object.getOwnPropertyDescriptor(user, 'name'))

// Object.defineProperty(user, 'fullName', {
//     get: function() {
//         return `${this.name} ${this.lastName}`;
//     }


// })

// console.log(user)





// delete user.name;

// user.name = 'Vova'

// console.log(user);


// const user = {
//     city: 'Kyiv',
//     hobby: 'sports',
//     pet: 'dog'
// }

// user.name = 'Inna';

// console.log(user);

// Object.defineProperty(user, 'name', {
//     writable: false,
//     value: 'Vova'
// })

// console.log(user);

// Object.defineProperty(user, 'age', {
//     writable: false,
//     value: 33,

// })

// console.log(user);

// console.log(Object.getOwnPropertyDescriptor(user, 'age'));

// console.log(user.hasOwnProperty('name'));

// console.log(Object.values(user));
// console.log(Object.keys(user));
// console.log(Object.entries(user));

// const user = new Object();
// user.name = "Inna";
// user['Last name'] = 'Yarmola';
// user.hobby = 'sport'

// console.log(user);

// delete user.hobby;

// console.log(user);

// console.log('Last name' in user, user['Last name'])


// const product = {
//     name: 'apple',
//     initQuantity: 10,
//     sold: 5,
//     _price: 6,

//     actualQuantity() {
//         return this.initQuantity - this.sold;
//     },

//     logThis() {
//         console.log(this)
//     },

// setNewPrice(value) {
//     if (value > 0) {
//         this.price = value;
//     }
// }

// set newPrice(value) {
//     if (value > 0) {
//         this._price = value;
//     }
// },

// get newPrice() {
//     return this._price;
// }

// }

// product.price = 30;

// console.log(product.newPrice(10));

// const onStock = product.actualQuantity();

// // console.log(onStock);

// // product.logThis();

// console.log(product.price);

// product.setNewPrice(10);

// Object.defineProperty(product, 'price', {
//     writable: false,
// })

// console.log(product.price);

// // product.price = -10;

// console.log(product.price);



// const user = {
//     name: 'Inna',
//     lastName: 'Yarmola',
//     age: 34,

//     children: {
//         'has children': true,
//         name: 'Vova',
//         age: 4,
//     },
//     marriage: {
//         'is married': true,
//         mate: 'husband',
//         name: 'Slavik',
//         age: 34,
//     }
// }

// console.log(user.children.hasOwnProperty('name'));

// Object.defineProperties(user, {
//     'name': {
//         writable: false,
//         value: 'Galia'
//     },
//     'age': {
//         writable: false,
//         value: 20
//     }
// })



// console.log(user);

// const user2 = user;

// user2.name = 'Galia';
// user2.children.name = 'Vasia';
// user2.marriage.name = 'Petro';

// console.log(user2);
// console.log(user);

// const user2 = {};

// for (let key in user) {
//     user2[key] = user[key];

// }

// user2.name = 'Galia';
// user2.age = 50;
// user2.children.name = 'Vasia';
// user2.marriage.name = 'Petro';

// console.log(user2);
// console.log(user);

// const user2 = Object.assign({}, user);

// user2.name = 'Galia';
// user2.age = 50;
// user2.children.name = 'Vasia';
// user2.marriage.name = 'Petro';

// console.log(user);
// console.log(user2);
// 
// console.log(Object.keys(user));
// console.log(Object.values(user));
// console.log(Object.entries(user));


// const obj = new Object();
// console.log(obj);

// const user = {
//     name: 'John',
//     age: 30,

// };

// const dog = {
//     name: 'Bob',
//     age: 3,
//     color: 'grey',

// };

// dog.owner = user.name;

// console.log(dog);

// console.log(dog.age);

// delete dog.age;

// console.log(dog.age);
// console.log(dog);

// const key = prompt('What do you want to know?', 'name');

// console.log(user[key]);
// console.log(user.key);

// const audi = {
//     standart: '4000$',
//     comfort: '6000$',
//     premium: '8000$'
// }

// let key = prompt('Conplectation?', 'standart');

// console.log(audi[key]);



// const fruit = prompt('What fruit should we buy?');
// const quantity = +prompt('How many?');

// const bag = {
//     [fruit]: quantity,
// }

// console.log(bag[fruit]);


// let fruit = prompt('What fruit should we buy?');
// let quantity = +prompt('How many?');

// const bag = {};

// bag[fruit] = quantity;

// console.log(bag);

// let key;
// let quantity;

// while (key !== null) {

//     key = prompt('What fruit should we buy?');
//     quantity = +prompt('How many?');

//     bag[key] = quantity;

// }


// console.log(bag);


// const makeUser = (name, age) => {
//     return {
//         name,
//         age,
//     };
// }

// const user = makeUser('Inna', 34);
// console.log(user);

// const makeUser = (key, value) => {
//     return {
//     [key]: value,
    
//     };
// }

// const user = makeUser('Inna', 34);

// console.log(user);


// const makeUser = (name, age) => {

//     name = prompt('Enter your name');
//     age = +prompt('Enter your age');

//     return {
//         name,
//         age,
//     };
// }

// const user = makeUser();
// console.log(user);


// Створіть об'єкт, який представляє сутність "Студент". Об'єкт повинен містити
//  властивості, такі як ім'я, прізвище, вік, курс та факультет.

// Додайте метод до об'єкта "Студент", який виводить повідомлення 
// з основною інформацією про студента (ім'я, прізвище, курс).

// Змініть властивість "вік" у вашому об'єкті "Студент".
// Додайте нову властивість, наприклад, "середній бал", і присвойте їй значення.

// const student = {
//     name: 'Ivan',
//     lastName: 'Ivanov',
//     year: 3,
//     age: 18,
//     faculty: 'IO-17',

//     showInfo() {
//         console.log(`Student - ${this.name} ${this.lastName}, course - ${this.year}.`);
//     },
// }


// student.age = 21;

// student.averageMark = 4.3;

// console.log(student);



// Створіть об'єкт, який представляє групу студентів. Кожен студент 
// повинен бути окремим об'єктом,
//  а сама група - це об'єкт, який містить об'єкти студентів.

// const pe26 = {

//     student1: {
//         name: 'Ivan',
//         lastName: 'Ivanov',
//         year: 3,
//         age: 18,
//         faculty: 'IO-17',
//         averageMark: 3.2,
//     },

//     student2: {
//         name: 'Petro',
//         lastName: 'Petrov',
//         year: 3,
//         age: 20,
//         faculty: 'IO-17',
//         averageMark: 4.2,
//     },

//     student3: {
//         name: 'Sergiy',
//         lastName: 'Sergiev',
//         year: 3,
//         age: 22,
//         faculty: 'IO-17',
//         averageMark: 4.8,
//     },

// }

// Виведіть інформацію про кожного студента з
//  групи, використовуючи цикл для ітерації по об'єктам.

// for (let key in pe26) {
//     console.log(key)
// }

// const user = new Object();

// const user = {
//     name: 'Inna',
//     age: 34,
//     hobbies: {
//         a: 'aaa',
//         b: 'bbb',
//     }
// };

// const key = prompt('Key');
// const value = prompt('Value');

// user[key] = value;
// console.log(user);

// console.log(user['a' + 'g' + 'e']);

// // console.log(user);

// user.city = 'Boryspil';
// user.age = 20;
// delete user.age;

// const newKey = prompt('What do you want to add');
// const value = prompt('Enter value');

// user[newKey] = value;

// // const key = prompt('What do you want to see?');

// console.log(user);

// const fruit = prompt('What fruit?');

// const basket = {
//     [fruit]: 5,
// }

// console.log(basket);


// const fruit = prompt('Fruit');

// const bag = {
//     [fruit]: 5,
// }

// console.log(bag);

// const userCreator = (name, age) => {

//     const user = {
//         name,
//         age: undefined,
//     };
//     return user;
// };

// const user = userCreator('Inna', 33);
// console.log(!!user.name);

// console.log(user.hasOwnProperty('name'));

// const user = {
//     name: 'Inna',
//     age: 34,
//     hobbies: {
//         a: 'aaa',
//         b: 'bbb',
//     }
// };

// for (let key in user) {
//     console.log(user[key]);

//     for (let key2 in user[key]) {
//         console.log(user[key][key2]);
//     }
// }

// for (let key in user) {
//     console.log(`${key} = ${user[key]}`);
// }


/**
 * Створіть об'єкт, який представляє сутність "Студент". 
 * Об'єкт повинен містити властивості, такі як ім'я, 
 * прізвище, вік, курс та факультет.
 */

/*
 Додавання методів:
Додайте метод до об'єкта "Студент", який виводить 
повідомлення з основною інформацією про студента 
(ім'я, прізвище, курс).
*/

// const student = {
//     name: 'Ivan',
//     lastName: 'Ivanov',
//     course: 2,
//     faculty: 'PE-26',
//     logInfo () {
//         console.log(`Student ${this.name} ${this.lastName}, faculty - ${this.faculty}`)
//     }
// }
// student.age = 33;
// student.averageMark = 4.3;
// console.log(student);
// student.logInfo();


/*
Маніпуляція властивостями:
Змініть властивість "вік" у вашому об'єкті "Студент".
Додайте нову властивість, наприклад, "середній бал", 
і присвойте їй значення.
*/



/*
Створення багатовимірного об'єкта:
Створіть об'єкт, який представляє групу студентів. 
Кожен студент повинен бути окремим об'єктом, а сама 
група - це об'єкт, який містить об'єкти студентів.
*/

// const pe26 = {

//     student1: {
//         name: 'Ivan',
//         lastName: 'Ivanov',
//         age: 32,
//     },

//     student2: {
//         name: 'Petro',
//         lastName: 'Petrov',
//         age: 30,
//     }, 
// }

/*
Ітерація об'єктів:
Виведіть інформацію про кожного студента з групи, 
використовуючи цикл для ітерації по об'єктам.
 */

// for (let key in pe26) {
//     console.log(pe26[key]);

//     const student = pe26[key];

//     for (let key2 in student) {
//         console.log(`${key2} = ${student[key2]}`);
//     }  
// }

