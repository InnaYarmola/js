'use strict';

// Напишіть функцію, 
// яка приймає масив чисел і повертає найбільший елемент у цьому масиві.

// const arr = [1, 4, 7, 98, 34, 23];

// const findBiggestNumber = (arr) => {

//     arr.sort((a, b) => b - a);

//     return arr[0];
// }

// findBiggestNumber(arr);
// console.log(findBiggestNumber(arr));


//_______________________________________________________________________________


// Напишіть функцію, яка видаляє всі дублікати 
// з масиву і повертає новий масив без повторень.


//______________________________________________________________________________

//Обчислення суми елементів масиву

// const arr = [1, 4, 7, 98, 34, 23];

// const calcSum = (arr) => {
//     let sum = 0;
//     arr.forEach((elem) => sum += elem);
//     return sum;
// }

// console.log(calcSum(arr));

//______________________________________________________________________________

// Фільтрація парних чисел

// const arr = [1, 4, 7, 98, 34, 23, 2];

// const findEvenNumbers = (arr) => arr.filter((elem) => elem % 2 === 0);

// const filteredArr = findEvenNumbers(arr);
// console.log(filteredArr);

//______________________________________________________________________________

//Пошук позиції елемента у масиві

// const arr = [1, 4, 7, 98, 34, 23, 2];

// const findIndex = (arr, elem) => {

//     return arr.indexOf(elem);

// }

// console.log(findIndex(arr, 98));



//______________________________________________________________________________

//Перевертання масиву
// const arr = [1, 4, 7, 98, 34, 23, 2];

// const reverseArr = (arr) => {
//     return arr.reverse();
// }

// console.log(reverseArr(arr))


// //______________________________________________________________________________
// const arr = [1, 4, 7, 98, 34, 23, 2, 120];

// Створіть функцію, яка приймає масив чисел і 
// повертає об'єкт з найменшим і найбільшим значенням.

// const findMinMaxElem = (arr) => {
//     arr.sort((a, b) => a - b);
//     return {
//         min: arr[0],
//         max: arr[arr.length - 1],
//     }
// }

// console.log(findMinMaxElem(arr))

//______________________________________________________________________________
//const arr = [1, 4, 7, 98, 34, 23, 2];

// Знаходження найменшого числа в масиві

// const arr = [1, 4, 7, 98, 34, 23];

// const findMinElem = (arr) => {
//     arr.sort((a, b) => a - b);
//     return arr[0];
// }

// console.log(findMinElem(arr))


// //______________________________________________________________________________
// const arr = [1, 4, 7, 98, 34, 23, 2, 120];

//Створіть функцію, яка підраховує суму всіх елементів в масиві чисел.

// const calcSum = (arr) => {
//     let sum = null;

//     arr.forEach((elem) => sum += elem);

//     return sum;
// }

// console.log(calcSum(arr))

// //______________________________________________________________________________
// const arr = [1, 4, 7, 98, 34, 23, 2, 120];

//Створіть функцію, яка повертає індекс заданого елемента у масиві 
//(якщо елемент не знайдено, повертати -1).

// const findIndex = (arr, elem) => arr.indexOf(elem);

// console.log(findIndex(arr, 1));

//______________________________________________________________________________


//Використовуючи forEach, виведіть кожен елемент масиву у консоль.
// arr.forEach((elem) => console.log(elem));

//Обчисліть суму всіх елементів у масиві чисел за допомогою forEach.
// let sum = null;
// arr.forEach((elem) => sum += elem);
// console.log(sum);

//Створіть об'єкт користувачів та виведіть інформацію про кожного
//  користувача за допомогою forEach.

// const users = [
//     { name: 'John', age: 25 },
//     { name: 'Jane', age: 30 },
//     { name: 'Bob', age: 22 }
//   ];

//   users.forEach((elem) => console.log(elem))

//Додайте число 10 до кожного елемента у масиві чисел за допомогою forEach.
// const arr = [1, 4, 7, 98, 34, 23, 2, 120];

// const addTenF = (elem) => elem + 10;

// arr.forEach((elem) => console.log(addTenF(elem)));

//______________________________________________________________________________



//Створіть новий масив, в якому видаляться всі дублікати з вихідного масиву.

// const newArr = [...new Set(arr)];
// console.log(newArr);

// Визначте, чи є дублікати в масиві.

// const isDublicate = (arr) => {
//     console.log(new Set(arr).size)
//     console.log(arr.length)

//     return new Set(arr).size !== arr.length;

// }

// console.log(isDublicate(arr))
// // isDublicate(arr)

// const hasDublicates = (arr) => new Set(arr).size === arr.length;

// console.log(hasDublicates(arr));


// Знайдіть середнє значення чисел в масиві.

// const middleValue = (arr) => {
//     let sum = null;
//     let iterator = null;

//     arr.forEach((elem) => {
//         iterator++;
//         sum += elem;
//     })

//     return sum / iterator;
// }

// console.log(middleValue(arr));


// Знайдіть максимальну довжину послідовності однакових елементів в масиві.


// const findRepeatMaxLength = (arr) => {

//     let currentLength = 1;
//     let maxLength = 1;

//     for (let i = 1; i < arr.length; i++) {

//         if (arr[i] === arr[i - 1]) {
//             currentLength++;
//             // console.log('current first', currentLength);

//         } else {
//             maxLength++;
//             // console.log('max', maxLength);


//             maxLength = Math.max(maxLength, currentLength);
//             currentLength = 1;
//             // console.log('current', currentLength);
//         }
//     }

//     return maxLength;

// }

// console.log(findRepeatMaxLength(arr));




//Замініть всі входження конкретного елемента на інший у масиві.
// const arr = [1, 2, 3, 4, 5];


// Знайдіть максимальну довжину послідовності однакових елементів в масиві.

// const arr = [1, 2, 2, 3, 3, 3, 3, 3, 2, 2];

// const maxRepeatElem = (arr) => {
//     let currentLength = 1;
//     let maxLength = 1;

//     for (let i = 1; i < arr.length; i++) {
//         // console.log(arr[i])
//         if (arr[i] === arr[i - 1]) {
//             currentLength++;

//         } else {
//             maxLength = Math.max(maxLength, currentLength);
//             currentLength = 1;

//         }

//     }

//     return maxLength;
// }

// console.log(maxRepeatElem(arr));

// Об'єднайте всі рядки у масиві в один рядок за допомогою forEach.

// const str = ['Hello', ', ', 'how ', 'are ', 'you?'];

// let newStr = '';

// str.forEach((elem) => newStr += elem);

// console.log(newStr);





// Користувач вводить багатозначне число через promt.
// Напишіть функцію colonOdd(num), яка приймає число num як аргумент і
// Вставляє двокрапку (:) між двома непарними числами.
// Наприклад, якщо вводиться число 55639217, то вихід має бути 5:563:921:7.


// const number = '1122334455';


// const colonOdd = (num) => {


//     let arr = num.split('');

//     const newArr = arr.map((elem, index, arr) => {

//         if (arr[index] % 2 !== 0 || arr[index - 1] % 2 !== 0) {

//             // const indexToAddColon = index - 1;

//             // // console.log(indexToAddColon);

//             return arr.splice(index, 0, ':');

//         } else {

//             return arr;
//         }
//     })

//     return newArr[0].join('');

// }

// console.log(colonOdd(number));





/**
 * Завдання 1.
 *
 * Написати імплементацію вбудованої функції рядка repeat(times).
 *
 * Функція повинна мати два параметри:
 * - Цільовий рядок для пошуку символу за індексом;
 * - Кількість повторень цільового рядка.
 *
 * Функція повинна повертати перетворений рядок.
 *
 * Умови:
 * - Генерувати помилку, якщо перший параметр не є рядком, а другий не числом.
 */


/**
 * Завдання 4.
 *
 * Написати функцію truncate, яка «обрізає» занадто довгий рядок, додавши 
 * в кінці три крапки «...».
 *
 * Функція має два параметри:
 * - Вихідний рядок для обрізки;
 * - Допустима довжина рядка.
 *
 * Якщо рядок довший, ніж його допустима довжина — його необхідно обрізати,
 * і конкатенувати до кінця символ три крапки так, щоб разом з ним довжина
 * Рядки дорівнювала максимально допустимої довжині рядка з другого параметра.
 *
 * Якщо рядки не довші, ніж її допустима довжина.
 */

// const str = 'Hello, my dear winter!'

// const truncate = (str, strLength) => {
//     const arr = str.split('');

//     if (arr.length > strLength) {
//         arr.length = strLength - 3;
//         arr.push('...');
//     }

//     return arr.join('');
// }

// console.log(truncate(str, 15));



/**
 * Завдання 5.
 *
 * Написати функцію-помічник комірника.
 *
 * Функція має один параметр:
 * - Рядок зі списком товарів через кому (water, banana, black, tea, apple).
 *
 * Функція повертає рядок у форматі ключ-значення, де ключ – ім'я товару, 
 * а значення – його залишок на складі.
 * Кожен новий товар усередині рядка повинен утримуватися на новому рядку.
 *
 * Якщо якогось товару на складі немає, як залишок вказати «not found».
 *
 * Умови:
 * - Ім'я товару не повинні бути чутливими до регістру;
 * - Додаткових перевірок робити не потрібно.
 */

/* Дано */
// const store = {
//     apple: 8,
//     beef: 162,
//     banana: 14,
//     chocolate: 0,
//     milk: 2,
//     water: 16,
//     coffee: 0,
//     tea: 13,
//     cheese: 0,
// };


// const goods = 'water, banana, tea, apple, cheese, milk, coffee';

// const storeHelper = (list) => {

//     const newArr = list.split(', ').map((elem) => {
//         let newArr = `${elem} - ${store[elem]}`;

//         if (store[elem] === 0) {
//             newArr = `${elem} - out of stock`;
//         }

//         return newArr;
//     });

//     return newArr.join(
//         `, 
// `)
// }

// console.log(storeHelper(goods));


/**
 * Завдання 3.
 *
 * Написати функцію, upperCaseAndDoubly, яка перекладає
 * символи рядка у верхній регістр і дублює кожен її символ.
 *
 * Умови:
 * - Використовувати вбудовану функцію repeat;
 * - Використовувати вбудовану функцію toUpperCase;
 * - Використовувати цикл for...of.
 */

// Напишіть функцію, яка приймає рядок як аргумент
// і перетворює регістр першого символу рядка з нижнього регістра у верхній.

// const str = 'hello, how are you doing?';


// const upperCaseAndDoubly = (str) => {

//     let newStr = '';

//     for (let i = 0; i < str.length; i++) {
//         newStr += str[i].repeat(2);

//     }

//     return newStr.toUpperCase();
// }

// console.log(upperCaseAndDoubly(str))

//Додайте новий елемент в кінець масиву.
//Зробіть зріз масиву від певного індексу до кінця.

// const arr = [1, 2, 3];

// // const arrLength = arr.push(6)
// // console.log(arr)
// // console.log(arrLength)
// // const str = arr.pop();
// // console.log(str);

// const newArr = arr.slice(3);
// console.log(newArr);

// arr.reverse();
// console.log(arr)

// arr.sort((a, b) => a - b);
// console.log(arr);

// //Перетворіть кожен елемент масиву в його квадрат.

// const squaredElem = arr.map((elem) => elem * elem);
// console.log(squaredElem)

// //Знайдіть суму всіх елементів масиву.

// // let sum = null;
// // arr.forEach((elem) => sum += elem);

// // console.log(sum)

// let sum = arr.reduce((acc, curr) => acc + curr, 0);
// console.log(sum);

//Відфільтруйте лише парні числа з масиву.

// const arr = [1, 2, 4, 8, 9, 2, 9, 10, 2, 2];
// const arr2 = [1, 3, 5, 7, 4, 9]

// const showElem = );

// arr.forEach((elem, index, arr) => console.log(`${index}: ${elem} -- ${arr}`));

// const arr = ['*'];

// const newArr = arr.map((elem) => {

//     let newArr = [];

//     for (let i = 0; i <= 10; i++) {

//         newArr[i] = elem.repeat(i);

//     }

//     return newArr;
// })

// console.log(newArr);

//Видалити всі входження певного елемента з масиву.

// const filteredArr = arr.filter((elem) => elem !== 2);
// console.log(filteredArr);

//Злити два відсортованих масиви в один відсортований масив.

// const newArr = [...new Set(arr, arr2)].sort((a, b) => a - b);
// console.log(newArr);

//Перевернути порядок слів у рядку.
// const str = 'Hello, how are you?';

// const arr = str.split(" ");
// console.log(arr.reverse());

//Знайти найбільшу послідовність однакових елементів у масиві.

//Знайти кількість входжень певного елемента в масив.

// const countRepeats = (elem, arr) => {
//     let count = null;

//     for (let i = 0; i < arr.length; i++) {

//         if (elem === arr[i]) {
//             count++;
//         }
//     }
//     return count;
// }

// console.log(countRepeats(2, arr));

//Створити новий масив, який містить тільки унікальні значення з вихідного масиву.

// const uniqueNumbers = [...new Set(arr, arr2)];



//Об'єднати два масиви, видаливши дублікати.

// const uniqueNumbers = [...new Set(arr, arr2)]


// console.log(uniqueNumbers);


//Знайти суму всіх парних чисел у масиві.

// const sumEvenNumbers = (arr) => {
//     let sum = null;

//     for (let elem of arr) {
//         if (elem % 2 === 0) {
//             sum += elem;
//         }
//     }

//     return sum;
// }
// console.log(sumEvenNumbers(arr));;

//Поверніть новий масив, який містить лише унікальні елементи обох масивів.

// const newArr = new Set([...arr, ...arr2]);
// console.log(newArr);


//Змініть кожен елемент масиву, додавши до нього 10.

// const newArr = arr.map(elem => elem * 10);
// console.log(newArr);

//Знайдіть середнє значення чисел у масиві.
// let sum = 0;

// arr.forEach(elem => sum += elem);

// const mediumNumber = sum / arr.length;
// console.log(sum);
// console.log(mediumNumber);

// const newArr = new Set(arr);
// console.log(newArr)

//Створіть новий масив, який містить тільки непарні числа з оригінального масиву.

// const oddNumbers = arr.filter(elem => elem % 2 !== 0);
// console.log(oddNumbers);

//Перевірте, чи масив містить певний елемент.

// const hasElem = arr.includes(1);
// console.log(hasElem)

// const maxElem = Math.max(...arr);
// console.log(maxElem)

// const minElem = Math.min(...arr);
// console.log(minElem)

//Знайдіть найменший елемент у масиві.

//Перетворіть масив рядків на один рядок, розділений комами.

// const str = arr.join(', ');
// console.log(str);

//Видаліть дублікати з масиву.



// const evenNumbers = arr.filter((elem) => elem % 2 === 0);
// console.log(evenNumbers)

//Перевірте, чи всі елементи масиву більше певного числа.

// const isBigger = arr.every((elem) => elem > 0);
// const someAreBigger = arr.some((elem) => elem > 8);

// console.log(isBigger);
// console.log(someAreBigger);

//Знайдіть найбільший елемент у масиві.

// const findBiggestElem = (arr) => {
//     arr.sort((a, b) => b - a);
//     return arr[0];
// }

// console.log(findBiggestElem(arr));

// const maxNumber = Math.max(...arr);
// console.log(maxNumber)

// const str = 'Hello';
// console.log(...str)
