'use strict';

// for (let i = 0; i <= 5; i++) {
//     console.log(i);
// }

// let sum = 0;

// for (let i = 0; i <= 10; i++) {
//     sum += i;  
// }

// console.log(sum);

// ourLoop : for (let i = 0; i <= 5; i++) {
//     for (let j = 0; j <= 5; j++) {
//         console.log(i, j);

//         if (j === 2) {
//             break ourLoop;
//         }
//     }
// }

// for (let i = 0; i < 100; i += 2) {
//     console.log(i);
// }

// let x = 1;

// while (x < 5) {
//     console.log(x);
//     x++;
// }

// let isOk = null;

// while (isOk === 'OK') {
// isOk = prompt('Is it ok?', 'OK');
// }

// Використайте цикл "for", щоб вивести числа від 1 до 10.

// for (let i = 1; i <= 10; i++) {
//     console.log(i);
// }

// Виведіть парні числа від 2 до 20, використовуючи цикл "for".

// for (let i = 2; i <= 20; i += 2) {
//     console.log(i);

// }

// Використайте цикл "for", щоб вивести числа від 1 до 10.

// for (let i = 1; i <= 10; i++) {
//     console.log(i);
// }

// Виведіть парні числа від 2 до 20, використовуючи цикл "for".

// for (let i = 2; i <= 20; i += 2) {
//     console.log(i);
// }

// Виведіть числа у зворотньому порядку від 10 до 1, використовуючи цикл "for".

// for (let i = 10; i >= 1; i--) {
//     console.log(i);
// }

// Створіть цикл "for", щоб вивести таблицю множення на 5 до 50.

// for (let i = 1; i <= 50; i++) {

//     console.log(`5 * ${i} = ${5 * i}`);
// }

// Використовуйте цикл "while" для знаходження суми перших 10 натуральних чисел.

// let summ = 0;
// let x = 1;

// while (x <= 10) {

//     summ += x;
//     console.log(`${summ} = ${x} + ${summ}`);
//     console.log(summ)
//     x++;
//     // console.log(x)
// }

// console.log(summ);



// Реалізуйте гру вгадування числа випадкового числа від 1 до 10 
// за допомогою циклу "while", доки гравець не вгадає.


// let userNumber = null;
// let randomNumber = null;

// do {
//     userNumber = +prompt('Enter your number from 1 to 10');
//     randomNumber = parseInt(Math.random() * 10);

//     console.log('userNumber -', userNumber);
//     console.log('randomNumber - ', randomNumber);
//     console.log(userNumber !== randomNumber);

// } while (userNumber !== randomNumber);



// Використовуйте цикл "do-while", щоб вимагати введення користувача числа від 1 до 5. 
// Продовжуйте вимагати введення, доки не введено правильне число.


// let userNumber;

// do {
//     userNumber = +prompt('Enter your number');

//     console.log(typeof userNumber);

//     console.log(!(userNumber >= 1 && userNumber <= 5));

// } while (!(userNumber >= 1 && userNumber <= 5));






// Створіть просте меню за допомогою циклу "do-while". 
// Дозвольте користувачу вибрати опції, такі як '1. Друк', '2. Обчислити', '3. Вихід'. 
// Продовжуйте цикл, поки користувач не вибере '3'.



// Створіть патерн трикутника, використовуючи зірочки. 
// Перший рядок повинен мати одну зірочку, другий дві і так далі, 
// до п'яти рядків, використовуючи цикл "for".

// let string = '';

// for (let i = 0; i <= 5; i++) {

//     string += ' * ';
//     console.log(string);
// }




/**
 * Використайте вкладені цикли "for", щоб вивести таблицю множення від 1 до 5. 
 * Кожну таблицю слід розділити рядком.
 */


// for (let i = 1; i <= 5; i++) {

//     for (let j = 1; j <= 9; j++) {

//         console.log(`${i} * ${j} = ${i * j}`);
//     }

//     console.log('----------------');
// }


// Виведіть перші 5 простих чисел за допомогою циклу "while".

// let x = 1;

// while (x <= 5) {

//     console.log(x);
//     x++;
// }

// Використовуючи цикл "for", виведіть числа від 10 до 1 у спадному порядку.

// for (let i = 10; i >= 1; i--) {
//     console.log(i);
// }

// Виведіть кратні числа 3 від 3 до 30, використовуючи цикл "for".

// for (let i = 3; i <= 30; i += 3) {
//     console.log(i);
// }

/**
 * 
 * Створити програму, яка просить користувача ввести пароль. 
 * Використовуйте цикл do-while, щоб продовжувати запитувати пароль, 
 * доки користувач не введе вірний пароль.
 */

// const password = 123;

// let userPassword;

// do {
//     userPassword = +prompt('Enter your passord');

//     console.log(userPassword !== password);


// } while (userPassword !== password);

// Вивести таблицю множення для числа 5.

// let x = 1;
// let result;

// while (x <= 5) {

//     console.log(`5 * ${x} = ${5 * x}`);
//     x++;
// }


// Запитати у користувача ввести число. 
// Потім вивести суму всіх чисел від 1 до введеного числа.

// let userNumber;

// while (!userNumber || isNaN(userNumber)) {

//     userNumber = prompt('Enter your number');

//     let summ = null;

//     for (let i = 1; i <= userNumber; i++) {
//         summ += i;

//     }

//     console.log(summ);
// }




// Вивести всі парні числа від 2 до 20.

// let x = 2;

// while (x <= 20) {
//     console.log(x);
//     x += 2;

// }

// Знайти суму чисел від 1 до 100, які діляться на 3.

// let x = 1;
// let summ = null;

// while (x <= 100) {
//     x++;

//     if (x % 3 === 0) {
//         console.log(x);
//         summ += x;
//     }
// }

// console.log(summ);


// Запитати у користувача ввести пароль. Питати його, доки не введе правильний пароль "12345".

// const password = '1234';

// let userPassword;

// while (userPassword !== password) {
//     userPassword = prompt('Enter your password');

//     console.log(userPassword !== password);
// }





// Вивести числа від 10 до 1 у зворотньому порядку.

// let x = 10;

// while (x >= 1) {
//     console.log(x);
//     x--;
// }



// Створити гру "вгадай число" для чисел від 1 до 10. Питати, доки користувач не вгадає.

// let randomNumber = parseInt(Math.random() * 10);

// let userNumber;

// while (userNumber !== randomNumber) {
    
//     userNumber = +prompt('Enter your number');

//     console.log(`randomNumber - ${randomNumber}`);
//     console.log(`userNumber - ${userNumber}`);
//     console.log(userNumber !== randomNumber);
// }

// Знайти суму всіх парних чисел від 1 до 50.

// let x = 2;
// let summ = 0;

// while (x <= 10) {
//     console.log(x);
//     x += 2;
//     summ += x;
//     console.log('summ', summ, x)
// }

// console.log(summ)


