'use strict';

// const { log } = require("console");

// const counter = () => {
//     let x = 0;

//     return () => {
//         console.log(++x);
//     };
// }

// const x = counter();

// console.log(x)
// x()

// (() => {console.log('I')})()

// function sum(a) {
//     return function (b) {
//         return a + b;
//     }
// }

// // const sumA = sum(10);
// // console.log(sumA(10))
// console.log(sum(10)(20))


/*
Напишіть функцію changeTextSize, яка матиме такі аргументи:
1. Посилання на DOM-елемент, розмір тексту якого потрібно змінити без реєстрації та SMS.
3. Розмір у px, яку потрібно змінити текст, 
повертає функцію, змінює розмір тексту на задану величину.

За допомогою цієї функції створіть дві:
- одна збільшує текст на 2px від початкового;
- друга - зменшує на 3px.

Після чого повісьте отримані функції як обробники на кнопки з id="increase-text" 
та id="decrease-text".
*/

// const text = document.querySelector('#paragraph');
// const incrementBtn = document.querySelector('#increase-text');
// const decrementBtn = document.querySelector('#decrease-text');



// const changeTextSize = (el, size) => {

//     return () => {

//         el.style.fontSize = `${parseInt(el.style.fontSize) + size}px`;
//     };

// }

// const decrementSize = changeTextSize(text, -5);
// const incrementSize = changeTextSize(text, 5);

// incrementBtn.addEventListener('click', incrementSize);
// decrementBtn.addEventListener('click', decrementSize);






/*

Напишіть набір готових функцій-фільтрів для array.filter();

1. inBetween(a, b) – знаходиться між a та b (включно).
3. inArray([...]) – знаходиться у даному масиві [...].

 */

// const array = [1, 4, 3, 5, 5, 6, 6, 11, 3, 31];
//
// const inBetween = (a, b) => {};
// const inArray = (array) => {};

// Приклад використання: const filterBetween3and4 = inBetween(3, 4); array.filter(filterBetween3and4)
// Приклад використання: const inArray123 = inArray([1, 2, 3]); array.filter(inArray123)


// const array = [1, 3, 2, 4, 4, 5, 3, 6, 11, 2, 31];


// const inBetween = (a, b) => {
//     return (el, index) => {
//         return index >= a && index <= b;
//     }
// };

// const arr1to5 = inBetween(1, 5);

// console.log(array.filter(arr1to5))

// const inArray = (array) => {
//     return (el) => {
//         return array.includes(el);
//     }
// };

// const have123 = inArray([1, 2, 3]);

// console.log(array.filter(have123))

// try {
//     console.log(dddd)
// } catch(err) {
//     console.log(err)
// }

// console.log(new Error('fffff'))


/*
Дан масив кольорів у форматі HEX

Виведіть на екран палітру, що складається з:

<div class="item" >
     <span>#000000</span>
</div>

де #000000 це код кольору. 
Задати div бекграунд з цим кольором

Якщо код кольору не відповідає формату HEX – 
виведе в консоль помилку (в пмилці має бути 
зазначений неправильний код) та не малюйте 
блок у палітру;

P.S. Використовуйте try...catch, 
свій тип помилки успадкований від Error, всі інші 
помилки прокиньте далі.
*/

// const colors = [
//     '#92a8d1',
//     '#deeaee',
//     '#b1cbbb',
//     '#c94c4c',
//     '#f7cac9',
//     '#f7786b',
//     '#82gdfgb74b',
//     '#405d27',
//     '#405d271231',
//     '#b5e7a0',
//     '#eca1a6',
//     '#d64161',
//     '#b2ad7f',
//     '#6b5b95',
// ];



// const createElement = (color) => {

//     const divEl = `
//     <div class="item" style="background-color: ${color};">
//           <span>${color}</span>
//     </div>`;

//     return divEl;
// }

// const containerEl = document.querySelector('.colors-container');

// class ColorElement {
//     constructor(color) {

//         this.color = color;

//         if (color.length > 7) {
//             throw new HexColorError(this.color);  
//         }


//     }

//     createElement(container) {
//         container.insertAdjacentHTML('beforeend', `
//         <div class="item" style="background-color: ${this.color};">
//               <span>${this.color}</span>
//         </div>`
//         );
//     }
// }

// class HexColorError extends Error {
//     constructor(color) {
//         super(`Wrong HEX color: ${color}`);
//         this.name = 'HexColorError';
//     }
// }

// colors.forEach((el) => {

//     try {

//         new ColorElement(el).createElement(containerEl);

//     } catch (err) {

//         if (err.name === 'HexColorError') {
//             console.error(err)
//         } else {
//             throw err;
//         }
//     }
// });






/*
Напишіть клас, який приймає рядок з поштовим кодом і повертає об'єкт {zipCode: code}.

Допустимі формати поштового індексу:
12345 - 5 цифр

Якщо аргумент, переданий у конструктор ZipCode не збігається з жодним із цих форматів, 
має бути кинуто виняток з ім'ям InvalidZipCodeError та текстом помилки 
"Неправильний поштовий індекс ${zipCode}, має бути ...".

Виняток має успадковуватися від Error;

Замініть усі числа у масиві codes на екземпляри класу ZipCode за допомогою map.

Передбачте можливі помилки та обробіть їх у try...catch;

*/

// const codes = [11111, 32123, 123123123123, 12342, 23212];

// class InvalidZipCodeError extends Error {
//     constructor(zipCode) {
//         super(`Неправильний поштовий індекс ${zipCode}.`)
//         this.name = 'InvalidZipCodeError';
//     }
// };


// class ZipCode {
//     constructor(zipStr) {
//         this.zipStr = zipStr;

//         if (zipStr.toString().length > 5) {
//             throw new InvalidZipCodeError(zipStr);
//         }
//     }

//     zipObj = {};
//     list = document.createElement('ul');

//     createObj() {

//         const listEl = document.createElement('li');

//         this.zipObj[this.zipStr] = this.zipStr;

//         for (let elem in this.zipObj) {
//             listEl.innerText = elem;
//         }

//         this.list.append(listEl);
//         document.body.append(this.list);
//     }
// }

// codes.forEach(el => {

//     try {
//         new ZipCode(el).createObj();

//     } catch (err) {
//         console.log(err)
//     }
// });

// const getArray = () => {
//     return ['Hello', 'Inna', 'Vova'];
// }


// // const [greeting,...name] = ['Hello', 'Inna', 'Vova'];

// const [greeting = 'ola',...name] = getArray()
// // const name = getArray()
// console.log(greeting);
// console.log(name);

// const arr = [1, 2, 3];

// const [first, second, third] = arr;

// console.log(first);
// console.log(second);
// console.log(third);

// const [,,third] = arr;
// console.log(third);

// let a = 1;
// let b = 3;

// [b, a] = [a, b];

// console.log(a);
// console.log(b);

// const f = () => {
//     return {
//         first: 1,
//         second: 2,
//     }
// }

// const result = f();
// console.log(result.first);
// console.log(result.second);
// const f = () => {
//     return [1, 2]
// }

// const [a , b] = f();
// console.log(a);
// console.log(b);


// const f = () => [1, 2, 3, 4, 5, 6];

// const [a,,b,,,c] = f();

// console.log('a: ', a);
// console.log('b: ', b);
// console.log('c: ', c);

// const logArgs = function(first, second, ...args) {
//     // console.log(arguments)
//     console.log(first);
//     console.log(second);
//     console.log(args);
//   };

// logArgs(1, 2, 3, 4, 5); // [1, 3, 4, 5, 6]

// const array = [1, 2, 3, 4, 5, 6, 7];
// const [a, b, ...args] = array;

// console.log(a);
// console.log(b);
// console.log(args);



// const logArgs = function(a, b, c) {
//     console.log('a = ', a);
//     console.log('b = ', b);
//     console.log('c = ', c);
// };

// const arr = [1, 2, 3];

// logArgs(...arr)


// const arr = ['will', 'love'];
// const data = ['You', ...arr, 'spread', 'operator'];

// console.log(data)


// const array = ['Spread', 'Rest', 'Operator', [1, 2, 3]];

// const newArr = [...array];
// console.log(newArr)

// const aCollection = document.getElementsByTagName('a');
// console.log(aCollection);

// const arr = [...aCollection];
// console.log(arr)


/*
Напишіть код, який створює змінні з ім'ям людини,
 а також з професією, виводить запис 
 типу "userName is userProfession"; */

//  const user1 = 'Cristian Stuart; developer';
//  const user2 = 'Archibald Robbins; seaman';
//  const user3 = 'Zach Dunlap; lion hunter';
//  const user4 = 'Uwais Johnston; circus artist';

// const showProfession = (user) => {
//     const [name, job] = user.split('; ');
//     return `${name} is ${job}`;
// }
// console.log(showProfession(user1))


/*
Напишіть функцію, яка створює об'єкт.
Як аргументи вона приймає в себе ім'я, прізвище, та перелік рядків формату 
"ім'яВластивості: значення". Їх може бути багато.

Приклад роботи:

const user = createObject("Jhon", "Johnson", "age: 31", "wife: Marta", "dog: Bob");
=>
user = {
    name: "Jhon",
    lastName: "Johnson",
    age: "31",
    wife: "Marta",
    dog: "Bob",
}
 */

// const createObject = (name, lastName, ...args) => {
//     const newObj = {
//         name,
//         lastName, 
//     }

//     args.forEach(el => {
//         const [key, value] = el.split(': ');
//         newObj[key] = value;
//     })

//     console.log(newObj)
// };


// createObject("Shanon", "Kerr", "age: 24", "cat: Honey", "city: LA", 'dog: true')


/* У вас є 2 масиви - об'єднайте їх вміст в один максимально елегантним способом. */

// const fe30Part1 = [
//     'Агеєнко Антон',
//     'Возненко Маргарита',
//     'Голуб Микита',
//     'Денісів Денис',
//     'Дроженець Віталій',
//     'Златоуст Богдан',
//     'Кисельова Юлія',
//     'Костенко Віктор',
//     'Кострома Михайло',
//     'Кутузов Іван',
//     'Літіченко Іван',
//     'Літус Аліна',
// ];

// const fe30Part2 = [
//     'Маринич Антон',
//     'Мартян Олександр',
//     'Мінін Сергій',
//     'Новосьолов Олександр',
//     'Романовська Катерина',
//     'Самсонов Іван',
//     'Сапацинський Павло',
//     'Сердюк Олександр',
//     'Скриннік Олександр',
//     'Хоменко Віталій',
//     'Чевжик Антон',
//     'Шкуріна Катерина',
// ];
//  const newArr = [...fe30Part1, ...fe30Part2];
//  console.log(newArr);




/*
При натисканні на кнопку з класом button виведіть у p з id user-number 
номер елемента у списку меню, на який ми клацнули.
Використовуйте отримані знання:)
  */

// const p = document.querySelector('#user-number');
// const buttons = [...document.querySelectorAll('.button')];
// // console.log(buttons);


// document.querySelector('.menu').addEventListener('click', (event) => {

//     if (event.target === event.currentTarget) {
//         return;
//     } 

//     const index = buttons.findIndex((el) => {
//         return event.target === el;
//     })

//     console.log(index + 1)

//     p.innerText = index + 1;
// });

// const obj = {
//     p: 10,
//     o: 20,
// }

// const {o: twenty, p: ten} = obj;

// console.log(twenty)
// console.log(ten)


// const metadata = {
//     title: "Scratchpad",
//     translations: [
//        {
//         locale: "de",
//         localization_tags: [ ],
//         last_edit: "2014-04-14T08:43:37",
// //         url: "/de/docs/Tools/Scratchpad",
// //         title: "JavaScript-Umgebung"
// //        }
// //     ],
// //     url: "/en-US/docs/Tools/Scratchpad"
// // };

// // // const {title, url, translations: [{locale, url: secondUrl, title: secondTitle}]} = metadata;
// // const {title, url, translations} = metadata;

// // console.log(translations)

// // // const {locale, url: secondUrl, title: secondTitle} = translations[0]

// // const locale = translations?.[0]?.locale;
// // console.log(locale)
// // // console.log(url)



// const people = [
//     {
//         name: "Mike Smith",
//         family: {
//             mother: "Jane Smith",
//             father: "Harry Smith",
//             sister: "Samantha Smith"
//         },
//         age: 35
//     },
//     {
//         name: "Tom Jones",
//         family: {
//             mother: "Norah Jones",
//             father: "Richard Jones",
//             brother: "Howard Jones"
//         },
//         age: 25
//     }
// ];

// people.forEach(({name, age, family}) => {

//     const {father, mother, brother} = family;

//     // console.log(name, age, father, mother, brother)

// })

// const updateUser = ({name, age}) => {

//     console.log(name)
//     console.log(age)

// }

// updateUser(people[0])

// const object = {
//     name: 'John',
//     lastName: 'Johnson',
//     age: 24,
//     city: 'NYC'
// };

// const {name, age, ...other} = object;

// console.log(name);
// console.log(age);
// console.log(other);


// const props = {
//     a: 1,
//     b: 2,
//     name: 3,
// }

// const object = {
//     ...props,
//     name: 'John',
//     lastName: 'Johnson',
// }


// const newObj = {...object}
// console.log(newObj)


// const obj = {
//     name: "Mike Smith",
//     family: {
//         mother: "Jane Smith",
//         father: "Harry Smith",
//         sister: "Samantha Smith"
//     },
//     age: 35,
//     city: 'ddd',
// };

// const {family, city, ...shortObj} = obj; 
// console.log(shortObj)


/*
1. Виведіть у параграф з id = href рядок url
поточної сторінки у браузері;
2. Напишіть функціонал перезавантаження сторінки на
кліку на кнопку з id = reload;
*/

// console.log(location)

// const p = document.querySelector('#href');
// const button = document.querySelector('#reload');


// const {href} = location;

// console.log(href)

// p.innerText = href

// button.addEventListener('click', () => {
//     setTimeout(() => {
//         location.reload()
//     }, 2000)
// });


/*
По кліку на кнопку, виведете alert з текстом з input
*/

// const input = document.querySelector('#text');
// const button = document.querySelector('#alert-text');

// // button.addEventListener('click', () => {
// //     const { value } = input;
// //     alert(value)
// // });

// input.addEventListener('blur', ({target: {value}}) => {
//     // const { value } = target;
//     alert(value);
// })



/*
Напишіть функцію createUser, яка приймає великий об'єкт з 
полями користувача
і створює та повертає новий об'єкт згідно шаблону:
const newObject = {
     name: "Leanne Graham",
     email: "Sincere@april.biz",
     city: "Gwenborough",
     street: "Kulas Light",
     zipcode: "92998-3874",
}

Створіть з масиву users новий використовуючи функцію createUser;
  */

// const createObj = ({ name, email, address }) => {

//     const { city, street, zipcode } = address;

//     return {
//         name,
//         email,
//         city,
//         street,
//         zipcode,
//     }
// }

// const users = [{
//     id: 1,
//     name: "Leanne Graham",
//     username: "Bret",
//     email: "Sincere@april.biz",
//     address: {
//         street: "Kulas Light",
//         suite: "Apt. 556",
//         city: "Gwenborough",
//         zipcode: "92998-3874",
//         geo: { "lat": "-37.3159", "lng": "81.1496" }
//     },
//     phone: "1-770-736-8031 x56442",
//     website: "hildegard.org",
//     company: {
//         name: "Romaguera-Crona",
//         catchPhrase: "Multi-layered client-server neural-net",
//         bs: "harness real-time e-markets"
//     }
// }, {
//     id: 2,
//     name: "Ervin Howell",
//     username: "Antonette",
//     email: "Shanna@melissa.tv",
//     address: {
//         street: "Victor Plains",
//         suite: "Suite 879",
//         city: "Wisokyburgh",
//         zipcode: "90566-7771",
//         geo: { "lat": "-43.9509", "lng": "-34.4618" }
//     },
//     phone: "010-692-6593 x09125",
//     website: "anastasia.net",
//     company: {
//         name: "Deckow-Crist",
//         catchPhrase: "Proactive didactic contingency",
//         bs: "synergize scalable supply-chains"
//     }
// }, {
//     id: 3,
//     name: "Clementine Bauch",
//     username: "Samantha",
//     email: "Nathan@yesenia.net",
//     address: {
//         street: "Douglas Extension",
//         suite: "Suite 847",
//         city: "McKenziehaven",
//         zipcode: "59590-4157",
//         geo: { "lat": "-68.6102", "lng": "-47.0653" }
//     },
//     phone: "1-463-123-4447",
//     website: "ramiro.info",
//     company: {
//         name: "Romaguera-Jacobson",
//         catchPhrase: "Face to face bifurcated interface",
//         bs: "e-enable strategic applications"
//     }
// }, {
//     id: 4,
//     name: "Patricia Lebsack",
//     username: "Karianne",
//     email: "Julianne.OConner@kory.org",
//     address: {
//         street: "Hoeger Mall",
//         suite: "Apt. 692",
//         city: "South Elvis",
//         zipcode: "53919-4257",
//         geo: { "lat": "29.4572", "lng": "-164.2990" }
//     },
//     phone: "493-170-9623 x156",
//     website: "kale.biz",
//     company: {
//         name: "Robel-Corkery",
//         catchPhrase: "Multi-tiered zero tolerance productivity",
//         bs: "transition cutting-edge web services"
//     }
// }, {
//     id: 5,
//     name: "Chelsey Dietrich",
//     username: "Kamren",
//     email: "Lucio_Hettinger@annie.ca",
//     address: {
//         street: "Skiles Walks",
//         suite: "Suite 351",
//         city: "Roscoeview",
//         zipcode: "33263",
//         geo: { "lat": "-31.8129", "lng": "62.5342" }
//     },
//     phone: "(254)954-1289",
//     website: "demarco.info",
//     company: {
//         name: "Keebler LLC",
//         catchPhrase: "User-centric fault-tolerant solution",
//         bs: "revolutionize end-to-end systems"
//     }
// }, {
//     id: 6,
//     name: "Mrs. Dennis Schulist",
//     username: "Leopoldo_Corkery",
//     email: "Karley_Dach@jasper.info",
//     address: {
//         street: "Norberto Crossing",
//         suite: "Apt. 950",
//         city: "South Christy",
//         zipcode: "23505-1337",
//         geo: { "lat": "-71.4197", "lng": "71.7478" }
//     },
//     phone: "1-477-935-8478 x6430",
//     website: "ola.org",
//     company: {
//         name: "Considine-Lockman",
//         catchPhrase: "Synchronised bottom-line interface",
//         bs: "e-enable innovative applications"
//     }
// }, {
//     id: 7,
//     name: "Kurtis Weissnat",
//     username: "Elwyn.Skiles",
//     email: "Telly.Hoeger@billy.biz",
//     address: {
//         street: "Rex Trail",
//         suite: "Suite 280",
//         city: "Howemouth",
//         zipcode: "58804-1099",
//         geo: { "lat": "24.8918", "lng": "21.8984" }
//     },
//     phone: "210.067.6132",
//     website: "elvis.io",
//     company: {
//         name: "Johns Group",
//         catchPhrase: "Configurable multimedia task-force",
//         bs: "generate enterprise e-tailers"
//     }
// }, {
//     id: 8,
//     name: "Nicholas Runolfsdottir V",
//     username: "Maxime_Nienow",
//     email: "Sherwood@rosamond.me",
//     address: {
//         street: "Ellsworth Summit",
//         suite: "Suite 729",
//         city: "Aliyaview",
//         zipcode: "45169",
//         geo: { "lat": "-14.3990", "lng": "-120.7677" }
//     },
//     phone: "586.493.6943 x140",
//     website: "jacynthe.com",
//     company: {
//         name: "Abernathy Group",
//         catchPhrase: "Implemented secondary concept",
//         bs: "e-enable extensible e-tailers"
//     }
// }, {
//     id: 9,
//     name: "Glenna Reichert",
//     username: "Delphine",
//     email: "Chaim_McDermott@dana.io",
//     address: {
//         street: "Dayna Park",
//         suite: "Suite 449",
//         city: "Bartholomebury",
//         zipcode: "76495-3109",
//         geo: { "lat": "24.6463", "lng": "-168.8889" }
//     },
//     phone: "(775)976-6794 x41206",
//     website: "conrad.com",
//     company: {
//         name: "Yost and Sons",
//         catchPhrase: "Switchable contextually-based project",
//         bs: "aggregate real-time technologies"
//     }
// }, {
//     id: 10,
//     name: "Clementina DuBuque",
//     username: "Moriah.Stanton",
//     email: "Rey.Padberg@karina.biz",
//     address: {
//         street: "Kattie Turnpike",
//         suite: "Suite 198",
//         city: "Lebsackbury",
//         zipcode: "31428-2261",
//         geo: { "lat": "-38.2386", "lng": "57.2232" }
//     },
//     phone: "024-648-3804",
//     website: "ambrose.net",
//     company: {
//         name: "Hoeger LLC",
//         catchPhrase: "Centralized empowering task-force",
//         bs: "target end-to-end models"
//     }
// }]

// users.forEach(el => {
//     console.log(createObj(el))
// })



/*
Напишіть функцію, яка приймає 2 об'єкти - резюме і вакансію,
та повертає відсоток збігу необхідних навичок (скіллів).

Навичка збігається якщо ім'я скіла збігається з імен
у вакансії та необхідний досвід <= досвіду людини в цій навичці
  */
// const resume = {
//     name: "John",
//     lastName: "Johnson",
//     age: 29,
//     city: "Kyiv",
//     skills: [
//         {
//             name: "Vanilla JS",
//             practice: 5
//         },
//         {
//             name: "ES6",
//             practice: 3
//         },
//         {
//             name: "React + Redux",
//             practice: 1
//         },
//         {
//             name: "HTML4",
//             practice: 6
//         },
//         {
//             name: "CSS2",
//             practice: 6
//         }
//     ]
// };

// const vacancy = {
//     company: "DanIt",
//     location: "Kyiv",
//     skills: [
//         {
//             name: "Vanilla JS",
//             experience: 3
//         },
//         {
//             name: "ES6",
//             experience: 2
//         },
//         {
//             name: "React + Redux",
//             experience: 2
//         },
//         {
//             name: "HTML4",
//             experience: 2
//         },
//         {
//             name: "CSS2",
//             experience: 2
//         },
//         {
//             name: "HTML5",
//             experience: 2
//         },
//         {
//             name: "CSS3",
//             experience: 2
//         },
//         {
//             name: "AJAX",
//             experience: 2
//         },
//         {
//             name: "Webpack",
//             experience: 2
//         }
//     ]
// };

// const checkSkills = ({ skills: resumeSkills }, { skills: vacancySkills }) => {

//     let passedSkills = 0;

//     vacancySkills.forEach(({ name: vacancyName, experience: vacancyExperience }) => {

//         const matchedSkills = resumeSkills.find(({ name }) => name === vacancyName);

//         if (matchedSkills && matchedSkills.practice >= vacancyExperience) {
//             passedSkills++;
//         }
//     });

//     return `${parseInt((passedSkills/vacancySkills.length) * 100)} %`;
// }

// console.log(checkSkills(resume, vacancy))



// const promise = new Promise((resolve, reject) => {

//     // setTimeout(() => {
//     //     resolve('Yes');
//     // }, 3000);

//     const random = Math.random();

//     if (random > 0.5) {
//         resolve('Success');
//     } else {

//         reject(new Error(`Rejected: ${random}`));
//     }

// });

// console.log(promise);

// promise
//     .then((result) => {
//         return result.toUpperCase()
//     })
//     .then((value) => {
//        console.log( value.toLowerCase())
//     })
//     .catch((error) => {
//         console.log(error)
//     })
//     .finally(() => {
//         console.log('Any way')
//     })



// fetch('https://www.boredapi.com/api/activity/')
// .then(res => res.json())
// .then(data => {
//     console.log(data);
//     document.body.insertAdjacentHTML('beforeend', `<h1>${data.activity}</h1>`)
// })
// .catch((error) => {
//     console.log(error)
//     document.body.insertAdjacentHTML('beforeend', `<h1 style='color: red'>${error.message}</h1>`)
// });



// for (let i = 0; i <= 5; i++) {
//     fetch('https://www.boredapi.com/api/activity/')
//         .then(res => res.json())
//         .then(data => {
//             console.log(data);
//             document.body.insertAdjacentHTML('beforeend', `<h1>${data.activity}</h1>`)
//         })
//         .catch((error) => {
//             console.log(error)
//             document.body.insertAdjacentHTML('beforeend', `<h1 style='color: red'>${error.message}</h1>`)
//         });
// }


// const promiseArray = [
//     fetch('https://www.boredapi.com/api/activity/').then(res => res.json()),
//     fetch('https://www.boredapi.com/api/activity/').then(res => res.json()),
//     fetch('https://www.12boredapi.com/api/activity/').then(res => res.json()),
//     fetch('https://www.boredapi.com/api/activity/').then(res => res.json()),
//     fetch('https://www.boredapi.com/api/activity/').then(res => res.json()),
// ];

// console.log(promiseArray)

// Promise.all(promiseArray)
//     .then((data) => {
//         console.log(data.map(el => el.activity));
//     })
//     .catch((error) => {
//         console.warn(error)
//     })



// Promise.allSettled(promiseArray)
//     .then((data) => {
//         console.log(data.filter(el => el.status ==='fulfilled'));
//     })
//     .catch((error) => {
//         console.warn(error)
//     })


/**Даний масив dogs у якому зберігаються об'єкти собак. 
У полі url зберігається посилання на back який поверне JSON із src зображення собаки.

Отримайте всі зображення і малюйте об'єкти собак у '' у вигляді карток:

<div class="card">
     <img src="" alt="">
     <p>Name: </p>
     <p>Breed: </p>
</div>

Якщо за якимось посиланням буде помилка – картка не повинна малюватись. 
Усі інші мають бути відмальовані. */


// // dogs.forEach(el => {
// //     console.log(el.url)
// //     fetch(el.url).then(res => res.json())
// // })

// const container = document.querySelector('.container');


// class Dog {
//     constructor(name, breed, url) {
//         this.name = name;
//         this.breed = breed;
//         this.url = url;
//     }

//     render() {
//         // console.log(this.url)
//         container.insertAdjacentHTML('beforeend', `
//         <div class="card">
//             <img src="${this.url}" alt="">
//             <p>Name: ${this.name} </p>
//             <p>Breed: ${this.breed} </p>
//         </div>
//         `)
//     }

// }

// // dogs.forEach(el => {
// //     // new Dog(el).render()
// //     fetch(el.url).then(res => res.json())
// //     .then(data => {
// //         // console.log(data)
// //         new Dog(el.name, el.breed, data.message).render()
// //     })
// //     .catch(error => console.log(error))
// // })

// const promiseArr = dogs.map(el => {
//     return fetch(el.url).then(res => res.json())
// });

// console.log(promiseArr)

// // console.log(promiseArr)

// // Promise.all(promiseArr).then(data => {
// //     console.log(data)
// // }).catch(error => {
// //     console.log(error)
// // })


// Promise.allSettled(promiseArr)
//     .then(data => {

//         data.forEach(({ value }, index) => {
//             if (value) {
//                 new Dog(dogs[index].name, dogs[index].breed, value.message).render();
//             }
//         })
//     })
//     .catch(error => console.log(error))







// new Promise(executor(resolve, reject))
// const promise = new Promise((resolve, reject) => {
//     const random = Math.random();
//     setTimeout(() => {
//         if (random > 0.5) {
//             resolve('Success');
//         } else {
//             reject(new Error(`Fail: ${random}`));
//         }
//     }, 1000);
// });


// promise
//     .then((result) => {
//         return result;
//     })
//     .then((value) => {
//         console.warn(value)
//     })
//     .catch((error) => {
//         console.log(error);
//     })
//     .finally(() => {
//         console.log('Nice');
//     })



// fetch('https://www.boredapi.com/api/activity')
//     .then(res => res.json())
//     .then((data) => {
//         console.log(data)
//         document.body.insertAdjacentHTML('beforeend', `<h1>${data.activity}</h1>`)
//     })
//     .catch((error) => {
//         document.body.insertAdjacentHTML('beforeend', `<h1 style='color: red'>${error.message}</h1>`)
//     })

// const dogs = [
//     {
//         name: 'Bob',
//         breed: 'hound',
//         url: 'https://dog.ceo/api/breed/hound/images/random'
//     }, {
//         name: 'Honey',
//         breed: 'husky',
//         url: 'https://dsdfsdfog.ceo/api/breed/husky/images/random'
//     }, {
//         name: 'John',
//         breed: 'ovcharka',
//         url: 'https://dog.ceo/api/breed/ovcharka/images/random'
//     }, {
//         name: 'Sweaty',
//         breed: 'germanshepherd',
//         url: 'https://dog.ceo/api/breed/germanshepherd/images/random'
//     }, {
//         name: 'Sam',
//         breed: 'germanshepherd',
//         url: 'https://dog.ceo/api/breed/germanshepherd/images/random'
//     }, {
//         name: 'Conor',
//         breed: 'corgi',
//         url: 'https://dog.ceo/api/breed/hound/images/random'
//     }, {
//         name: 'Ivan',
//         breed: 'ovcharka',
//         url: 'https://dog.ceo/api/breed/ovcharka/images/random'
//     }, {
//         name: 'Kate',
//         breed: 'whippet',
//         url: 'https://dog.ceo/api/breed/whippet/images/random'
//     }, {
//         name: 'Adam',
//         breed: 'husky',
//         url: 'https://dog.ceo/api/breed/husky/images/random'
//     },
// ];

// const promiseArr = [
//     fetch('https://www.boredapi.com/api/activity').then(res => res.json()),
//     fetch('https://www.boredapi.com/api/activity').then(res => res.json()),
//     fetch('https://www.borffedapi.com/api/activity').then(res => res.json()),
//     fetch('https://www.boredapi.com/api/activity').then(res => res.json()),
//     fetch('https://www.boredapi.com/api/activity').then(res => res.json()),
// ];

// Promise.all(promiseArr)
//     .then((data) => {
//         data.map(el => document.body.insertAdjacentHTML('beforeend', `<h1>${el.activity}</h1>`));
//     })
//     .catch((error) => {
//         console.log(error)
//     })

// Promise.allSettled(promiseArr)
//     .then((data) => {
//         console.log(data.filter(el => el.status === 'fulfilled'))
//         console.log(data.filter(el => el.status === 'rejected'));
//     })
//     .catch((error) => {
//         console.log(error)
//     })

/**Даний масив dogs у якому зберігаються об'єкти собак. 
У полі url зберігається посилання на back який поверне JSON із src зображення собаки.

Отримайте всі зображення і малюйте об'єкти собак у '' у вигляді карток:

<div class="card">
 <img src="" alt="">
 <p>Name: </p>
 <p>Breed: </p>
</div>

Якщо за якимось посиланням буде помилка – картка не повинна малюватись. 
Усі інші мають бути відмальовані. */

// const container = document.querySelector('.container');
// class Dog {
//     constructor(name, breed, url) {
//         this.name = name
//         this.breed = breed
//         this.url = url
//     }

//     render() {
//         container.insertAdjacentHTML('beforeend', `
//         <div class="card">
//              <img src="${this.url}" alt="">
//              <p>Name: ${this.name} </p>
//              <p>Breed: ${this.breed} </p>
//         </div>
//     `)
//     }
// }

// const dogsArr = dogs.map(el => {
//     return fe;tch(el.url).then(res => res.json());;
// })

// Promise.allSettled(dogsArr)
//     .then(data => {

//         data.forEach(({ value }, index) => {

//             if (value) {

//                 new Dog(dogs[index].name, dogs[index].breed, value.message).render();
//             }
//         })
//     })
//     .catch(err => new Error(`Error: ${err}`));




// fetch('https://dog.ceo/api/breeds/image/random').then(res => res.json())
// .then(value => {
// console.log(value)
// });

// const arr = [
//     fetch('https://dog.ceo/api/breeds/image/random').then(res => res.json()),
//     fetch('https://dog.ceo/api/breeds/image/random').then(res => res.json()),
//     fetch('https://dog.ceo/api/breeds/image/random').then(res => res.json()),
//     fetch('https://dog.ceo/api/breeds/image/random').then(res => res.json()),
//     fetch('https://dog.ceo/api/breeds/image/random').then(res => res.json()),
//     fetch('https://dog.ceo/api/breeds/image/random').then(res => res.json()),
//     fetch('https://dog.ceo/api/breeds/image/random').then(res => res.json()),
//     fetch('https://dog.ceo/api/breeds/image/random').then(res => res.json()),
//     fetch('https://dog.ceo/api/breeds/image/random').then(res => res.json()),
//     fetch('https://dog.ceo/api/breeds/image/random').then(res => res.json()),
//     fetch('https://dog.ceo/api/breeds/image/random').then(res => res.json()),
//     fetch('https://dog.ceo/api/breeds/image/random').then(res => res.json()),
//     fetch('https://dog.ceo/api/breeds/image/random').then(res => res.json()),
//     fetch('https://dog.ceo/api/breeds/image/random').then(res => res.json()),
//     fetch('https://dog.ceo/api/breeds/image/random').then(res => res.json()),
//     fetch('https://dog.ceo/api/breeds/image/random').then(res => res.json()),
// ];

// Promise.allSettled(arr)
//     .then((result) => {

//         result.forEach(({ value }) => {

//             container.insertAdjacentHTML('beforeend', `
//                  <div class="card">
//                      <img src="${value.message}" alt="">
//                  </div>`
//         )})
//     })
//     .catch(err => {
//         console.log(err)
//     })


// const container = document.querySelector('.container')

// fetch('https://official-joke-api.appspot.com/random_ten')
//   .then(res => res.json())
//   .then(data => {
//     data.forEach(el => {

//       container.insertAdjacentHTML('beforeend', `
//            <h1>${el.setup}</h1>
//            <p>${el.punchline}</p>`
//       )
//     });
//   })
//   .catch(err => {
//     console.log(err)
//   })
//   .finally(() => {
//     console.log('Completed')
//   })

//Create a promise that resolves after a delay of 2 seconds 
//and logs "Promise resolved!" to the console.

// const promise = new Promise((resolve, reject) => {

//   setTimeout(() => {
//     resolve('Promise resolved!')
//   }, 2000);

// });

// promise.then((result) => {
//   console.log(result)
// })

//---------------------------------------------------------


// const xhr = new XMLHttpRequest();
// // console.log(xhr)

// xhr.open('GET', 'https://www.documents.philips.com/assets/20230303/03110b04c0734cbf81fdafba01335141.pdf');

// xhr.send();

// xhr.onload = () => {
//   const data = JSON.parse(xhr.response);
//   // document.write(data.fact)
// }
// xhr.onerror = () => {
//   console.log(xhr)
// }

// const bar = document.querySelector('.bar');

// xhr.onprogress = (event) => {
//   // event.loaded - кількість завантажених байт
//   // event.lengthComputable = true, якщо сервер надсилає заголовок Content-Length
//   // event.total - кількість байт всього (тільки якщо lengthComputable дорівнює true)

//  console.log(event.loaded)
//  console.log(event.lengthComputable)
//  console.log(event.total)


//   // if (event.lengthComputable) {
//   //     console.log(`Отримано ${event.loaded} з ${event.total} байт`);
//   // } else {
//   //     console.log(`Отримано ${event.loaded} байт`); // якщо відповіді немає заголовка Content-Length
//   // }

//   if (event.lengthComputable) {
//       const loaded = (event.loaded / event.total) * 100;
//       console.log(loaded)
//       bar.style.width = `${loaded}%`
//   }
// };

/*

# Пошук університетів?

По кліку на кнопку зчитувати країну, яка обрана в селекті і 
відправляти `GET` запит
на `http://universities.hipolabs.com/search?country=${country}`.

Малювати в `ul` посилання на сайт кожного університету.

Поки йде завантаження даних - показуйте прелоадер (можна просто 
слово Loading...)

*/

// const url = 'http://universities.hipolabs.com/search?country=Ukraine';


// const select = document.querySelector('#country');
// const btn = document.querySelector('button');
// const ul = document.querySelector('ul');


// btn.addEventListener('click', (event) => {
//   console.log(select.value)
//   const xhr = new XMLHttpRequest();
//   xhr.open('GET', `http://universities.hipolabs.com/ssdfearch?country=${select.value}`);
//   xhr.send();
//   ul.innerHTML = 'Loading...'

//   xhr.onload = () => {
//     const response = JSON.parse(xhr.response);
//     console.log(response)

//     response.forEach(({ name, web_pages }) => {
//       ul.insertAdjacentHTML('beforeend', `
//       <ul><a href="${web_pages}">${name}</a></ul>
//       `)
//     });
//   }


//   xhr.onerror = (event) => {
//     ul.innerText = `ERROR: ${xhr.status}: ${xhr.statusText}`
//   }
// });


/**
 * 
Отримати список зображень по 
https://ajax.test-danit.com/api/json/photos 
та відмалювати їх у контейнер id якого дорівнює id альбому зображення.
Формат картки:


<div class="card">
     <img src="" alt="">
     <span>Title</span>
</div> 
*/


// fetch('https://ajax.test-danit.com/api/json/photos')
//   .then(res => res.json())
//   .then(data => {

//     const filteredData = data.filter(el => el.albumId <= 10);

//     filteredData.forEach(({albumId, thumbnailUrl, title}) => {
//       document.querySelector(`#container_${albumId}`).insertAdjacentHTML('beforeend', `
//           <div class="card">
//                <img src="${thumbnailUrl}" alt="">
//                <span>${title}</span>
//           </div> 
//           `);
//     });
//   })
//   .catch(err => {
//     console.log(err)
//   })



















// const url = 'https://ajax.test-danit.com/api/json/posts'

// HTTP METHODS:

// ---GET ------------------------------------------


// fetch('https://ajax.test-danit.com/api/json/posts')
// .then(res => res.json())
// .then(data => console.log(data))

// axios.get(url)
//   .then(({ data, status }) => {
//     console.log(data)
//     console.log(status)
//   })

// ---POST --- СТВОРИТИ------------------------------------------

// const newPost = {
//   title: '1111',
//   body: '2222'
// }

// fetch(url, {
//   method: 'POST',
//   body: JSON.stringify(newPost),
//   headers: {
//     'content-type': 'application/json'
//   }
// })
//   .then(res => res.json())
//   .then(data => console.log(data))


// axios.post(url, newPost)
// .then(({data}) => console.log(data))




// ---PUT --- ПЕРЕЗАПИСАТИ ВСЕ перезаписує все, навіть якщо міняємо один параметр------------------------------------------


// fetch(`${url}/1`, {
//   method: 'PUT',
//   body: JSON.stringify(newPost),
//   headers: {
//     'content-type': 'application/json'
//   }
// })
//   .then(res => res.json())
//   .then(data => console.log(data))

// axios.put(`${url}/1`, {title: 'hello'}).then(({ data }) => console.log(data))




// ---PATCH --- ПЕРЕЗАПИСАТИ ЧАСТИНУ може перезаписати один параметр------------------------------------------

// fetch(`${url}/1`, {
//   method: 'PATCH',
//   body: JSON.stringify({title: 'hello'}),
//   headers: {
//     'content-type': 'application/json'
//   }
// })
//   .then(res => res.json())
//   .then(data => console.log(data))


// axios.patch(`${url}/1`, {title: 'hello'}).then(({ data }) => console.log(data))


// ---DELETE --- ВИДАЛИТИ------------------------------------------

// fetch(`${url}/99`, {
//   method: 'DELETE',
// })
//   .then(data => console.log(data))



// axios.delete(`${url}/99`).then(({ data }) => console.log(data))



// const postsInstance = axios.create({
//   baseURL: 'https://ajax.test-danit.com/api/json',
//   headers: {
//     'API-CODE': '123'
//   }
// });

// postsInstance.get('/posts').then(({data}) => console.log(data))
// postsInstance.get('/users').then(({data}) => console.log(data))
// postsInstance.get('/comments').then(({data}) => console.log(data))


// const f = async () => {

//   try {

//     const result = await fetch('https://catfact.ninja/fact');
//     const data = await result.json();
//     console.log(data)

//   } catch (err) {
//     console.log(err)
//   }

// }

// f()


// (async () => {
//   try {

//     const result = await fetch('https://catfact.ninja/fact');
//     const data = await result.json();
//     console.log(data)

//   } catch (err) {
//     console.log(err)
//   }
// })()


// class Card {
//   constructor(user) {
//     this.user = user;

//   }

//   div = document.createElement('div');


//   render() {
//     const { picture, name, gender, location, cell, email } = this.user[0];

//     this.div.className = 'container';

//     this.div.insertAdjacentHTML('beforeend', `
//     <img src="${picture.medium}" alt="${name.first}">
//      <span>Name: ${name.first} ${name.last}</span>
//      <span>Gender: ${gender}</span>
//      <span>City: ${location.city}</span>
//      <a href="tel:${cell}">Tel: ${cell}</a>
//      <a href="mailto:${email}">Tel: ${email}</a>
//     `)
//     document.querySelector('.container').append(this.div)
//   }
// }

// const userAdapter = (user) => {
//   return {}
// }


// const url = 'https://randomuser.me/api';

// const getUserData = async () => {

//   try {
//     const user = await axios.get(url)
//     const { data } = await user;
//     // 
//     return data;

//   } catch (err) {
//     console.log(err)
//   }
// }



// ( async () => {
//   const user = await getUserData();
//   console.log(user.results[0])

//   new Card(user.results).render()

// })()





// fetch("https://ajax.test-danit.com/api/swapi/films")
//   .then(res => res.json())
//   .then(data => {

//     data.forEach(({ episodeId, name, openingCrawl, characters }) => {
//       const card = document.createElement("div");
//       card.className = "card"

//       const filmId = document.createElement("span");
//       const filmName = document.createElement("h1");
//       const summary = document.createElement("p");
//       const list = document.createElement("ul");

//       filmId.innerText = `Episode ${episodeId}`;
//       filmName.innerText = `${name}`;
//       summary.innerText = `${openingCrawl}`;
//       list.innerText = "Characters:"

//       card.append(filmId, filmName, summary, list);
//       document.body.append(card);





//       characters.forEach(character => {
//         fetch(character)
//           .then(res => res.json())

//           .then(({ name }) => {

//             const li = document.createElement("li");
//             li.innerText = `${name}`;
//             card.append(li)
//           })
//       })
//     })
//   })


// class Card {
//   constructor(user) {
//     this.user = user;
//   }

//   div = document.createElement('div');

//   render() {
//     this.div.className = 'card';

//     this.div.insertAdjacentHTML('beforeend', `
//     <img src="${picture}" alt="${name}">
//     <span>Name: ${fullname}</span>
//     <span>Gender: ${gender}</span>
//     <span>City: ${city}</span>
//     <span>Tel: ${phone}</span>
//     <span>Email: ${email}</span>

//     `);

//     document.querySelector('.container').append(this.div)
//   }

// }









// const f = async () => {
//   // const res = await axios.get('https://catfact.ninja/fact');
//   try {

//     const res = await fetch('https://catfact.ninja/fact');
//     const data = await res.json();

//     console.log(data);

//   } catch (err) {
//     console.log(err)
//   }
// }

// f()


// const b = async () => {
//   const arr = [
//     axios.get('https://catfact.ninja/fact'),
//     axios.get('https://catfact.ninja/fact'),
//   ]

//   try {
//     const res = await Promise.all(arr);
//     console.log(res)

//   } catch (err) {
//     console.log(err)
//   }


// }

// b();


// (async () => {
//   const arr = [
//     axios.get('https://catfact.ninja/fact'),
//     axios.get('https://catfact.ninja/fact'),
//   ]

//   try {
//     const res = await Promise.all(arr);
//     console.log(res)

//   } catch (err) {
//     console.log(err)
//   }
// })()



// const url = 'https://randomuser.me/api';

// const container = document.querySelector('.container');

// const getUser = async () => {
//   try {
//     const res = await axios.get(url);
//     return res;
//   } catch (err) {
//     console.log(err)
//   }
// }

// class User {
//   constructor(user) {
//     this.user = user;
//   }

//   render() {
//     const { picture, name, fullName, gender, city, phone, email } = this.user;

//     container.insertAdjacentHTML('beforeend', `
//       <div class="user container">
//           <img src="${picture}" alt="${name}">
//           <span>Name: ${fullName}</span>
//           <span>Gender: ${gender}</span>
//           <span>City: ${city}</span>
//           <a href='tel:${phone}'>Tel: ${phone}</a>
//           <a href='mailto:${email}'>Email: ${email}</a>
//       </div>
// `)
//   }
// }

// const userAdapter = ( {data} ) => {

//   const [user] = data.results;

//   return {
//     picture: user.picture.medium,
//     name: user.name.first,
//     fullName: `${user.name.first} ${user.name.last}`,
//     gender: user.gender,
//     city: user.location.city,
//     phone: user.cell,
//     email: user.email
//   }
// }

// for (let i = 0; i <= 5; i++) {
//   (async () => {
//     const user = await getUser()

//     new User(userAdapter(user)).render()
//   })()
// }

const select = document.querySelector('#activity');
const time = document.querySelector('#time');
const weight = document.querySelector('#weight');
const form = document.querySelector('#activity-form');

const instance = axios.create({
  baseURL: 'https://fitness-calculator.p.rapidapi.com',
  headers: {
    'x-rapidapi-host': 'fitness-calculator.p.rapidapi.com',
    'x-rapidapi-key': 'ab0637da31msh0652f644f41a177p1a4e5cjsn0c04c39f4fb8'
  }

});

select.disabled = true;

const getActivities = async () => {

  try {
    const {status, data: { data } } = await instance.get('/activities',
      { params: { intensitylevel: '1' } })

    const activitiesArr = data.map(({ activity, description, id }) => `
        <option value="${id}" selected>${activity} - ${description}</option>`);

    select.insertAdjacentHTML('beforeend', activitiesArr.join(''));
    
    if (status === 200) {
      select.disabled = false;
    }

  } catch (err) {

    console.log(err)
  }

}

getActivities();

const button = document.querySelector('#get-c');


const getCalories = async (event) => {
  event.preventDefault()

  try {

    const {data: {data}} = await instance.get('/burnedcalorie', 
    {params: {activityid: select.value, activitymin: time.value, weight: weight.value}});
    
alert(data.burnedCalorie)
console.log(data)

  } catch ({response: { data }}) {
    console.log(data.errors)
  }
}


form.addEventListener('submit', getCalories)






















