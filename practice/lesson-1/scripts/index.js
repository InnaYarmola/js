
/**
 *  Створити змінну, записати в неї число, вивести значення цієї змінної в консоль.
 */

// const a = 5;
// console.log(a);

/**
 * Створити змінну за допомогою ключового слова const, записати до неї число.
 * Присвоїти цій змінній нове число.
 */


// let b = 12;
// b = 13;
// console.log(b);

/**
 * Записати в змінну '123', вивести в консоль тип цієї змінної.
 * Перетворити цю змінну на тип number за допомогою parseInt(), parseFloat(), унарний плюс +
 * Після цього повторно вивести в консоль тип цієї змінної.
 */

// let myNumber = '123';
// console.log(typeof myNumber);
// myNumber = parseInt(myNumber);
// console.log(typeof myNumber);

/**
 * Вивести на екран повідомлення з текстом Hello! This is alert за допомогою модального вікна alert.
 */

// alert('Hello! This is alert');


/**
 * Вивести на екран модальне вікно prompt із повідомленням "Enter the number".
 * Результат виконання модального вікна записати до змінної, значення якої вивести в консоль.
 */

// const userNumber = prompt('Enter your number');
// console.log(userNumber);


/**
 * За допомогою модального вікна prompt отримати від користувача два числа.
 * Вивести в консоль суму, різницю, множення, результат поділу та залишок від поділу їх один на одного.
 */

// const a = +prompt('number 1');
// const b = +prompt('number 2');
// console.log(`a + b = ${a + b}.`);
// console.log(`a - b = ${a - b}.`);
// console.log(`a * b = ${a * b}.`);
// console.log(`a / b = ${a / b}.`);
// console.log(`a % b = ${a % b}.`);


console.log()
console.log(1 ?? null )
console.log(Math.pow(2, 10))