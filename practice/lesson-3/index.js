"use strict";

// || - оператор АБО

// console.log(true || true);
// console.log(true || false);
// console.log(false || true);
// console.log(false || false);

// console.log(Boolean(0))
// console.log(Boolean(1))
// console.log(Boolean(''))
// console.log(Boolean('text'))
// console.log(Boolean('0'))
// console.log(Boolean(' '))
// console.log(Boolean(null))
// console.log(Boolean(undefined))
// console.log(Boolean(NaN))

// let currentUser = null;
// let defaultUser = "John";

// let name = currentUser || defaultUser || "unnamed";
// console.log(name);

// let name;

// if (currentUser) {

//     name = currentUser;

// } else if (defaultUser) {

//     name = defaultUser;

// } else {

//     name = 'unnamed';
// }

// console.log(name);
// const name = 'John'
// const age = 29;

// if (name === 'John' || age === 29) {
//     console.log('ok');
// }

// let x;
// let number;
// console.log(number || (x = 1));
// console.log(x);



// && - оператор i

// console.log( true && true );
// console.log(false && true);
// console.log( true && false );
// console.log(false && false);

// const name = 'John'
// const age = 29;

// if (name === 'John' && age === 29) {

//     console.log('ok');
// }

// let currentUser = null;
// let defaultUser = 'John';

// let name = currentUser && defaultUser && "unnamed";
// console.log(name);

// const message = prompt('Your message');

// if (!message) {
// console.log('No message');
// }

// let firstName = null;
// let lastName = 'John';

// console.log(lastName ? lastName : firstName);

// const answer = prompt('Яка “офіційна” назва JavaScript?');

// if (answer === 'ECMAScript') {
//     console.log('Правильно!');
// } else {

//     console.log('Ви не знаєте? ECMAScript!');
// }

// const number = prompt('Enter your number!');

// if (number > 0) {
//     console.log(1);
// } else if (number < 0) {
//     console.log(-1);
// } else {
//     console.log(0);
// }

// const a = 2;
// const b = 1;
// let result;
// console.log(result = (a + b < 4) ? 'Нижче' : 'Вище');


// let login;
// let message;

// login === 'Працівник' ? message = 'Привіт' :
// login === 'Директор' ? message = 'Вітаю' :
// login === '' ? message = 'Немає логіну' :
// message = '';

// console.log(message);


// let a = 3;

// switch (a) {
//   case 4:
//     alert('Вірно!');
//     break;

//   case 3: // (*) групуємо два блоки `case`
//   case 5:
//     alert('Невірно!');
//     alert("Можливо вам варто відвідати урок математики?");
//     break;

//   default:
//     alert('Результат виглядає дивно. Дійсно.');
// }

// let browser;

// if (browser === 'Edge') {
//     console.log("You've got the Edge!");

// } else if (browser === 'Chrome'
//     || browser === 'Firefox'
//     || browser === 'Safari'
//     || browser === 'Opera') {

//     console.log('Ми підтримуємо і ці браузери');

// } else {
//     console.log('Маємо надію, що ця сторінка виглядає добре!');
// }


// let a = +prompt('a?', '');

// switch (a) {
//     case 0:
//         console.log(0);
//         break;

//     case 1:
//         console.log(1)
//         break;

//     case 2:
//     case 3:
//         console.log('2,3');
//         break;

//     default:
//         console.log('No match');


// } 

// const userName = prompt('Your name');

// switch (userName) {
//     case 'John': {
//         console.log('Hi, John!');
//         break;
//     }
//     case 'Bob': {
//         console.log('Hi, Bob!');
//         break;
//     }
//     case 'Sam': {
//         console.log('I dont know you!');
//         break;
//     }
//     case 'Ben': {
//         console.log('Hello, mr.Ben');
//         break;
//     }

//     default: {
//         console.log('No name');
//     }

// }

// Вправа 1: Основні оператори if-else

// Напишіть програму, яка приймає вік користувача 
// як вхідні дані та 
// перевіряє, чи має він право голосу. Якщо 
// користувачу 18 років або більше, 
// відобразіть «Ви маєте право голосувати». 
// В іншому випадку відобразіть 
// «Ви не маєте права голосувати».

// let age = prompt('Your age');

// if (!age || isNaN(age)) {
//     console.log('Ви ввели не число');

// } else if (age < 0 || age > 110) {
//     console.log('Not valid');

// } else if (age >= 18) {
//     console.log('Ви маєте право голосувати');

// } else {
//     console.log('Ви не маєте права голосувати');
// }



// Вправа 4: Switch оператор для днів

// Створіть програму, яка просить користувача 
// ввести число від 1 до 7. Використовуйте оператор
//  switch, щоб відобразити відповідний день тижня. 
//  Наприклад, якщо користувач вводить 3, програма
//  повинна вивести «середа».

// const number = prompt('Enter number from 1 to 7');

// const isNotNumber = !number || isNaN(number);
// const isInRange = number >= 1 && number <= 7;

// if (!isNotNumber && isInRange) {

//     switch (number) {
//         case '1': {
//             console.log('Monday');
//             break;
//         }

//         case '': {
//             console.log('Tuesday');
//             break;
//         }

//         case '3': {
//             console.log('Wednesday');
//             break;
//         }

//         case '4': {
//             console.log('Thursday');
//             break;
//         }

//         case '5': {
//             console.log('Friday');
//             break;
//         }

//         case '6': {
//             console.log('Saturday');
//             break;
//         }

//         case '7': {
//             console.log('Sunday');
//             break;
//         }
//     }
// }


// Вправа 8: Перевірка високосного року

// Створіть програму, яка пропонує користувачеві ввести 
// рік, а потім визначає, чи є він високосним. Розглянемо 
// правила для високосних років (які діляться на 4, 
//     за винятком років, які діляться на 100, але 
//     не на 400).

// const userYear = +prompt('Enter your year');

// // const isNotNumber = !number || isNaN(number);

// const isIntercalaryYear = (userYear % 4 === 0 && userYear % 100 !== 0) || userYear % 400 === 0;

// if (isIntercalaryYear) {
//     alert('This is an intercalary year');

// } else {
//     alert('This is a regular year');
// }



// Вправа 2: Вкладені оператори if-else

// Створіть програму, яка просить користувача 
// ввести два числа. Перевірте, чи перше число ділиться 
// навпіл на друге. Якщо так, відобразіть повідомлення 
// про це; інакше повідомте користувача, що числа не 
// діляться рівномірно.

// const a = prompt("number 1");
// const b = prompt("number 2");



// if ( a % b === 0) {
//     console.log('Good');
// } else {
//     console.log('Not good');
// }

// --------------------------------------------------------------------

/**
 * Вправа 3: Кілька умов із if-else
 * 
Напишіть програму, яка приймає число як вхідні дані 
та перевіряє, чи є воно додатним, від’ємним чи нулем. 
Відобразіть результат відповідно.
 */

// const number = prompt('Enter your number');
// let result;

// if (number && !isNaN(number)) {
//     result = number > 0 ? 'Positive number' :
//     number < 0 ? 'Negative number' : 
//     'Your number is zero';  
// }

// console.log(result);

// ---------------------------------------------------------------------

/**
 * Вправа 5: Switch оператор для оцінок
Напишіть програму, яка пропонує користувачеві 
ввести оцінку (A, B, C, D або F), а потім 
використовує оператор switch для відображення 
повідомлення на основі оцінки. Наприклад, якщо 
користувач вводить "B", програма повинна вивести 
"Гарна робота!"
 */

// const mark = prompt('Enter your mark');

// if (mark) {
//     switch (mark) {
//         case 'A': {
//             console.log('Your mark is A.');
//             break;
//         }
//         case 'B': {
//             console.log('Your mark is B.');
//             break;
//         }
//         case 'C': {
//             console.log('Your mark is C.');
//             break;
//         }
//         case 'D': {
//             console.log('Your mark is D.');
//             break;
//         }
//         case 'E': {
//             console.log('Your mark is E.');
//             break;
//         }
//         default: {
//             console.log('Your mark is not valid');
//         }
//     }
// }

// -----------------------------------------------------------------------


/**
 * Вправа 7: Температурна класифікація
 * 
Напишіть програму, яка приймає температуру як вхідні 
дані та класифікує її як гарячу, теплу, прохолодну або 
холодну за допомогою операторів if-else. Визначте температурні 
діапазони для кожної категорії на основі ваших уподобань.
 */

// const temperature = prompt('Enter outside temperature');

// if (temperature && !isNaN(temperature)) {

//     if (temperature >= 27) {
//         console.log('It is damn hot today!');
//     }

//     else if (temperature <= 26 && temperature > 15) {
//         console.log('Perfect temperature!');
//     }

//     else if (temperature <= 14 && temperature > 0) {
//         console.log('It is good temperature.');
//     }

//     else if (temperature <= -1 && temperature > -10) {
//         console.log('Perfect winter temperature!');
//     }

//     else if (temperature <= -10) {
//         console.log('It is damn cold today!');
//     }
// }

/**
 * Вправа 10: Привітання з часом
 * 
Напишіть програму, яка приймає поточний час як 
вхідні дані (у 24-годинному форматі) і виводить 
вітальне повідомлення на основі часу доби. Використовуйте 
оператори if-else, щоб розрізняти ранок, день, вечір і ніч.
 */


// const time = prompt('What time is this?');

// if (time && !isNaN(time)) {

//     if (time >= 6 && time < 12) {
//         console.log('Good morning!');

//     } else if (time >= 12 && time < 18) {
//         console.log('Good afternoon!');

//     } else if (time >= 18 && time < 21) {
//         console.log('Good evening!');

//     } else if (time >= 21 && time <= 24 ) {
//         console.log('Good night!');

//     } else if (time >= 0 && time < 6 ) {
//         console.log('Good night!');
//     }
// }

/**
 * Вправа 12: Система оцінювання
Розширити вправу системи оцінювання. Окрім відображення 
повідомлення на основі оцінки, також включіть числовий 
діапазон для кожної оцінки (наприклад, A: 90-100, B: 80-89 тощо).
 */

// let mark = prompt('Enter your mark');

// if (mark && !isNaN(mark)) {

//     if (mark <= 100 && mark >= 90) {
//         mark = 'A';

//     } else if (mark <= 89 && mark >= 80) {
//         mark = 'B';

//     } else if (mark <= 79 && mark >= 60) {
//         mark = 'C';

//     } if (mark <= 59 && mark >= 40) {
//         mark = 'D';

//     } else if (mark <= 39 && mark >= 20) {
//         mark = 'E';

//     } else if (mark <= 19 && mark >= 0) {
//         mark = 'F';

//     }
// }


// switch (mark) {
//     case 'A': {
//         console.log('Your mark is A.');
//         break;
//     }
//     case 'B': {
//         console.log('Your mark is B.');
//         break;
//     }
//     case 'C': {
//         console.log('Your mark is C.');
//         break;
//     }
//     case 'D': {
//         console.log('Your mark is D.');
//         break;
//     }
//     case 'E': {
//         console.log('Your mark is E.');
//         break;
//     }
//     case 'F': {
//         console.log('Your mark is F.');
//         break;
//     }

//     default: {
//         console.log('Not valid number');
//     }
// }


/**
 * Вправа 15: Калькулятор вартості квитків
Напишіть програму, яка пропонує користувачеві ввести свій вік і 
перевіряє вартість квитка на фільм. Правила такі:

Вік до 10 років: 5 доларів США
Вік 11-17: $8
Вік 18-64: 12 доларів
Вік 65 років і старше: $7

Використовуйте оператори if-else, щоб визначити ціну квитка.
 */

// const age = prompt('Enter your age');

// if (age && !isNaN(age)) {

//     if (age < 0 || age > 110) {
//         console.log('Invalid age');

//     }  else if (age <= 10) {
//         console.log('Ticket price is $5');

//     } else if (age >= 11 && age <= 17) {
//         console.log('Ticket price is $8');

//     } else if (age >= 18 && age <= 64) {
//         console.log('Ticket price is $12');

//     } else if (age >= 65) {
//         console.log('Ticket price is $7');

//     }
// }

//----------------------------------------------------------------

/**
 * Вправа 17: гра «Камінь, папір, ножиці».
 * 
Створіть просту гру «камінь, папір, ножиці», де користувач може 
ввести свій вибір, а програма випадковим чином генерує вибір комп’ютера. 
Визначте переможця за допомогою операторів if-else.
 */

// const userChoice = prompt('Choose rock, paper or scissors', 'Rock, Paper, Scissors');
// console.log(userChoice);

// const randomNumber = Math.random();

// let computerChoice;

// if (randomNumber > 0 && randomNumber <= 0.35) {
//     computerChoice = 'Rock';

// } else if (randomNumber > 0.35 && randomNumber <= 0.70) {
//     computerChoice = 'Paper';

// } else if (randomNumber > 0.70 && randomNumber < 1) {
//     computerChoice = 'Scissors';
// }

// console.log(computerChoice);

// let gameResult;

// if (userChoice === computerChoice) {
//     gameResult = 'Nobody won';

// } else if (userChoice === 'Rock' && computerChoice === 'Paper') {
//     gameResult = 'User won';

// } else if (userChoice === 'Rock' && computerChoice === 'Scissors') {
//     gameResult = 'User won';

// } else if (userChoice === 'Paper' && computerChoice === 'Rock') {
//     gameResult = 'Computer won';

// } else if (userChoice === 'Paper' && computerChoice === 'Scissors') {
//     gameResult = 'Computer won';

// } else if (userChoice === 'Scissors' && computerChoice === 'Paper') {
//     gameResult = 'User won';

// } else if (userChoice === 'Scissors' && computerChoice === 'Rock') {
//     gameResult = 'Computer won';

// }

// console.log(gameResult);

/**
 * Вправа 24: Симулятор банкомату
 * 
Створіть просту програму-симулятор банкомату. Попросіть користувача ввести
 баланс рахунку, а потім запитайте зняття або депозит. Відповідно оновіть 
 баланс рахунку за допомогою операторів if-else.
 */

//  const accountBalance = prompt('Enter your account balance.');

//  if (!accountBalance) {
//     console.warn('You didnt enter your balance.');

//  } else if (isNaN(accountBalance)) {
//     console.warn('You entered wrond data.');

//  } else {
//     const userAction = prompt('Withdrowal or deposit?', 'withdrowal, deposit');

//     const amountOfMoney = prompt('How much?');

//     let currentBalance;

//     switch (userAction) {

//         case 'withdrowal': {
//             currentBalance = accountBalance - amountOfMoney;
//             break;
//         }

//         case 'deposit': {
//             currentBalance = +accountBalance + +amountOfMoney;
//             break;
//         }
//     }

//     console.log(`Account balance: ${currentBalance} UA`);
//  }

/**
 * 
 * Вправа 25: Змішувач кольорів
 * 
Напишіть програму, яка приймає два основні кольори 
(червоний, синій, жовтий) як вхідні дані та виводить 
результат їх змішування. Наприклад, якщо змішати червоний і 
синій, ви отримаєте фіолетовий. Використовуйте оператор switch.
 */

// const colorOne = prompt('Enter color one', 'червоний, синій, жовтий');
// const colorTwo = prompt('Enter color two', 'червоний, синій, жовтий');

// if (!colorOne) {
//     console.warn('Please enter color one');

// } else if (!colorTwo) {
//     console.warn('Please enter color otwo');

// } else {

//     let mixedColor;

//     if (colorOne === 'червоний' && colorTwo === 'червоний') {
//         mixedColor = 'червоний';

//     } else if (colorOne === 'червоний' && colorTwo === 'синій') {
//         mixedColor = 'коричневий';

//     } else if (colorOne === 'червоний' && colorTwo === 'жовтий') {
//         mixedColor = 'помаранчевий';
        
//     } else if (colorOne === 'синій' && colorTwo === 'синій') {
//         mixedColor = 'синій';
        
//     } else if (colorOne === 'синій' && colorTwo === 'червоний') {
//         mixedColor = 'коричневий';
        
//     } else if (colorOne === 'синій' && colorTwo === 'жовтий') {
//         mixedColor = 'зелений';
        
//     } else if (colorOne === 'жовтий' && colorTwo === 'жовтий') {
//         mixedColor = 'жовтий';
        
//     } else if (colorOne === 'жовтий' && colorTwo === 'червоний') {
//         mixedColor = 'помаранчевий';
        
//     } else if (colorOne === 'жовтий' && colorTwo === 'синій') {
//         mixedColor = 'зелений';
        
//     }
//     console.log(mixedColor);
// }


















