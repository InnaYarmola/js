'use strict';

const posts = [
    {
        id: 1,
        title: "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
        body: "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
    },
    {
        id: 2,
        title: "qui est esse",
        body: "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla"
    },
    {
        id: 3,
        title: "ea molestias quasi exercitationem repellat qui ipsa sit aut",
        body: "et iusto sed quo iure\nvoluptatem occaecati omnis eligendi aut ad\nvoluptatem doloribus vel accusantium quis pariatur\nmolestiae porro eius odio et labore et velit aut"
    },
    {
        id: 12,
        src: 'https://www.thesprucepets.com/thmb/hxWjs7evF2hP1Fb1c1HAvRi_Rw0=/2765x0/filters:no_upscale():strip_icc()/chinese-dog-breeds-4797219-hero-2a1e9c5ed2c54d00aef75b05c5db399c.jpg',
        alt: 'Dog'
    },
    {
        id: 4,
        title: "eum et est occaecati",
        body: "ullam et saepe reiciendis voluptatem adipisci\nsit amet autem assumenda provident rerum culpa\nquis hic commodi nesciunt rem tenetur doloremque ipsam iure\nquis sunt voluptatem rerum illo velit"
    },
    {
        id: 5,
        title: "nesciunt quas odio",
        body: "repudiandae veniam quaerat sunt sed\nalias aut fugiat sit autem sed est\nvoluptatem omnis possimus esse voluptatibus quis\nest aut tenetur dolor neque"
    },
    {
        id: 6,
        title: "dolorem eum magni eos aperiam quia",
        body: "ut aspernatur corporis harum nihil quis provident sequi\nmollitia nobis aliquid molestiae\nperspiciatis et ea nemo ab reprehenderit accusantium quas\nvoluptate dolores velit et doloremque molestiae"
    },
    {
        id: 7,
        title: "magnam facilis autem",
        body: "dolore placeat quibusdam ea quo vitae\nmagni quis enim qui quis quo nemo aut saepe\nquidem repellat excepturi ut quia\nsunt ut sequi eos ea sed quas"
    }
];

/**
class Card
Загальні риси для всіх Card
* Основний контейнер;
* Кнопки видалення та редагування картки (функції повинні прийматися 
    ззовні);
* Контейнер для контенту;
* Є метод render;
 */

/**
class ArticleCard - дочірній від Card
Відмінні риси ArticleCard
Приймає заголовок посту та вставляє його у вигляді `<h3></h3>` 
у контейнер для контенту;
Приймає текст посту та вставляє його у вигляді `<p></p>` у контейнер 
ля контенту;
 */

const editCard = function () {
    new Modal().render();
};
const deleteCard = function (card) {
    console.log(card)

    const confirmDelete = function () {
        card.container.remove()
    }

    new DeleteModal(card.header, confirmDelete).render()
};

class Card {
    constructor(editFunc, deleteFunc) {
        this.editFunc = editFunc;
        this.deleteFunc = deleteFunc;
    }

    container = document.createElement('div');
    editBtn = document.createElement('button');
    deleteBtn = document.createElement('button');
    contentContainer = document.createElement('div');

    createElement() {
        this.container.className = 'card';
        this.editBtn.classList.add('card__btn', 'card__edit');
        this.deleteBtn.classList.add('card__btn', 'card__delete');
        this.contentContainer.className = 'card__content-container';
        this.container.append(this.editBtn, this.deleteBtn, this.contentContainer);
    }

    addListeners() {
        this.editBtn.addEventListener('click', this.editFunc);
        // this.deleteBtn.addEventListener('click', this.deleteFunc.bind(this));
        this.deleteBtn.addEventListener('click', () => {
            this.deleteFunc(this);
        });
    }

    render(renderContainer = document.body) {
        this.createElement();
        this.addListeners();
        renderContainer.append(this.container);
    }
}

// const card = new Card(editCard, deleteCard);
// card.render(document.body)

class ArticleCard extends Card {
    constructor(header, content, ...args) {
        super(...args);
        this.header = header;
        this.content = content;
    }

    createElement() {
        super.createElement();
        this.contentContainer.innerHTML = `
        <h3>${this.header}</h3>
        <p>${this.content}</p>
        `
    }
};

class PictureCard extends Card {
    constructor(img, alt, ...args) {
        super(...args);
        this.img = img;
        this.alt = alt;
    }

    createElement() {
        super.createElement();
        this.editBtn.remove();
        this.contentContainer.innerHTML = `
        <img src='${this.img}' alt ='${this.alt}' title='${this.alt}'>
        `
    }
};

posts.forEach(el => {
    new ArticleCard(el.title, el.body, editCard, deleteCard).render();

    if (el.src) {
        new PictureCard(el.src, el.alt, editCard, deleteCard).render();
    }

});

class Modal {
    consrtuctor() { };

    container = document.createElement('div');
    background = document.createElement('div');
    mainContainer = document.createElement('div');
    closeBtn = document.createElement('button');
    contentWrapper = document.createElement('div');
    buttonWrapper = document.createElement('div');

    createElement() {
        this.container.className = 'modal';
        this.background.className = 'modal__background';
        this.mainContainer.className = 'modal__main-container';
        this.closeBtn.className = 'modal__close';
        this.contentWrapper.className = 'modal__content-wrapper';
        this.buttonWrapper.className = 'modal__button-wrapper';

        this.mainContainer.append(this.closeBtn, this.contentWrapper, this.buttonWrapper);
        this.container.append(this.background, this.mainContainer);
    }

    closeModal() {
        this.container.remove();
    }

    addListeners() {
        this.closeBtn.addEventListener('click', this.closeModal.bind(this));
        this.background.addEventListener('click', this.closeModal.bind(this));

    }

    render(renderContainer = document.body) {
        this.createElement();
        this.addListeners();
        renderContainer.append(this.container);
    }
}


/**
 * Відмінні риси DeleteModal

Приймає заголовок посту та вставляє його у вигляді <h3>Do you really 
want to delete "${name}"?</h3> у контейнер для контенту;
Створює кнопки "Підтвердити" та "Скасувати", вставляє їх у контейнер для кнопок;
При натисканні на "Скасувати" модальне вікно закривається;
При натисканні на "Підтвердити" видаляє пост, потім модальне вікно закривається.
 (функція видалення посту приймається ззовні);
 */



class DeleteModal extends Modal {

    constructor(header, confirmHandler) {
        super()
        this.header = header;
        this.confirmHandler = confirmHandler;
    }

    confirmBtn = document.createElement('button');
    cancelBtn = document.createElement('button');

    createElement() {
        super.createElement();
        this.confirmBtn.className = 'modal__confirm-btn';
        this.confirmBtn.innerText = 'Confirm';
        this.cancelBtn.className = 'modal__cancel-btn';
        this.cancelBtn.innerText = 'Cancel';
        this.contentWrapper.innerHTML = `
      <h3>Do you really want to delete "${this.header}"?</h3>`;
        this.buttonWrapper.append(this.confirmBtn, this.cancelBtn);


    }

    addListeners() {
        super.addListeners();
        this.cancelBtn.addEventListener('click', this.closeModal.bind(this));
        this.confirmBtn.addEventListener('click', () => {
            this.confirmHandler();
            this.closeModal();
        });

    }

    render(renderContainer = document.body) {
        this.createElement();
        this.addListeners();
        renderContainer.append(this.container);
    }



}












