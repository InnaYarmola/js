'use strict';

const posts = [
    {
        id: 1,
        title: "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
        body: "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
    },
    {
        id: 2,
        title: "qui est esse",
        body: "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla"
    },
    {
        id: 3,
        title: "ea molestias quasi exercitationem repellat qui ipsa sit aut",
        body: "et iusto sed quo iure\nvoluptatem occaecati omnis eligendi aut ad\nvoluptatem doloribus vel accusantium quis pariatur\nmolestiae porro eius odio et labore et velit aut"
    },
    {
        id: 12,
        src: 'https://www.thesprucepets.com/thmb/hxWjs7evF2hP1Fb1c1HAvRi_Rw0=/2765x0/filters:no_upscale():strip_icc()/chinese-dog-breeds-4797219-hero-2a1e9c5ed2c54d00aef75b05c5db399c.jpg',
        alt: 'Dog'
    },
    {
        id: 4,
        title: "eum et est occaecati",
        body: "ullam et saepe reiciendis voluptatem adipisci\nsit amet autem assumenda provident rerum culpa\nquis hic commodi nesciunt rem tenetur doloremque ipsam iure\nquis sunt voluptatem rerum illo velit"
    },
    {
        id: 5,
        title: "nesciunt quas odio",
        body: "repudiandae veniam quaerat sunt sed\nalias aut fugiat sit autem sed est\nvoluptatem omnis possimus esse voluptatibus quis\nest aut tenetur dolor neque"
    },
    {
        id: 6,
        title: "dolorem eum magni eos aperiam quia",
        body: "ut aspernatur corporis harum nihil quis provident sequi\nmollitia nobis aliquid molestiae\nperspiciatis et ea nemo ab reprehenderit accusantium quas\nvoluptate dolores velit et doloremque molestiae"
    },
    {
        id: 7,
        title: "magnam facilis autem",
        body: "dolore placeat quibusdam ea quo vitae\nmagni quis enim qui quis quo nemo aut saepe\nquidem repellat excepturi ut quia\nsunt ut sequi eos ea sed quas"
    }
];

// --- CARD ---------------------------------------------------------------------

class Card {
    constructor(EditModal, DeleteModal) {
        this.EditModal = EditModal;
        this.DeleteModal = DeleteModal;
    }

    mainContainer = document.createElement('div');
    editBtn = document.createElement('button');
    deleteBtn = document.createElement('button');
    contentContainer = document.createElement('div');

    createElement() {
        this.mainContainer.className = 'card';
        this.editBtn.classList.add('card__btn', 'card__edit');
        this.deleteBtn.classList.add('card__btn', 'card__delete');
        this.contentContainer.className = 'card__content-container';

        this.mainContainer.append(this.editBtn, this.deleteBtn, this.contentContainer);
    }

    delete() {
        this.mainContainer.remove();
    }

    updateCard(newTitle, newText) {
        this.header = newTitle;
        this.title.innerText = newTitle;

        this.mainText = newText;
        this.paragraph.innerText = newText;
    }

    addListeners() {

        this.editBtn.addEventListener('click', () => {
            new this.EditModal(this).render();
        });

        this.deleteBtn.addEventListener('click', () => {
            new this.DeleteModal(this.header, this.delete.bind(this)).render();
        });
    }

    render(container = document.body) {
        this.createElement();
        this.addListeners()
        container.append(this.mainContainer);
    }
}

class ArticleCard extends Card {
    constructor(header, mainText, ...args) {
        super(...args);
        this.header = header;
        this.mainText = mainText;
    }

    title = document.createElement('h3');
    paragraph = document.createElement('p');

    createElement() {
        super.createElement();
        this.title.innerText = this.header;
        this.paragraph.innerText = this.mainText;
        this.contentContainer.append(this.title, this.paragraph);
    }
}

class PictureCard extends Card {

    constructor(src, alt, ...args) {
        super(...args);
        this.src = src;
        this.alt = alt;

    }

    createElement() {
        super.createElement();
        this.editBtn.remove();
        this.contentContainer.innerHTML = `
        <img src='${this.src}' alt ='${this.alt}' title='${this.alt}'>`;
    }

}

// --- MODAL ------------------------------------------------------------------

class Modal {
    constructor() {
    }

    mainContainer = document.createElement('div');
    background = document.createElement('div');
    contentContainer = document.createElement('div');
    closeBtn = document.createElement('button');
    contentWrapper = document.createElement('div');
    buttonWrapper = document.createElement('div');

    createElement() {
        this.mainContainer.className = 'modal';
        this.background.className = 'modal__background';
        this.contentContainer.className = 'modal__main-container';
        this.closeBtn.className = 'modal__close';
        this.contentWrapper.className = 'modal__content-wrapper';
        this.buttonWrapper.className = 'modal__button-wrapper';

        this.contentContainer.append(this.closeBtn, this.contentWrapper, this.buttonWrapper);
        this.mainContainer.append(this.background, this.contentContainer);
    }

    close() {
        this.mainContainer.remove()
    }

    addListeners() {
        this.closeBtn.addEventListener('click', this.close.bind(this));
        this.background.addEventListener('click', this.close.bind(this));
    }

    render(container = document.body) {
        this.createElement();
        this.addListeners();
        container.append(this.mainContainer);
    }
}

class DeleteModal extends Modal {
    constructor(header, deleteCardHandler) {
        super()
        this.header = header;
        this.deleteCardHandler = deleteCardHandler;

    }

    confirmBtn = document.createElement('button');
    cancelBtn = document.createElement('button');

    createElement() {
        super.createElement();
        this.confirmBtn.className = 'modal__confirm-btn';
        this.confirmBtn.innerText = 'Confirm';
        this.cancelBtn.className = 'modal__cancel-btn';
        this.cancelBtn.innerText = 'Cancel';

        this.contentWrapper.innerHTML = `
        <h3>Do you really want to delete "${this.header}"?</h3>`;
        this.buttonWrapper.append(this.confirmBtn, this.cancelBtn);
    }

    addListeners() {
        super.addListeners()
        this.confirmBtn.addEventListener('click', () => {
            this.deleteCardHandler();
            this.close();
        });

        this.cancelBtn.addEventListener('click', this.close.bind(this));
    }
}

class EditModal extends Modal {
    constructor(card) {
        super();
        this.card = card;
    }

    confirmBtn = document.createElement('button');
    form = document.createElement('form');
    titleLabel = document.createElement('label');
    input = document.createElement('input');
    postLabel = document.createElement('label');
    textarea = document.createElement('textarea');

    createElement() {
        super.createElement();
        this.confirmBtn.className = 'modal__confirm-btn';
        this.confirmBtn.innerText = 'Confirm';

        this.titleLabel.innerText = 'Title';
        this.input.value = `${this.card.header}`;
        this.postLabel.innerText = 'Post';
        this.textarea.innerText = `${this.card.mainText}`;

        this.form.append(this.titleLabel, this.input, this.postLabel, this.textarea);
        this.contentWrapper.append(this.form);
        this.buttonWrapper.append(this.confirmBtn);
    }

    addListeners() {
        super.addListeners()

        this.confirmBtn.addEventListener('click', () => {
            this.card.updateCard(this.input.value, this.textarea.value);
            this.close();

        });
    };
};

// --- RENDER CARDS ----------------------------------------------------------

posts.forEach(el => {
    if (el.src) {
        new PictureCard(el.src, el.alt, EditModal, DeleteModal).render();
    } else {
        new ArticleCard(el.title, el.body, EditModal, DeleteModal).render();
    }
});

