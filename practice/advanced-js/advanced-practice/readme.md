<!-- # Картки постів
<hr />

## class Card
### Загальні риси для всіх Card
* Основний контейнер;
* Кнопки видалення та редагування картки (функції повинні прийматися ззовні);
* Контейнер для контенту;
* Є метод render;
<hr /> -->

<!-- ## class ArticleCard - дочірній від Card
### Відмінні риси ArticleCard
* Приймає заголовок посту та вставляє його у вигляді `<h3></h3>` у контейнер для контенту;
* Приймає текст посту та вставляє його у вигляді `<p></p>` у контейнер для контенту;
<hr /> -->

## class Modal
### Загальні риси для всіх Modal
* Головний контейнер;
* Контейнер із затемненим фоном. По кліку на фоні модальне вікно закривається.
* Кнопку закрити. Натисніть кнопку модальне вікно закривається.
* Основний контейнер. У ньому контейнери для контенту та кнопок;
* Є метод render;
<hr />

## class DeleteModal - дочірній від Modal
### Відмінні риси DeleteModal
* Приймає заголовок посту та вставляє його у вигляді `<h3>Do you really want to delete "${name}"?</h3>` у контейнер для контенту;
* Створює кнопки "Підтвердити" та "Скасувати", вставляє їх у контейнер для кнопок;
* При натисканні на "Скасувати" модальне вікно закривається;
* При натисканні на "Підтвердити" видаляє пост, потім модальне вікно закривається. (функція видалення посту приймається ззовні);
<hr />

## class EditModal - дочірній від Modal
### Відмінні риси EditModal
* Створює та вставляє в контейнер для контенту форму конструкції
  ```html

   <form>
     <label>Title</label>
     <input>
     <label>Post</label>
     <textarea></textarea>
   </form>

   ````
* Приймає заголовок посту і надає його як значення в `input`;
* Приймає текст посту і надає його як значення в `textarea`;
* Створює кнопку "Підтвердити", вставляє її в контейнер для кнопок;
* При натисканні на "Підтвердити" змінює значення в пості, потім модальне вікно закривається. (функція зміни посту приймається ззовні);
<hr />