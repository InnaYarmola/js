'use strict';

// function show () {
//     console.log(this)
// }
// show()
// function User (name, lastName) {

//     if (!new.target) {
//         console.warn('Use "new" keyword');
//     }
//     this.name = name;
//     this.lastName = lastName;

//     this.getFullName = function () {
//         return `${this.name} ${this.lastName}`
//     };

//     // this.fullName = this.getFullName()
// }

// const user1 = new User('Inna', 'Yarmola');
// console.log(user1);
// console.log(user1.getFullName());


// const user2 = new User('fff', 'Yhfdhgh');
// console.log(user2);
// console.log(user2.getFullName());

// let counter = {
//     count: 0,
//     next: function () {
//         return ++this.count;
//     },
// };

// console.log(counter.next())

// console.log(this === window);

// this.color = 'red';

// console.log(this.color);
// console.log(window.color);

// const car = {
//     brand: 'Honda',
//     getBrand: function () {
//         return this.brand;
//     },
// };

// let brand = car.getBrand.bind(car);

// console.log(brand())

// console.log(globalThis);

// const f = () => {
//     console.log(this)
// }

// const f1 = function() {
//     console.log(this)
// }

// f();
// f1();

// [1, 3, 4].forEach(el => console.log(el));
// const pCollection = document.querySelectorAll('p');
// console.log(pCollection)
// pCollection.forEach(el => console.log(el))

// pCollection.forEach(element => {
//     console.log(element);
// });

// [].forEach.call(pCollection, (el) => console.log(el));

// const textArr = [].map.call(pCollection, (el) => {
//     return el.innerText;
// })

// console.log(textArr)

// const arr = [...pCollection];
// console.log(arr);

// const buildUrl = (protocol, domain, route) => {
//     return `${protocol}://${domain}/${route}`;
// };

// console.log(buildUrl('https', 'google', 'search'));

// // const buildHttpsUrl = buildUrl.bind(null, 'https');
// // console.log(buildHttpsUrl('rozetka', 'search'))



// const buildHttpUrl = buildUrl.bind(null, 'http');
// const buildHttpDanItUrl = buildHttpUrl.bind(null, 'dan-it.com');
// console.log(buildHttpUrl('rozetka', 'search'));
// console.log(buildHttpDanItUrl('search'));


/*
Заповніть колонку Colors кольорами параграфів із колонки Paragraphs.
Для розв'язання задачі запозичте метод "map" у масиву.
 */


// const pCollection = document.getElementsByTagName('p');
// const colorsTb = document.querySelector('#colors');

// console.log(colorsTb);

// // const colorArr = [].map.call(pCollection, el => {
// //     const color = el.style.color;
// //     return `<p style='color: ${color}'>${color}</p>`;
// // });

// const colorArr = [...pCollection].map(el => {
//     const color = el.style.color;
//     return `<p style='color: ${color}'>${color}</p>`;
// });


// colorsTb.insertAdjacentHTML("beforeend", colorArr.join(''));

// const User = function(name, age) {
//     this.name = name;
//     this.age = age;
// };

// const inna = new User('Inna', 34);
// console.log(inna);
// // console.log(inna.__proto__);
// console.log(Object.getPrototypeOf(inna));

// User.prototype.logAge = function () {console.log(this.age)};
// inna.logAge();
// console.log(inna.constructor)
// console.log([].constructor)







/**Функція конструктор ShowMore повинна приймати в себе текст, кількість рядків,
 *  що відображаються, і селектор головного контейнера.
У функції повинен бути метод render який повинен буде малювати отриманий
контейнер

<div class="show-more__wrapper">
     <span class="show-more__text">Some Text</span>
     <button class="show-more__button">Show more</button>
</div>


На <span class="show-more__text"> при ініціалізації повинен додаватися стиль

-webkit-line-clamp: ${NumberOfLine};

де ${NumberOfLine} - аргумент функції конструктора (кількість рядків, що
    відображаються);
При натисканні на кнопку "Show more" блок повинен розкриватися (властивість
    стилю display має ставати block) а текст кнопки повинен змінюватися на
    Show less. При повторному натисканні все повертається в початковий стан. */

// const someText = 'Prototypes are the mechanism by which JavaScript objects inherit features from one another. In this article, we explain what a prototype is, how prototype chains work, and how a prototype for an object can be set. This is an object with one data property, city, and one method, greet(). If you type the object\'s name followed by a period into the console, like myObject., then the console will pop up a list of all the properties available to this object. You\'ll see that as well as city and greet, there are lots of other properties!'
// const mainContainer = document.querySelector('.container');


// const ShowMore = function (text, numberOfLine, container) {

//     this.text = text;
//     this.numberOfLine = numberOfLine;
//     this.container = container;
// };

// ShowMore.prototype.render = function () {

//     const divContainer = document.createElement('div');
//     divContainer.className = 'show-more__wrapper';

//     const spanText = document.createElement('span');
//     spanText.className = 'show-more__text';
//     spanText.innerText = someText;
//     spanText.style[`-webkit-line-clamp`] = this.numberOfLine;

//     const button = document.createElement('button');
//     button.className = 'show-more__button';
//     button.innerText = 'Show more';

//     button.addEventListener('click', (event) => {

//         if(spanText.style.display === '-webkit-box') {
//             spanText.style.display = 'block';
//             event.target.innerText = 'Show less';

//         } else {
//             spanText.style.display = '-webkit-box';
//             event.target.innerText = 'Show more';
//         }
//     });

//     divContainer.append(spanText, button);
//     this.container.append(divContainer);
// };

// new ShowMore(someText, 5, mainContainer).render();
// new ShowMore(someText, 1, mainContainer).render();
// new ShowMore(someText, 2, mainContainer).render();
// new ShowMore(someText, 3, mainContainer).render();



// const User = function (name, lastName, age) {
//     new.target;
//     this.name = name;
//     this.lastName = lastName;
//     this.age = age;
// };

// User.prototype.getFullName = function () {
//     return `${this.name} ${this.lastName}`
// }

// const user1 = new User('Inna', 'Yarmola', 34);

// console.log(user1);
// console.log(user1.getFullName());

// const car = {
//     brand: 'Honda',
//     getBrand: function() {
//         return this.brand;
//     }
// }

// const car2 = {
//     brand: 'Toyota',
//     getBrand: function() {
//         return this.brand;
//     }
// }

// const brand = car.getBrand.bind(car2);

// console.log(brand())

// const Car = function (brand) {
//     this.brand = brand;
// }

// Car.prototype.logBrand = function () {
//     console.log(this.brand);
// }

// const car1 = new Car('BMW');
// const car2 = new Car('Toyota');
// const car3 = new Car('Zhiguli');
// // console.log(car1)
// // console.log(car2)
// // console.log(car3)
// // car1.logBrand()
// // car2.logBrand()
// // car3.logBrand()

// console.log(Object.getPrototypeOf(car1))
// console.log(car1.constructor.prototype)

// const showBrand = function (prefix) {
//     return `${prefix} ${this.brand}`
// };

// // const honda = {
// //     brand:
// // }

// console.log(showBrand.call(car1, 'It is'))
// console.log(showBrand.call(car2, 'It is'))
// console.log(showBrand.call(car3, 'It is'))

// const Box = function (w, h) {
//     this.w = `${w}sm`;
//     this.h = h;
//     this.start = function() {
//         console.log('Start car!')
//     }
// }

// const ColoredBox = function (w, h, color) {
//     Box.call(this, w, h);
//     this.color = color;
// }

// const showBox = new ColoredBox(15, 25, 'red');
// console.log(showBox);

/**
 * Создайте конструктор для объекта "Person", который принимает параметры
 * name и age. Затем создайте два экземпляра этого объекта.
 */
/**
 * Добавьте метод greet в конструктор "Person", который
 * выводит приветствие с именем и возрастом.
 *
 * Используйте прототипы для добавления метода getAge к объекту "Person",
 * который возвращает возраст.
 *
 * Создайте конструктор "Student", который наследует свойства и методы от "Person".
 * Добавьте новое свойство grade.
 *
 * Добавьте статический метод isAdult к конструктору "Person",
 * который возвращает true, если возраст больше или равен 18.
 *
 * Используйте оператор instanceof, чтобы проверить, является ли объект
 * экземпляром конструктора "Person".
 *
 * Создайте конструктор "Address", который принимает параметры city и country.
 *  Затем добавьте свойство address к объекту "Person", используя композицию.
 */

// const Address = function(city, country) {
//     this.city = city;
//     this.country = country;
// }

// const Person = function (name, age, address) {
//     this.name = name;
//     this.age = age;
//     this.address = address;
//     this.greet = function () {
//         return `Hello, ${this.name}, who is ${this.age} years old!`
//     }
// }

// const inna = new Person('Inna', 34, new Address('Kyiv', 'Ukr'));
// console.log(inna)

// Person.prototype.getAge = function () {
//     return this.age;
// }

// Person.prototype.isAdult = function() {
//     return this.age >= 18;
// }

// const Student = function (name, age, grade) {
//     Person.call(this, name, age);
//     this.grade = grade;
// }



// Student.prototype = Object.create(Person.prototype);
// Student.prototype.constructor = Student;


// const vova = new Person('Vova', 4);

// console.log(inna instanceof Person)

// const student1 = new Student('Slava', 15, 'A');
// console.log(student1.getAge())
// console.log(student1.isAdult())

// console.log(inna.greet());
// console.log(vova.getAge());

/**
 * Создайте функцию-конструктор "Car", которая принимает параметры make
 * и model, и возвращает новый объект. Убедитесь, что она вызывается с
 * использованием "new".
 */

// const Car = function(make, model) {
//     this.make = make;
//     this.model = model;
// }

// const bmw = new Car('BMW', 'X5');
// console.log(bmw)

/**
 * Создайте функцию isObject которая принимает аргумент и возвращает true,
 * если это объект (не массив и не null).
 */

// const isObject = function (arg) {

//     return typeof arg === 'object' && arg !== null && !Array.isArray(arg);
// }

// console.log(isObject())

/**
 * Создайте конструктор "Counter", который имеет приватное свойство count.
 * Добавьте методы increment и getCount, чтобы увеличивать счетчик и
 * получать его значение.
 */

// const Counter = function() {
//     let count = null;

//     this.increment = function() {
//         count++;
//     }

//     this.getCount = function() {
//         return count;
//     }
// };

// const newCount = new Counter();
// newCount.increment();

// class User {
//     constructor(name, lastName, age) {
//         this.name = name;
//         this.lastName = lastName;
//         this.age = age;
//         this._password = '123';
//     }

//     getFullName() {
//         return `${this.name} ${this.lastName}`;
//     }

//     // get password() {
//     //     return this._password;
//     // }

//     // set password(value) {
//     //     if (value !== this._password) {
//     //         this._password = value;
//     //     } else {
//     //         console.warn('Same');
//     //     }
//     // }

//     static password = 1234;

//     static getPassword() {
//         return this.password;
//     }

// }

// const inna = new User('Inna', 'Yarmola', 34);
// console.log(inna.password);
// inna.password = '123'
// console.log(inna.password);
// console.log(User.getPassword());


// class User {
//     constructor(name, lastName, age) {
//         this.name = name;
//         this.lastName = lastName;
//         this.age = age;
//         this._password = '123';
//     }



//     getFullName() {
//         console.log(`${this.name} ${this.lastName}`);
//     }

//     static password = 1234;

//     static getPassword() {
//         return this.password;
//     }
// }

// class Student extends User {

//     constructor(name, lastName, age) {
//         super(name, lastName, age);
//         this.course = 'history';

//     }

//     #createHw() {
//         return `This is your task`;
//     }

//     giveHw() {
//         return this.#createHw();
//     }
// }

// class FirstYearStudent extends Student {
//     constructor(year, ...args) {
//         super(...args);
//         this.year = year;
//     }

//     #salary = 2000;

//     getFullName() {
//         super.getFullName();
//         console.log('First year student')
//     }

//     getHw(hw) {
//         console.log(hw)
//     }
// }

// const bob = new Student('John', 'Adams', 25);
// const john = new FirstYearStudent(1, 'John', 'Adams', 25);


// console.log(john.getHw(bob.giveHw()));
// console.log(john.getFullName());




/**
     Напишіть клас User, який буде використовуватись для створення об'єкта, який 
     описує користувача панелі адміністрування сайту.
     Об'єкт має мати такі поля:
     - role (super admin, admin, main manager, content manager);
     - login;
     - Email;
     - password;

     сеттери та гетери:
     - role (можна встановити роль лише зі списку: super admin, admin, main manager, 
        content manager);
     - login (не менше трьох літер);

     а також метод:
     - isValidEmail - перевірять, чи є переданий рядок email'ом (у ньому має бути 
        символ @, мінімум 1 точка і не повинно бути прогалин);

     Додаткове завдання: додайте метод getPasswordStrength, який перевіряє чинність 
     пароля. Метод отримує рядок (пароль) і повертає один із трьох варіантів:
     - weak – менше 6 символів, не містить літер або цифр;
     - medium - більше 6 символів, містить цифри та літери, але не містить 
     знаків: -_^$%#@*&?
     - strong - більше 6 символів, містить літери, цифри та спецсимволи: -_^$%#@*&?

 */

//  class User {
//     constructor() {

//     }
//  }




// /**Створіть клас Animal який прийматиме ім'я тварини.
// Ім'я тварини має бути не менше 3-х символів.
// У тварини повинен бути метод render який приймає селектор контейнера 
// та малює в цей контейнер html:
// <div class="animal">
//      <span>{name}</span>
//      <button>Move</button>
// </div>
// При натисканні на кнопку "Move" тварина повинна переміщатися на 
// 20px вправо (створіть метод move); */
// /**
//  * @return {boolean}

//  */
// const getRandomChance = () => Math.random() < 0.5;

// const animals = [];

// class Animal {
//     constructor(name) {
//         this.name = name;
//     }
//     x = 0;
//     div = null;

//     createElements() {
//         this.div = document.createElement('div');
//         this.div.className = 'animal';
//         this.div.innerHTML = `<span>${this.name}</span>`;

//         const button = document.createElement('button');
//         button.innerText = 'Move';
//         // button.addEventListener('click', () => {this.move()});
//         button.addEventListener('click', this.move.bind(this));

//         this.div.append(button);
//     }

//     render(selector = '.container') {
//         this.createElements();
//         const container = document.querySelector(selector);
//         container.append(this.div);
//         animals.push(this.div);
//     }

//     move() {
//         this.x += 20;
//         this.div.style.transform = `translateX(${this.x}px)`;
//     }
// };

// const dog = new Animal('dog');
// dog.render('.container')

/**Створіть клас Dog який успадковується від Animal.
До контейнера div class="animal"> додайте клас dog

Додайте собаці метод say, який додаватиме до контейнера div 
клас dog-say і прибирати його через 200мс.
Собака гіперактивна і іноді при натисканні на кнопку "Move" 
переміщається на 20px двічі, при цьому гавкає (метод say). 
Для рендому використовуйте функцію getRandomChance() */

// class Dog extends Animal {

//     createElements() {
//         super.createElements();
//         this.div.classList.add('dog');
//     }

//     say() {
//         this.div.classList.add('dog-say');
//         setTimeout(() => {
//             this.div.classList.remove('dog-say');
//         }, 2000)

//     }

//     move() {
//         super.move();

//         if (getRandomChance()) {
//             super.move();
//             this.say();
//         }
//     }
// }
// const dog = new Dog('dog');
// dog.render()

// /**Створіть клас Cat який успадковується від Animal.
// До контейнера div class="animal"> додайте клас cat

// Додайте собаці метод say, який додаватиме до контейнера 
// div клас cat-say і прибирати його через 200мс.
// Кіт не любить коли йому наказують і іноді при натисканні 
// на кнопку "Move" не переміщається, при цьому нявкає (метод say).
//  Для рендому використовуйте функцію getRandomChance() */

// class Cat extends Animal {
//     createElements() {
//         super.createElements();
//         this.div.classList.add('cat');
//     }

//     say() {
//         this.div.classList.add('cat-say');
//         setTimeout(() => {
//             this.div.classList.remove('cat-say');
//         }, 2000);
//     }

//     move() {

//         if (!getRandomChance()) {
//             this.say();

//         } else {
//             super.move();
//         }
//     }
// }

// const cat = new Cat('cat');
// cat.render()

// /**
//  * Створіть клас Snake який успадковується від Animal.
// Змія приймає додаткові параметри type. Цей параметр може 
// приймати тільки два значення (poison або safe);
// До контейнера div class="animal"> додайте клас snake

// Змія дуже повільна і при натисканні на кнопку "Move" переміщається 
// всього-на-всього на 5px. Також змія має шанс зашипіти. Для рендома 
// використовуйте функцію getRandomChance();
// Якщо змія отруйна, то після того, як вона шипить всі інші тварини 
// розбігаються (видалити з екрану всіх кішок і собак).
//  */

// class Snake extends Animal {
//     constructor(name, type) {
//         super(name);
//         this.type = type;

//     }

//     createElements() {
//         super.createElements();
//         this.div.classList.add('snake');
//     }

//     say() {
//         if (getRandomChance()) {
//             console.warn('Shhhhhhhhh');

//             if (this.type === 'poison') {

//                 animals.forEach(el => {

//                     if (!el.className.includes('snake')) {
//                         el.remove();
//                     }
//                 })
//             }
//         }
//     }

//     move() {

//         if (!getRandomChance()) {
//             this.say();

//         } else {
//             this.x -= 15;
//             super.move();
//         }
//     }
// }

// const snake = new Snake('Poison', 'poison');
// const snake2 = new Snake('Safe', 'safe');
// snake.render();
// snake2.render();

// console.log(animals)











