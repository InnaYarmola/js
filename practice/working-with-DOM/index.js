'use strict';

// console.log(document.head.nextSibling);
// // console.log(document.documentElement);
// console.log(document.body.previousSibling);

// for (let i = 0; i < document.body.children.length; i++) {
//     console.log(document.body.children[i]);
// }

// const arr = Array.from(document.body.childNodes);
// console.log(arr)

// const headerTitle = document.getElementById('title');
// console.log(headerTitle);

// const header = document.querySelectorAll('.header__title');
// console.log(header);

// header.forEach(elem => {
//     if (elem.matches('.title')) {
//         console.log(elem, 'Title')
//     }
// })

// const childElem = document.querySelector('.child');
// console.log(childElem);

// let closestAncestor = childElem.closest('.parent');
// closestAncestor.textContent = 'How are you?'
// console.log(closestAncestor.textContent)

// if (closestAncestor) {
//     closestAncestor.style.border = '2px solid green';
// }

// console.log(document.head.childNodes);
// console.log(document.head.children);


// const headerTitle = document.getElementById('header__title');
// console.log(headerTitle.innerText = 'Hello, there))');

// const classFeature = document.getElementsByClassName('feature');
// console.log(classFeature);
// const arr = [...classFeature]
// arr.forEach(elem => elem.style.borderBottom = '1px solid yellow')

// const pElements = document.querySelectorAll('p');

// console.log(pElements);

// const pArr = [...pElements];

// pArr.forEach(elem => elem.style.color = 'red');

/**
 * Створіть HTML-сторінку з елементом (наприклад, <img>).
Використовуючи JavaScript, змініть значення атрибуту цього елементу (наприклад, src).
 */

// const imgElem = document.querySelector('img')
// // console.log(imgElem.alt = 'Hello')
// imgElem.setAttribute('alt', 'Hello2')
// console.log(imgElem)

/**
 * Створіть HTML-сторінку з контейнером (наприклад, <div>).
Використовуючи JavaScript, додайте новий елемент (наприклад, <p>) у цей контейнер.
 */

// const header = document.querySelector('header');
// header
// console.log(header);

// const div = document.createElement('div');
// div.innerText = 'Hello, Inna';
// div.classList = 'divStyle';
// header.append(div)
// console.log(div)

// const p = document.createElement('p');
// p.innerText = 'Before';
// console.log(p.innerText)
// p.classList = 'pStyle';
// console.log(p)

// const div = document.querySelector('.parent');
// // console.log(div)
// // div.prepend(p);
// // div.append(p);
// // div.before(p);
// // div.after(p);

// div.insertAdjacentHTML('afterbegin', )

// const users = [
//     {
//         name: 'John',
//         age: 32,
//     },
//     {
//         name: 'Bob',
//         age: 55,
//     },
//     {
//         name: 'Adam',
//         age: 17,
//     }
// ];

// users.forEach((elem) => {
//     document.body.insertAdjacentHTML('beforeend', `
//     <div class="user">
//         <h1>${elem.name}</h1>
//         <p>${elem.age}</p>
//     </div>`)

// })


// const elem = document.querySelector('div');
// console.log(elem);
// elem.remove();


// const p = document.querySelector('p');
// const newP = document.createElement('p');
// newP.innerText = ' Inna';
// newP.style.color = 'red';
// newP.className = 'pStyle';
// const span = document.createElement('span');
// span.innerText = 'Vova ';

// const div = document.querySelector('.parent');
// // div.append(newP)


// const divider = document.querySelector('.divider');

// div.insertAdjacentHTML('beforeend', `
//     <h1>Hello</h1>
//     <p>from</p>
//     <p>Inna</p>
//     <a href="#">link</a>

// `)
// divider.prepend(span)

// console.log(newP); 

// const container = document.querySelector('.parent');
// const p = document.querySelector('.divider');

// const copyContainer = container.cloneNode();
// console.log(copyContainer)

// container.after(copyContainer)


// const button = document.createElement('button');
// button.innerText = 'CLICK HERE';
// button.className = 'btn';
// button.onclick = () => alert('You clicked');

// const btn = document.querySelector('.btn')
// btn.before(button);


/**
 * Завдання 1.
 *
 * Написати скрипт, який створить квадрат довільного розміру.
 *
 * Розмір квадрата в пікселях отримати інтерактивно за 
 * допомогою діалогового вікна prompt.
 *
 * Якщо користувач ввів розмір квадрата в некоректному форматі
 * Запитувати дані повторно до тих пір, поки дані не будуть 
 * введені коректно.
 *
 * Всі стилі для квадрата встановити через JavaScript за 
 * допомогою одного рядка коду.
 *
 * Тип елемента, що описує квадрат - div.
 * Задати новоствореному елементу CSS-клас .square.
 *
 * Квадрат у вигляді стилізованого елемента div необхідно
 * зробити першим та єдиним нащадком body документа.
 */


// const square = document.createElement('div');
// square.className = 'square';

// const side = prompt('Enter square side');
// const color = prompt('Enter color');

// document.body.insertAdjacentHTML('afterbegin', `<div style="width: ${side}px; height: ${side}px; border: 1px solid ${color}"></div>`)

// // square.style.width = `${side}px`
// square.style.height = `${side}px`;
// square.style.background = color;

// document.body.append(square);

// console.log(square);





// Написати "to do" лист.

// Запитувати у користувача пункти для додавання до 
// списку доти, доки
// не натисне скасування. Кожен prompt є новий елемент списку.
// Усі пункти вставити сторінку.
// Не забуваємо про семантику (список має бути оформлений 
// через ul або ol);


// Натисніть на елемент списку - видалити цей пункт.
// Підказка
//elem.onclick = function() {
// пишемо що відбувається на кліку
//};


{/* <ul> To Do LIst
    <li></li>
    <li></li>
    <li></li>
</ul> */}

// const toDoList = document.createElement('ul');
// toDoList.innerText = `To Do List

// `;

// let toDo;

// toDo = prompt('What do you need to do?');

// while (toDo !== null) {

//     // toDoList.insertAdjacentHTML('beforeend',`<li>${toDo}</li>`);
//     const li = document.createElement('li');
//     li.innerText = toDo;
//     li.classList.add('list-item');

//     li.onclick = () => li.remove();

//     toDoList.append(li);
//     toDo = prompt('What do you need to do?');
// }

// document.body.append(toDoList);

// const elem = document.querySelectorAll('ul>li');
// console.log(elem[1])



/**
 * Завдання 4.
 *
 * Написати функцію fillChessBoard, яка розгорне 
 * «шахівницю» 8 на 8.
 *
 * Колір темних осередків - #161619.
 * Колір світлих осередків - #FFFFFF.
 * Інші стилі CSS для дошки та осередків готові.
 *
 * Дошку необхідно розгорнути всередині елемента з 
 * класом .board.
 *
 * Кожна комірка дошки представляє елемент div із 
 * класом .cell.
 * 
 *  <div class="board">
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      ...
    </div>
 */

/* Дано */


// const LIGHT_CELL = '#ffffff';
// const DARK_CELL = '#161619';
// const V_CELLS = 8;
// const H_CELLS = 8;

// const board = document.createElement('div');
// board.className = 'board';

// for (let j = 1; j <= 4; j++) {

//     for (let i = 1; i <= 4; i++) {
//         board.insertAdjacentHTML('beforeend', `
//         <div class="cell" style="background: #ffffff;"></div>
//         <div class="cell" style="background: #161619;"></div>`);
//     }

//     for (let i = 1; i <= 4; i++) {
//         board.insertAdjacentHTML('beforeend', `
//         <div class="cell" style="background: #161619;"></div>
//         <div class="cell" style="background: #ffffff;"></div>
//         `); 
//     }
// }

// document.body.append(board);

/*
Створіть галерею зображень бородатих мужиків та котів.

При натисканні на кнопку "Бородачі" відобразити на екран 
100 зображень бородатих мужиків.
Тайтл має помінятися на "Бородачі"

При натисканні на кнопку "Коти" відобразити на екран 100 
зображень котів.
Тайтл має змінитись на "Коти"


У кожного зображення має бути рамка та ефект наведення.
При натисканні зображення повинне відкриватися на весь екран,
 при повторному натисканні повертатися назад.
  */

// Підказка
//elem.onclick = function() {
// пишемо що відбувається на кліку
//};

// const beardedManUrl = 'https://placebeard.it/200x200';
// const catUrl = 'https://loremflickr.com/200/200';

// const container = document.createElement('div');
// container.classList.add('container')


// const menButton = document.createElement('button');
// menButton.innerText = "Бородачі";
// menButton.classList.add('btn');


// const catsButton = document.createElement('button');
// catsButton.innerText = "Коти";

// catsButton.classList.add('btn');

// const btnContainer = document.createElement('div');
// btnContainer.classList.add('buttons-container');


// btnContainer.append(menButton);
// btnContainer.append(catsButton);

// document.body.append(container);
// container.prepend(btnContainer);

// const galleryContainer = document.createElement('div');
// container.append(galleryContainer);
// galleryContainer.classList.add('gallery-container');

// const h1 = document.createElement('h1');
// btnContainer.after(h1);
// h1.classList.add('title');
// h1.innerText = 'Photos';



// const addPicture = (url, title) => {
//     galleryContainer.innerHTML = '';

//     for (let i = 0; i < 10; i++) {
//         const galleryImage = document.createElement('img');
//         galleryImage.src = `${url}?v=${i}`;
//         galleryImage.classList.add('gallery-item');
//         galleryContainer.append(galleryImage);
//         galleryImage.onclick = () => galleryImage.classList.toggle('view');

//     }

//     h1.innerText = title;
// }



// menButton.onclick = () => {
//     addPicture(beardedManUrl, 'Men');
// };
// catsButton.onclick = () => {
//     addPicture(catUrl, 'Cats');
// };

// addPicture(beardedManUrl, 'Men');



// Даний масив постів. Намалюйте ці пости на екран у контейнер з класом .container
// як картки, у картки має бути дизайн - рамка, відступи, 
// зміна стилю при наведенні (напишіть стилі самі).

// Розв'яжіть задачу двома способами - використовуючи createElements 
// і insertAdjacentHTML

// Додаткове завдання - по кліку на постову картку він повинен ставати 
// прочитаним - стилі повинні змінитися.
// Підказка
//elem.onclick = function() {
// пишемо що відбувається на кліку
//};


{/* <div class="card">
    <span class="card__id">Id: 1</span>
    <h3 class="card__title">Title</h3>
    <p class="card__text"> Lorem ipsum dolor, sit amet consectetur adipisicing elit. Deserunt. </p>
</div> 
*/}

// const posts = [{
//     id: 1,
//     userId: 1,
//     title: "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
//     body: "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
// }, {
//     id: 2,
//     userId: 1,
//     title: "qui est esse",
//     body: "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla"
// }, {
//     id: 3,
//     userId: 1,
//     title: "ea molestias quasi exercitationem repellat qui ipsa sit aut",
//     body: "et iusto sed quo iure\nvoluptatem occaecati omnis eligendi aut ad\nvoluptatem doloribus vel accusantium quis pariatur\nmolestiae porro eius odio et labore et velit aut"
// }, {
//     id: 4,
//     userId: 1,
//     title: "eum et est occaecati",
//     body: "ullam et saepe reiciendis voluptatem adipisci\nsit amet autem assumenda provident rerum culpa\nquis hic commodi nesciunt rem tenetur doloremque ipsam iure\nquis sunt voluptatem rerum illo velit"
// }, {
//     id: 5,
//     userId: 1,
//     title: "nesciunt quas odio",
//     body: "repudiandae veniam quaerat sunt sed\nalias aut fugiat sit autem sed est\nvoluptatem omnis possimus esse voluptatibus quis\nest aut tenetur dolor neque"
// }, {
//     id: 6,
//     userId: 1,
//     title: "dolorem eum magni eos aperiam quia",
//     body: "ut aspernatur corporis harum nihil quis provident sequi\nmollitia nobis aliquid molestiae\nperspiciatis et ea nemo ab reprehenderit accusantium quas\nvoluptate dolores velit et doloremque molestiae"
// }, {
//     id: 7,
//     userId: 1,
//     title: "magnam facilis autem",
//     body: "dolore placeat quibusdam ea quo vitae\nmagni quis enim qui quis quo nemo aut saepe\nquidem repellat excepturi ut quia\nsunt ut sequi eos ea sed quas"
// }, {
//     id: 8,
//     userId: 1,
//     title: "dolorem dolore est ipsam",
//     body: "dignissimos aperiam dolorem qui eum\nfacilis quibusdam animi sint suscipit qui sint possimus cum\nquaerat magni maiores excepturi\nipsam ut commodi dolor voluptatum modi aut vitae"
// }, {
//     id: 9,
//     userId: 1,
//     title: "nesciunt iure omnis dolorem tempora et accusantium",
//     body: "consectetur animi nesciunt iure dolore\nenim quia ad\nveniam autem ut quam aut nobis\net est aut quod aut provident voluptas autem voluptas"
// }, {
//     id: 10,
//     userId: 1,
//     title: "optio molestias id quia eum",
//     body: "quo et expedita modi cum officia vel magni\ndoloribus qui repudiandae\nvero nisi sit\nquos veniam quod sed accusamus veritatis error"
// }, {
//     id: 11,
//     userId: 2,
//     title: "et ea vero quia laudantium autem",
//     body: "delectus reiciendis molestiae occaecati non minima eveniet qui voluptatibus\naccusamus in eum beatae sit\nvel qui neque voluptates ut commodi qui incidunt\nut animi commodi"
// }, {
//     id: 12,
//     userId: 2,
//     title: "in quibusdam tempore odit est dolorem",
//     body: "itaque id aut magnam\npraesentium quia et ea odit et ea voluptas et\nsapiente quia nihil amet occaecati quia id voluptatem\nincidunt ea est distinctio odio"
// }, {
//     id: 13,
//     userId: 2,
//     title: "dolorum ut in voluptas mollitia et saepe quo animi",
//     body: "aut dicta possimus sint mollitia voluptas commodi quo doloremque\niste corrupti reiciendis voluptatem eius rerum\nsit cumque quod eligendi laborum minima\nperferendis recusandae assumenda consectetur porro architecto ipsum ipsam"
// }, {
//     id: 14,
//     userId: 2,
//     title: "voluptatem eligendi optio",
//     body: "fuga et accusamus dolorum perferendis illo voluptas\nnon doloremque neque facere\nad qui dolorum molestiae beatae\nsed aut voluptas totam sit illum"
// }, {
//     id: 15,
//     userId: 2,
//     title: "eveniet quod temporibus",
//     body: "reprehenderit quos placeat\nvelit minima officia dolores impedit repudiandae molestiae nam\nvoluptas recusandae quis delectus\nofficiis harum fugiat vitae"
// }, {
//     id: 16,
//     userId: 2,
//     title: "sint suscipit perspiciatis velit dolorum rerum ipsa laboriosam odio",
//     body: "suscipit nam nisi quo aperiam aut\nasperiores eos fugit maiores voluptatibus quia\nvoluptatem quis ullam qui in alias quia est\nconsequatur magni mollitia accusamus ea nisi voluptate dicta"
// }, {
//     id: 17,
//     userId: 2,
//     title: "fugit voluptas sed molestias voluptatem provident",
//     body: "eos voluptas et aut odit natus earum\naspernatur fuga molestiae ullam\ndeserunt ratione qui eos\nqui nihil ratione nemo velit ut aut id quo"
// }, {
//     id: 18,
//     userId: 2,
//     title: "voluptate et itaque vero tempora molestiae",
//     body: "eveniet quo quis\nlaborum totam consequatur non dolor\nut et est repudiandae\nest voluptatem vel debitis et magnam"
// }, {
//     id: 19,
//     userId: 2,
//     title: "adipisci placeat illum aut reiciendis qui",
//     body: "illum quis cupiditate provident sit magnam\nea sed aut omnis\nveniam maiores ullam consequatur atque\nadipisci quo iste expedita sit quos voluptas"
// }, {
//     id: 20,
//     userId: 2,
//     title: "doloribus ad provident suscipit at",
//     body: "qui consequuntur ducimus possimus quisquam amet similique\nsuscipit porro ipsam amet\neos veritatis officiis exercitationem vel fugit aut necessitatibus totam\nomnis rerum consequatur expedita quidem cumque explicabo"
// }, {
//     id: 21,
//     userId: 3,
//     title: "asperiores ea ipsam voluptatibus modi minima quia sint",
//     body: "repellat aliquid praesentium dolorem quo\nsed totam minus non itaque\nnihil labore molestiae sunt dolor eveniet hic recusandae veniam\ntempora et tenetur expedita sunt"
// }, {
//     id: 22,
//     userId: 3,
//     title: "dolor sint quo a velit explicabo quia nam",
//     body: "eos qui et ipsum ipsam suscipit aut\nsed omnis non odio\nexpedita earum mollitia molestiae aut atque rem suscipit\nnam impedit esse"
// }, {
//     id: 23,
//     userId: 3,
//     title: "maxime id vitae nihil numquam",
//     body: "veritatis unde neque eligendi\nquae quod architecto quo neque vitae\nest illo sit tempora doloremque fugit quod\net et vel beatae sequi ullam sed tenetur perspiciatis"
// }, {
//     id: 24,
//     userId: 3,
//     title: "autem hic labore sunt dolores incidunt",
//     body: "enim et ex nulla\nomnis voluptas quia qui\nvoluptatem consequatur numquam aliquam sunt\ntotam recusandae id dignissimos aut sed asperiores deserunt"
// }, {
//     id: 25,
//     userId: 3,
//     title: "rem alias distinctio quo quis",
//     body: "ullam consequatur ut\nomnis quis sit vel consequuntur\nipsa eligendi ipsum molestiae et omnis error nostrum\nmolestiae illo tempore quia et distinctio"
// }, {
//     id: 26,
//     userId: 3,
//     title: "est et quae odit qui non",
//     body: "similique esse doloribus nihil accusamus\nomnis dolorem fuga consequuntur reprehenderit fugit recusandae temporibus\nperspiciatis cum ut laudantium\nomnis aut molestiae vel vero"
// }, {
//     id: 27,
//     userId: 3,
//     title: "quasi id et eos tenetur aut quo autem",
//     body: "eum sed dolores ipsam sint possimus debitis occaecati\ndebitis qui qui et\nut placeat enim earum aut odit facilis\nconsequatur suscipit necessitatibus rerum sed inventore temporibus consequatur"
// }, {
//     id: 28,
//     userId: 3,
//     title: "delectus ullam et corporis nulla voluptas sequi",
//     body: "non et quaerat ex quae ad maiores\nmaiores recusandae totam aut blanditiis mollitia quas illo\nut voluptatibus voluptatem\nsimilique nostrum eum"
// }, {
//     id: 29,
//     userId: 3,
//     title: "iusto eius quod necessitatibus culpa ea",
//     body: "odit magnam ut saepe sed non qui\ntempora atque nihil\naccusamus illum doloribus illo dolor\neligendi repudiandae odit magni similique sed cum maiores"
// }, {
//     id: 30,
//     userId: 3,
//     title: "a quo magni similique perferendis",
//     body: "alias dolor cumque\nimpedit blanditiis non eveniet odio maxime\nblanditiis amet eius quis tempora quia autem rem\na provident perspiciatis quia"
// }, {
//     id: 31,
//     userId: 4,
//     title: "ullam ut quidem id aut vel consequuntur",
//     body: "debitis eius sed quibusdam non quis consectetur vitae\nimpedit ut qui consequatur sed aut in\nquidem sit nostrum et maiores adipisci atque\nquaerat voluptatem adipisci repudiandae"
// }, {
//     id: 32,
//     userId: 4,
//     title: "doloremque illum aliquid sunt",
//     body: "deserunt eos nobis asperiores et hic\nest debitis repellat molestiae optio\nnihil ratione ut eos beatae quibusdam distinctio maiores\nearum voluptates et aut adipisci ea maiores voluptas maxime"
// }, {
//     id: 33,
//     userId: 4,
//     title: "qui explicabo molestiae dolorem",
//     body: "rerum ut et numquam laborum odit est sit\nid qui sint in\nquasi tenetur tempore aperiam et quaerat qui in\nrerum officiis sequi cumque quod"
// }, {
//     id: 34,
//     userId: 4,
//     title: "magnam ut rerum iure",
//     body: "ea velit perferendis earum ut voluptatem voluptate itaque iusto\ntotam pariatur in\nnemo voluptatem voluptatem autem magni tempora minima in\nest distinctio qui assumenda accusamus dignissimos officia nesciunt nobis"
// }, {
//     id: 35,
//     userId: 4,
//     title: "id nihil consequatur molestias animi provident",
//     body: "nisi error delectus possimus ut eligendi vitae\nplaceat eos harum cupiditate facilis reprehenderit voluptatem beatae\nmodi ducimus quo illum voluptas eligendi\net nobis quia fugit"
// }, {
//     id: 36,
//     userId: 4,
//     title: "fuga nam accusamus voluptas reiciendis itaque",
//     body: "ad mollitia et omnis minus architecto odit\nvoluptas doloremque maxime aut non ipsa qui alias veniam\nblanditiis culpa aut quia nihil cumque facere et occaecati\nqui aspernatur quia eaque ut aperiam inventore"
// }, {
//     id: 37,
//     userId: 4,
//     title: "provident vel ut sit ratione est",
//     body: "debitis et eaque non officia sed nesciunt pariatur vel\nvoluptatem iste vero et ea\nnumquam aut expedita ipsum nulla in\nvoluptates omnis consequatur aut enim officiis in quam qui"
// }, {
//     id: 38,
//     userId: 4,
//     title: "explicabo et eos deleniti nostrum ab id repellendus",
//     body: "animi esse sit aut sit nesciunt assumenda eum voluptas\nquia voluptatibus provident quia necessitatibus ea\nrerum repudiandae quia voluptatem delectus fugit aut id quia\nratione optio eos iusto veniam iure"
// }, {
//     id: 39,
//     userId: 4,
//     title: "eos dolorem iste accusantium est eaque quam",
//     body: "corporis rerum ducimus vel eum accusantium\nmaxime aspernatur a porro possimus iste omnis\nest in deleniti asperiores fuga aut\nvoluptas sapiente vel dolore minus voluptatem incidunt ex"
// }, {
//     id: 40,
//     userId: 4,
//     title: "enim quo cumque",
//     body: "ut voluptatum aliquid illo tenetur nemo sequi quo facilis\nipsum rem optio mollitia quas\nvoluptatem eum voluptas qui\nunde omnis voluptatem iure quasi maxime voluptas nam"
// }, {
//     id: 41,
//     userId: 5,
//     title: "non est facere",
//     body: "molestias id nostrum\nexcepturi molestiae dolore omnis repellendus quaerat saepe\nconsectetur iste quaerat tenetur asperiores accusamus ex ut\nnam quidem est ducimus sunt debitis saepe"
// }, {
//     id: 42,
//     userId: 5,
//     title: "commodi ullam sint et excepturi error explicabo praesentium voluptas",
//     body: "odio fugit voluptatum ducimus earum autem est incidunt voluptatem\nodit reiciendis aliquam sunt sequi nulla dolorem\nnon facere repellendus voluptates quia\nratione harum vitae ut"
// }, {
//     id: 43,
//     userId: 5,
//     title: "eligendi iste nostrum consequuntur adipisci praesentium sit beatae perferendis",
//     body: "similique fugit est\nillum et dolorum harum et voluptate eaque quidem\nexercitationem quos nam commodi possimus cum odio nihil nulla\ndolorum exercitationem magnam ex et a et distinctio debitis"
// }, {
//     id: 44,
//     userId: 5,
//     title: "optio dolor molestias sit",
//     body: "temporibus est consectetur dolore\net libero debitis vel velit laboriosam quia\nipsum quibusdam qui itaque fuga rem aut\nea et iure quam sed maxime ut distinctio quae"
// }, {
//     id: 45,
//     userId: 5,
//     title: "ut numquam possimus omnis eius suscipit laudantium iure",
//     body: "est natus reiciendis nihil possimus aut provident\nex et dolor\nrepellat pariatur est\nnobis rerum repellendus dolorem autem"
// }, {
//     id: 46,
//     userId: 5,
//     title: "aut quo modi neque nostrum ducimus",
//     body: "voluptatem quisquam iste\nvoluptatibus natus officiis facilis dolorem\nquis quas ipsam\nvel et voluptatum in aliquid"
// }, {
//     id: 47,
//     userId: 5,
//     title: "quibusdam cumque rem aut deserunt",
//     body: "voluptatem assumenda ut qui ut cupiditate aut impedit veniam\noccaecati nemo illum voluptatem laudantium\nmolestiae beatae rerum ea iure soluta nostrum\neligendi et voluptate"
// }, {
//     id: 48,
//     userId: 5,
//     title: "ut voluptatem illum ea doloribus itaque eos",
//     body: "voluptates quo voluptatem facilis iure occaecati\nvel assumenda rerum officia et\nillum perspiciatis ab deleniti\nlaudantium repellat ad ut et autem reprehenderit"
// }, {
//     id: 49,
//     userId: 5,
//     title: "laborum non sunt aut ut assumenda perspiciatis voluptas",
//     body: "inventore ab sint\nnatus fugit id nulla sequi architecto nihil quaerat\neos tenetur in in eum veritatis non\nquibusdam officiis aspernatur cumque aut commodi aut"
// }, {
//     id: 50,
//     userId: 5,
//     title: "repellendus qui recusandae incidunt voluptates tenetur qui omnis exercitationem",
//     body: "error suscipit maxime adipisci consequuntur recusandae\nvoluptas eligendi et est et voluptates\nquia distinctio ab amet quaerat molestiae et vitae\nadipisci impedit sequi nesciunt quis consectetur"
// }, {
//     id: 51,
//     userId: 6,
//     title: "soluta aliquam aperiam consequatur illo quis voluptas",
//     body: "sunt dolores aut doloribus\ndolore doloribus voluptates tempora et\ndoloremque et quo\ncum asperiores sit consectetur dolorem"
// }, {
//     id: 52,
//     userId: 6,
//     title: "qui enim et consequuntur quia animi quis voluptate quibusdam",
//     body: "iusto est quibusdam fuga quas quaerat molestias\na enim ut sit accusamus enim\ntemporibus iusto accusantium provident architecto\nsoluta esse reprehenderit qui laborum"
// }, {
//     id: 53,
//     userId: 6,
//     title: "ut quo aut ducimus alias",
//     body: "minima harum praesentium eum rerum illo dolore\nquasi exercitationem rerum nam\nporro quis neque quo\nconsequatur minus dolor quidem veritatis sunt non explicabo similique"
// }, {
//     id: 54,
//     userId: 6,
//     title: "sit asperiores ipsam eveniet odio non quia",
//     body: "totam corporis dignissimos\nvitae dolorem ut occaecati accusamus\nex velit deserunt\net exercitationem vero incidunt corrupti mollitia"
// }, {
//     id: 55,
//     userId: 6,
//     title: "sit vel voluptatem et non libero",
//     body: "debitis excepturi ea perferendis harum libero optio\neos accusamus cum fuga ut sapiente repudiandae\net ut incidunt omnis molestiae\nnihil ut eum odit"
// }, {
//     id: 56,
//     userId: 6,
//     title: "qui et at rerum necessitatibus",
//     body: "aut est omnis dolores\nneque rerum quod ea rerum velit pariatur beatae excepturi\net provident voluptas corrupti\ncorporis harum reprehenderit dolores eligendi"
// }, {
//     id: 57,
//     userId: 6,
//     title: "sed ab est est",
//     body: "at pariatur consequuntur earum quidem\nquo est laudantium soluta voluptatem\nqui ullam et est\net cum voluptas voluptatum repellat est"
// }, {
//     id: 58,
//     userId: 6,
//     title: "voluptatum itaque dolores nisi et quasi",
//     body: "veniam voluptatum quae adipisci id\net id quia eos ad et dolorem\naliquam quo nisi sunt eos impedit error\nad similique veniam"
// }, {
//     id: 59,
//     userId: 6,
//     title: "qui commodi dolor at maiores et quis id accusantium",
//     body: "perspiciatis et quam ea autem temporibus non voluptatibus qui\nbeatae a earum officia nesciunt dolores suscipit voluptas et\nanimi doloribus cum rerum quas et magni\net hic ut ut commodi expedita sunt"
// }, {
//     id: 60,
//     userId: 6,
//     title: "consequatur placeat omnis quisquam quia reprehenderit fugit veritatis facere",
//     body: "asperiores sunt ab assumenda cumque modi velit\nqui esse omnis\nvoluptate et fuga perferendis voluptas\nillo ratione amet aut et omnis"
// }, {
//     id: 61,
//     userId: 7,
//     title: "voluptatem doloribus consectetur est ut ducimus",
//     body: "ab nemo optio odio\ndelectus tenetur corporis similique nobis repellendus rerum omnis facilis\nvero blanditiis debitis in nesciunt doloribus dicta dolores\nmagnam minus velit"
// }, {
//     id: 62,
//     userId: 7,
//     title: "beatae enim quia vel",
//     body: "enim aspernatur illo distinctio quae praesentium\nbeatae alias amet delectus qui voluptate distinctio\nodit sint accusantium autem omnis\nquo molestiae omnis ea eveniet optio"
// }, {
//     id: 63,
//     userId: 7,
//     title: "voluptas blanditiis repellendus animi ducimus error sapiente et suscipit",
//     body: "enim adipisci aspernatur nemo\nnumquam omnis facere dolorem dolor ex quis temporibus incidunt\nab delectus culpa quo reprehenderit blanditiis asperiores\naccusantium ut quam in voluptatibus voluptas ipsam dicta"
// }, {
//     id: 64,
//     userId: 7,
//     title: "et fugit quas eum in in aperiam quod",
//     body: "id velit blanditiis\neum ea voluptatem\nmolestiae sint occaecati est eos perspiciatis\nincidunt a error provident eaque aut aut qui"
// }, {
//     id: 65,
//     userId: 7,
//     title: "consequatur id enim sunt et et",
//     body: "voluptatibus ex esse\nsint explicabo est aliquid cumque adipisci fuga repellat labore\nmolestiae corrupti ex saepe at asperiores et perferendis\nnatus id esse incidunt pariatur"
// }, {
//     id: 66,
//     userId: 7,
//     title: "repudiandae ea animi iusto",
//     body: "officia veritatis tenetur vero qui itaque\nsint non ratione\nsed et ut asperiores iusto eos molestiae nostrum\nveritatis quibusdam et nemo iusto saepe"
// }, {
//     id: 67,
//     userId: 7,
//     title: "aliquid eos sed fuga est maxime repellendus",
//     body: "reprehenderit id nostrum\nvoluptas doloremque pariatur sint et accusantium quia quod aspernatur\net fugiat amet\nnon sapiente et consequatur necessitatibus molestiae"
// }, {
//     id: 68,
//     userId: 7,
//     title: "odio quis facere architecto reiciendis optio",
//     body: "magnam molestiae perferendis quisquam\nqui cum reiciendis\nquaerat animi amet hic inventore\nea quia deleniti quidem saepe porro velit"
// }, {
//     id: 69,
//     userId: 7,
//     title: "fugiat quod pariatur odit minima",
//     body: "officiis error culpa consequatur modi asperiores et\ndolorum assumenda voluptas et vel qui aut vel rerum\nvoluptatum quisquam perspiciatis quia rerum consequatur totam quas\nsequi commodi repudiandae asperiores et saepe a"
// }, {
//     id: 70,
//     userId: 7,
//     title: "voluptatem laborum magni",
//     body: "sunt repellendus quae\nest asperiores aut deleniti esse accusamus repellendus quia aut\nquia dolorem unde\neum tempora esse dolore"
// }, {
//     id: 71,
//     userId: 8,
//     title: "et iusto veniam et illum aut fuga",
//     body: "occaecati a doloribus\niste saepe consectetur placeat eum voluptate dolorem et\nqui quo quia voluptas\nrerum ut id enim velit est perferendis"
// }, {
//     id: 72,
//     userId: 8,
//     title: "sint hic doloribus consequatur eos non id",
//     body: "quam occaecati qui deleniti consectetur\nconsequatur aut facere quas exercitationem aliquam hic voluptas\nneque id sunt ut aut accusamus\nsunt consectetur expedita inventore velit"
// }, {
//     id: 73,
//     userId: 8,
//     title: "consequuntur deleniti eos quia temporibus ab aliquid at",
//     body: "voluptatem cumque tenetur consequatur expedita ipsum nemo quia explicabo\naut eum minima consequatur\ntempore cumque quae est et\net in consequuntur voluptatem voluptates aut"
// }, {
//     id: 74,
//     userId: 8,
//     title: "enim unde ratione doloribus quas enim ut sit sapiente",
//     body: "odit qui et et necessitatibus sint veniam\nmollitia amet doloremque molestiae commodi similique magnam et quam\nblanditiis est itaque\nquo et tenetur ratione occaecati molestiae tempora"
// }, {
//     id: 75,
//     userId: 8,
//     title: "dignissimos eum dolor ut enim et delectus in",
//     body: "commodi non non omnis et voluptas sit\nautem aut nobis magnam et sapiente voluptatem\net laborum repellat qui delectus facilis temporibus\nrerum amet et nemo voluptate expedita adipisci error dolorem"
// }, {
//     id: 76,
//     userId: 8,
//     title: "doloremque officiis ad et non perferendis",
//     body: "ut animi facere\ntotam iusto tempore\nmolestiae eum aut et dolorem aperiam\nquaerat recusandae totam odio"
// }, {
//     id: 77,
//     userId: 8,
//     title: "necessitatibus quasi exercitationem odio",
//     body: "modi ut in nulla repudiandae dolorum nostrum eos\naut consequatur omnis\nut incidunt est omnis iste et quam\nvoluptates sapiente aliquam asperiores nobis amet corrupti repudiandae provident"
// }, {
//     id: 78,
//     userId: 8,
//     title: "quam voluptatibus rerum veritatis",
//     body: "nobis facilis odit tempore cupiditate quia\nassumenda doloribus rerum qui ea\nillum et qui totam\naut veniam repellendus"
// }, {
//     id: 79,
//     userId: 8,
//     title: "pariatur consequatur quia magnam autem omnis non amet",
//     body: "libero accusantium et et facere incidunt sit dolorem\nnon excepturi qui quia sed laudantium\nquisquam molestiae ducimus est\nofficiis esse molestiae iste et quos"
// }, {
//     id: 80,
//     userId: 8,
//     title: "labore in ex et explicabo corporis aut quas",
//     body: "ex quod dolorem ea eum iure qui provident amet\nquia qui facere excepturi et repudiandae\nasperiores molestias provident\nminus incidunt vero fugit rerum sint sunt excepturi provident"
// }, {
//     id: 81,
//     userId: 9,
//     title: "tempora rem veritatis voluptas quo dolores vero",
//     body: "facere qui nesciunt est voluptatum voluptatem nisi\nsequi eligendi necessitatibus ea at rerum itaque\nharum non ratione velit laboriosam quis consequuntur\nex officiis minima doloremque voluptas ut aut"
// }, {
//     id: 82,
//     userId: 9,
//     title: "laudantium voluptate suscipit sunt enim enim",
//     body: "ut libero sit aut totam inventore sunt\nporro sint qui sunt molestiae\nconsequatur cupiditate qui iste ducimus adipisci\ndolor enim assumenda soluta laboriosam amet iste delectus hic"
// }, {
//     id: 83,
//     userId: 9,
//     title: "odit et voluptates doloribus alias odio et",
//     body: "est molestiae facilis quis tempora numquam nihil qui\nvoluptate sapiente consequatur est qui\nnecessitatibus autem aut ipsa aperiam modi dolore numquam\nreprehenderit eius rem quibusdam"
// }, {
//     id: 84,
//     userId: 9,
//     title: "optio ipsam molestias necessitatibus occaecati facilis veritatis dolores aut",
//     body: "sint molestiae magni a et quos\neaque et quasi\nut rerum debitis similique veniam\nrecusandae dignissimos dolor incidunt consequatur odio"
// }, {
//     id: 85,
//     userId: 9,
//     title: "dolore veritatis porro provident adipisci blanditiis et sunt",
//     body: "similique sed nisi voluptas iusto omnis\nmollitia et quo\nassumenda suscipit officia magnam sint sed tempora\nenim provident pariatur praesentium atque animi amet ratione"
// }, {
//     id: 86,
//     userId: 9,
//     title: "placeat quia et porro iste",
//     body: "quasi excepturi consequatur iste autem temporibus sed molestiae beatae\net quaerat et esse ut\nvoluptatem occaecati et vel explicabo autem\nasperiores pariatur deserunt optio"
// }, {
//     id: 87,
//     userId: 9,
//     title: "nostrum quis quasi placeat",
//     body: "eos et molestiae\nnesciunt ut a\ndolores perspiciatis repellendus repellat aliquid\nmagnam sint rem ipsum est"
// }, {
//     id: 88,
//     userId: 9,
//     title: "sapiente omnis fugit eos",
//     body: "consequatur omnis est praesentium\nducimus non iste\nneque hic deserunt\nvoluptatibus veniam cum et rerum sed"
// }, {
//     id: 89,
//     userId: 9,
//     title: "sint soluta et vel magnam aut ut sed qui",
//     body: "repellat aut aperiam totam temporibus autem et\narchitecto magnam ut\nconsequatur qui cupiditate rerum quia soluta dignissimos nihil iure\ntempore quas est"
// }, {
//     id: 90,
//     userId: 9,
//     title: "ad iusto omnis odit dolor voluptatibus",
//     body: "minus omnis soluta quia\nqui sed adipisci voluptates illum ipsam voluptatem\neligendi officia ut in\neos soluta similique molestias praesentium blanditiis"
// }, {
//     id: 91,
//     userId: 10,
//     title: "aut amet sed",
//     body: "libero voluptate eveniet aperiam sed\nsunt placeat suscipit molestias\nsimilique fugit nam natus\nexpedita consequatur consequatur dolores quia eos et placeat"
// }, {
//     id: 92,
//     userId: 10,
//     title: "ratione ex tenetur perferendis",
//     body: "aut et excepturi dicta laudantium sint rerum nihil\nlaudantium et at\na neque minima officia et similique libero et\ncommodi voluptate qui"
// }, {
//     id: 93,
//     userId: 10,
//     title: "beatae soluta recusandae",
//     body: "dolorem quibusdam ducimus consequuntur dicta aut quo laboriosam\nvoluptatem quis enim recusandae ut sed sunt\nnostrum est odit totam\nsit error sed sunt eveniet provident qui nulla"
// }, {
//     id: 94,
//     userId: 10,
//     title: "qui qui voluptates illo iste minima",
//     body: "aspernatur expedita soluta quo ab ut similique\nexpedita dolores amet\nsed temporibus distinctio magnam saepe deleniti\nomnis facilis nam ipsum natus sint similique omnis"
// }, {
//     id: 95,
//     userId: 10,
//     title: "id minus libero illum nam ad officiis",
//     body: "earum voluptatem facere provident blanditiis velit laboriosam\npariatur accusamus odio saepe\ncumque dolor qui a dicta ab doloribus consequatur omnis\ncorporis cupiditate eaque assumenda ad nesciunt"
// }, {
//     id: 96,
//     userId: 10,
//     title: "quaerat velit veniam amet cupiditate aut numquam ut sequi",
//     body: "in non odio excepturi sint eum\nlabore voluptates vitae quia qui et\ninventore itaque rerum\nveniam non exercitationem delectus aut"
// }, {
//     id: 97,
//     userId: 10,
//     title: "quas fugiat ut perspiciatis vero provident",
//     body: "eum non blanditiis soluta porro quibusdam voluptas\nvel voluptatem qui placeat dolores qui velit aut\nvel inventore aut cumque culpa explicabo aliquid at\nperspiciatis est et voluptatem dignissimos dolor itaque sit nam"
// }, {
//     id: 98,
//     userId: 10,
//     title: "laboriosam dolor voluptates",
//     body: "doloremque ex facilis sit sint culpa\nsoluta assumenda eligendi non ut eius\nsequi ducimus vel quasi\nveritatis est dolores"
// }, {
//     id: 99,
//     userId: 10,
//     title: "temporibus sit alias delectus eligendi possimus magni",
//     body: "quo deleniti praesentium dicta non quod\naut est molestias\nmolestias et officia quis nihil\nitaque dolorem quia"
// }, {
//     id: 100,
//     userId: 10,
//     title: "at nam consequatur ea labore ea harum",
//     body: "cupiditate quo est a modi nesciunt soluta\nipsa voluptas error itaque dicta in\nautem qui minus magnam et distinctio eum\naccusamus ratione error aut"
// }];

// const container = document.querySelector('.container');

// posts.map((elem) => {
//     const card = document.createElement('div');
//     card.classList.add('card');
//     card.innerHTML = `<span class="card__id">ID - ${elem.id}</span>
//     <h3 class="card__title">${elem.title}</h3>
//     <p class="card__text">${elem.body}</p>`

//     card.onclick = () => card.classList.toggle('visited');
//     container.append(card);
// });


// Створіть кнопку на сторінці.
// // Додайте обробник подій, який буде вставляти 
// новий елемент (наприклад, <div>) при кожному кліку на кнопку.

// const btn = document.createElement('button');
// document.body.append(btn);
// btn.innerText = 'CLICK ME';
// btn.classList.add('btn');

// let div;

// let counter = 0;

// const container = document.createElement('div');
// btn.after(container);

// btn.onclick = () => {
//   counter++;
//   div = document.createElement('div');
//   div.innerText = `Clicked ${counter} times`;
//   container.append(div);
// }


/**
 * Створіть блок з текстом.
Додайте можливість зміни кольору тексту за допомогою кнопок 
(червоний, зелений, синій).
 */

// const redBtn = document.createElement('button');
// redBtn.classList.add('red', 'btn');
// document.body.append(redBtn);
// redBtn.innerText = 'Red'

// const greenBtn = document.createElement('button');
// greenBtn.classList.add('green', 'btn');
// document.body.append(greenBtn);
// greenBtn.innerText = 'Green'

// const blueBtn = document.createElement('button');
// blueBtn.classList.add('blue', 'btn');
// document.body.append(blueBtn);
// blueBtn.innerText = 'Blue'

// const text = document.createElement('h1');
// text.innerText = `I change color
// Try any button`;
// document.body.append(text);
// text.classList.add('title');

// redBtn.onclick = () => text.classList.toggle('red');
// greenBtn.onclick = () => text.classList.toggle('green');
// blueBtn.onclick = () => text.classList.toggle('blue');


/**
 * Створіть список елементів (наприклад, <ul> з <li>).
Додайте можливість видаляти елемент при кліку на нього.
 */

// const list = document.createElement('ul');
// document.body.append(list);

// let listItem;

// for(let i = 1; i <= 10; i++) {

//   listItem = document.createElement('li');
//   list.append(listItem);


// }


/**
 * Завдання 4.
 *
 * Написати функцію fillChessBoard, яка розгорне «шахівницю» 8 на 8.
 *
 * Колір темних осередків - #161619.
 * Колір світлих осередків - #FFFFFF.
 * Інші стилі CSS для дошки та осередків готові.
 *
 * Дошку необхідно розгорнути всередині елемента з класом .board.
 *
 * Кожна комірка дошки представляє елемент div із класом .cell.
 * 
 *  <div class="board">
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      ...
    </div>
 */

/* Дано */
// const LIGHT_CELL = '#ffffff';
// const DARK_CELL = '#161619';
// const V_CELLS = 8;
// const H_CELLS = 8;

// const board = document.querySelector('.board');

// const fillChessBoard = () => {

//   let cell;

//   for (let i = 1; i <= 64; i++) {
//     cell = document.createElement('div');
//     cell.classList.add('cell');
//     board.append(cell);

//     for (let j = 1; j <= 64; j += 2) {

//     }
//   }
// }

// fillChessBoard();

// console.log(board);


/**
 * Написати скрипт, який створить елемент button із текстом «Увійти».
 *
 * При натисканні на кнопці виводити alert з повідомленням: 
 * «Ласкаво просимо!».
 */

/**
 * Поліпшити скрипт із попереднього завдання.
 *
 * При наведенні на кнопку вказівником миші виводити alert 
 * з повідомленням:
 * «При натисканні кнопки ви увійдете в систему.».
 *
 * Повідомлення має виводитися один раз.
 *
 * Умови:
 * - Розв'язати завдання грамотно.
 */

// const btn = document.createElement('button');
// btn.innerText = 'Увійти';
// document.body.append(btn)

// btn.addEventListener('click', () => {
//     alert('Welcome!')
// });

// const isHovered = () => {
//     alert('При натисканні кнопки ви увійдете в систему.');
//      btn.removeEventListener('mouseover', isHovered);

// }

// btn.addEventListener("mouseover", isHovered);

// btn.addEventListener('mouseover', () => console.log('При натисканні кнопки ви увійдете в систему.'))


// window.addEventListener('scroll', () => {

//     document.querySelector('.js-scroll').style.width = `${window.scrollY/window.innerHeight *100}%`;
// })


/**
 * Написати скрипт, який створить елемент button із текстом «Увійти».
 *
 * При натисканні на кнопці виводити alert з повідомленням: «Ласкаво просимо!».
 */

/**
 * Поліпшити скрипт із попереднього завдання.
 *
 * При наведенні на кнопку вказівником миші виводити alert з повідомленням:
 * «При натисканні кнопки ви увійдете в систему.».
 *
 * Повідомлення має виводитися один раз.
 *
 * Умови:
 * - Розв'язати завдання грамотно.
 */


// const button = document.createElement('button');
// button.innerText = 'Увійти';
// document.body.append(button);

// button.addEventListener('click', () => {
//     alert('Ласкаво просимо!');
// });


// let isHovered = false;

// button.addEventListener('mouseover', () => {

//     if(!isHovered) {

//         isHovered = true;
//         alert('При натисканні кнопки ви увійдете в систему.');
//     }

// });

// const hoverOverButton = () => {
//     alert('При натисканні кнопки ви увійдете в систему.');
//     button.removeEventListener('mouseover', hoverOverButton);
// }

// button.addEventListener('mouseover', hoverOverButton);


/**
 * Створити елемент h1 з текстом «Ласкаво просимо!».
 *
 * Під елементом h1 додати елемент button з текстом «Розфарбувати».
 *
 * При натисканні кнопки змінювати колір кожної літери елемента h1 на випадковий.
 */

/**
 * При кожному русі курсору по сторінці
 * Змінювати колір кожної літери елемента h1 на випадковий.
 */


/* Дано */
// const PHRASE = 'Ласкаво просимо!';

// const phraseArr = PHRASE.split('');
// console.log(phraseArr)

// function getRandomColor() {
//     const r = Math.floor(Math.random() * 255);
//     const g = Math.floor(Math.random() * 255);
//     const b = Math.floor(Math.random() * 255);

//     return `rgb(${r}, ${g}, ${b})`;
// }


// const phraseHtml = phraseArr.map(el => `<span>${el}</span>`).join('');

// const h1 = document.createElement('h1');
// h1.innerHTML = phraseHtml;

// document.body.append(h1);

// console.log(h1);

// const button = document.createElement('button');
// button.innerText = 'Розфарбувати';
// button.style.display = 'block';
// button.style.marginTop = '20px';
// document.body.append(button);

// button.addEventListener('click', () => {

//     for (let elem of h1.children) {
//         // console.log(elem)
//         elem.style.color = getRandomColor();
//     }
// })


/**
 * При наведенні на блок 1 робити
 * блок 2 зеленим кольором
 * А при наведенні на блок 2 робити
 * блок 1 червоним кольором
 *
 */

// const div1 = document.querySelector('#block-1');
// const div2 = document.querySelector('#block-2');

// div1.addEventListener('mouseover', () => {
//     div2.style.backgroundColor = 'green';
// })

// div1.addEventListener('mouseout', () => {
//     div2.style.backgroundColor = 'transparent';
// })


// div2.addEventListener('mouseover', () => {
//     div1.style.backgroundColor = 'red';
// })

// div2.addEventListener('mouseout', () => {
//     div1.style.backgroundColor = 'transparent';
// })

/**
 * При натисканні на кнопку Validate відображати
 * VALID зеленим кольром, якщо значення проходить валідацію
 * INVALID червоним кольором, якщо значення не проходить валідацію
 *
 * Правила валідації значення:
 * - значення не пусте
 *
 * ADVANCED
 * Правила валідації значення:
 * - повинно містити щонайменше 6 символів
 * - не повинно містити пробілів
 * - повинно починатися з літери (потрібно використати регулярні вирази)
 */

// const button = document.querySelector('#validate-btn');
// const input = document.querySelector('#input');

// const p = document.createElement('p');

// button.addEventListener('click', () => {
//     if (input.value.length >= 6 && !input.value.includes(' ')) {
//         p.innerText = 'Valid';
//         p.style.color = 'green';
//         document.body.append(p);
//     } else {
//         p.innerText = 'Invalid';
//         p.style.color = 'red';
//         document.body.append(p);
//     }
// })

// input.addEventListener('input', () => {
//     p.innerText = '';
// })


/**
 * Початкове значення лічильника 0
 * При натисканні на + збільшувати лічильник на 1
 * При натисканні на - зменшувати лічильник на 1
 *
 * ADVANCED: не давати можливості задавати лічильник менше 0
 *
 */
// const counterValue = document.querySelector('#counter>span');
// const minus = document.querySelector('#decrement-btn');
// const plus = document.querySelector('#increment-btn');

// let counter = 0;

// minus.addEventListener('click', () => {

//     if (counterValue.innerText > 0) {
//         counter--;
//         counterValue.innerText = counter;
//     }


// });

// plus.addEventListener('click', () => {
//     counter++;
//     counterValue.innerText = counter;

// })


/**
 * Зробити список покупок з можливістю
 * додавати нові товари
 *
 * В поле вводу ми можемо ввести назву нового елементу
 * для списку покупок
 * При натисканні на кнопку Add введене значення
 * потрібно додавати в кінець списку покупок, а
 * поле вводу очищувати
 *
 * ADVANCED: Додати можливість видаляти елементи зі списку покупок
 * (додати Х в кінець кожного елементу, при кліку на який відбувається
 * видалення) та відмічати як уже куплені (елемент перекреслюється при
 * кліку на нього)
 * Для реалізації цього потрібно розібратися з Event delegation
 * https://javascript.info/event-delegation
 */


// const input = document.querySelector('#new-good-input');
// const button = document.querySelector('#add-btn');
// const shoppingList = document.querySelector('.shopping-list');

// button.addEventListener('click', () => {
//     const listItem = document.createElement('li');
//     listItem.innerText = input.value;
//     shoppingList.append(listItem);
//     input.value = '';


// })

// const div = document.createElement('div');
// div.classList.add('test');
// document.body.append(div);

// const button = document.createElement('button');

// button.innerText = 'Click Me';
// div.append(button);

// button.addEventListener('click', (event) => {
// console.log(event.target);
// console.log(event.currentTarget);
// console.log(event.clientX);
// console.log(event.clientY);
// console.log(event.type);
// console.log(event.button);
// console.log(event.pageX);
// console.log(event.pageY);
// });

// button.addEventListener('click', (event) => {
//   if (event.altKey && event.shiftKey) {
//     console.log('alt+shift+click');
//   }
// });

// button.addEventListener('mouseover', (event) => {
//   console.log(event.target);
//   console.log(event.relatedTarget);
//   // console.log(event.currentTarget);

// })
// button.addEventListener('mouseout', (event) => {
//   console.log(event.target);
//   console.log(event.relatedTarget);
//   // console.log(event.currentTarget);

// })

// let countDown = 0;
// let countUp = 0;

// window.addEventListener('keydown', (event) => {
//   countDown++;
//   console.log(countDown);
//   console.log(event.repeat)
// })

// window.addEventListener('keyup', (event) => {
//   countUp++;
//   console.log(countUp);
//   console.log(event.repeat)
// })

// window.addEventListener('wheel', (event) => {
//   console.log('wheel')
// })


// console.log(document.forms.test.elements.text.value)

// const select = document.querySelector('#test-id');

// console.log(select.options)

// for (let el of select.options) {
//   console.log(el.text)
// }

// console.log(select.value)

// const form = document.forms.test;
// console.log(form.elements);

// window.addEventListener('keydown', (event) => {
//   if (event.key === 'Enter') {
//     console.log(event.key)
//   }
// })

/**
 * Завдання 1.
 *
 * Створити елемент h1 з текстом «Натисніть будь-яку клавішу».
 *
 * При натисканні будь-якої клавіші клавіатури змінювати текст 
 * елемента h1 на:
 * «Натиснена клавіша: ІМ'Я_КЛАВИШІ».
 */

// document.body.insertAdjacentHTML('beforeend', 
// '<h1>Натисніть будь-яку клавішу</h1>');

// const h1 = document.querySelector('h1');
// console.log(h1.innerText)


// window.addEventListener('keydown', (event) => {
//   h1.innerText = `Натиснена клавіша: ${event.key}`
// })

/**
 * Рахувати кількість натискань на пробіл, ентер,
 * та Backspace клавіші
 * Відображати результат на сторінці
 *
 * ADVANCED: створити функцію, яка приймає тільки
 * назву клавіші, натискання якої потрібно рахувати,
 * а сам лічильник знаходиться в замиканні цієї функції
 * (https://learn.javascript.ru/closure)
 * id елемента, куди відображати результат має назву
 * "KEY-counter"
 *
 * Наприклад виклик функції
 * createCounter('Enter');
 * реалізує логіку підрахунку натискання клавіші Enter
 * та відображає результат у enter-counter блок
 *
 */

// const spaceCounter = document.querySelector('.space-counter');
// const enterCounter = document.querySelector('.enter-counter');
// const backspaceCounter = document.querySelector('.backspace-counter');

// // const spanCollection = document.querySelectorAll('span');
// // console.log(spanCollection)

// let counter = 0;

// console.log(spaceCounter.innerText)

// // Space Enter Backspace

// window.addEventListener('keydown', (event) => {

//   // spanCollection.forEach(element => element.classList.add('black'));

//   const activeElement = document.querySelector('.active');
//   if (activeElement) {
//     activeElement.classList.remove('active');
//   }


//   if (event.code === 'Space') {
//     counter++;
//     spaceCounter.innerText = counter;
//     spaceCounter.classList.add('active');
//   }

//   if (event.code === 'Enter') {
//     counter++;
//     enterCounter.innerText = counter;
//     enterCounter.classList.add('active');
//   }

//   if (event.code === 'Backspace') {
//     counter++;
//     backspaceCounter.innerText = counter;
//     backspaceCounter.classList.add('active');
//   }

// })


// window.addEventListener('keydown', (event) => {

//   const el = document.querySelector(`.${event.code.toLowerCase()}-counter`);

//   const activeElement = document.querySelector('.active');

//   if (activeElement) {
//     activeElement.classList.remove('active');
//   }

//   if (el) {
//     el.innerText = +el.innerText + 1;
//     el.classList.add('active');
//   }


// });


// console.log(document.body.clientHeight)
// console.log(document.body.clientWidth)
// console.log(window.scrollX)

// window.addEventListener('scroll', ()=> {
//   console.log(window.scrollY)
// })

/*
    1. При положенні скролла сторінки більше 100px додавати до хедеру 
    стилі для тіні (`class="shadow"`). При положенні скролла менше 100px 
    тіні не повинно бути.
    2. При положенні скролл сторінки більше висоти показувати кнопку 
    scrollToTop. 
    При менше – приховувати.
    3. При натисканні на кнопку - плавно скролити до початку документа.
 */

// const header = document.querySelector('.header');

// const button = document.querySelector('#scroll-to-top');

// window.addEventListener('scroll', () => {

//   if (window.scrollY > 100) {
//     header.classList.add('shadow')
//   } else {
//     header.classList.remove('shadow')
//   }

//   console.log(window.innerHeight)
//   console.log(document.body.clientHeight)

//   if (window.scrollY > window.innerHeight) {
//     button.style.display = 'flex';
//   } else {
//     button.style.display = 'none';
//   }


// });

// button.addEventListener('click', () => {
//   window.scrollTo({
//     top: 0,
//     left: 0,
//     behavior: "smooth",
//   });
// })


// const form = document.querySelector('form');


// form.addEventListener('submit', (event) => {
//   event.preventDefault();
//   const inputList = event.target.querySelectorAll('input');
//   console.log(inputList);

//   const values = {};

//   inputList.forEach(el => values[el.name] = el.value);
//   console.log(values)

//   form.reset();

// });

// const inputLogin = document.querySelector('input[name="login"]');
// console.log(inputLogin);

// inputLogin.addEventListener('copy', (event) => {
//   console.log(event.type + '-' + document.getSelection());
//   event.preventDefault();

// })

/*

При натисканні на сабміт збирати всі значення з інпутів в об'єкт 
і виводити в консоль. Очищати усі поля введення. 
Сторінка не повинна перезавантажуватись!
   {
      name: 'John',
      tel: '+76546623462',
   }

   
Реалізувати подію сабміта форми при натисканні 
Enter 


При введенні в інпут тексту забороняти вводити 
спец. символи html тегів (<, >, </)


За розфокусом з інпуту repeatPassword 
перевіряти чи відповідають введені дані полю password. 
Якщо ні – показувати помилку (додавати текст червоного 
кольору під полем введення). За фокусом приховувати помилку.

*/

// const values = {};

// document.querySelector('.sign-up-form').addEventListener('submit', (event) => {
//   event.preventDefault();

//   const inputList = event.target.querySelectorAll('input');
//   console.log(inputList);

//   inputList.forEach(el => {
//     values[el.name] = el.value
//   });
//   console.log(values);

//   event.target.reset();
// });

// const inputName = document.querySelector('#login');
// console.log(inputName);

// inputName.addEventListener('input', (event) => {
//   event.target.value = event.target.value.replaceAll('>', '').replaceAll('<', '');

// });

// const password = document.querySelector('#password');
// const repeatPassword = document.querySelector('#repeat_password');
// const eyeDiv = document.querySelector('.icon');
// console.log()

// eyeDiv.addEventListener('click', () => {

//   if (password.type === 'password') {
//     password.type = 'text';
//   } else {
//     password.type = 'password';
//   }

// });

// repeatPassword.addEventListener('blur', (event) => {

//   if (event.target.value !== password.value) {
//     document.querySelector('.error').innerText = 'Wrong password';
//   }

// });

// repeatPassword.addEventListener('focus', (event) => {
//   document.querySelector('.error').innerText = '';
// })


/**
 * Завдання 1.
 *
 * Створити елемент h1 з текстом «Натисніть будь-яку клавішу».
 *
 * При натисканні будь-якої клавіші клавіатури змінювати текст елемента h1 на:
 * «Натиснена клавіша: ІМ'Я_КЛАВИШІ».
 */





/**
 * Рахувати кількість натискань на пробіл, ентер,
 * та Backspace клавіші
 * Відображати результат на сторінці
 *
 * ADVANCED: створити функцію, яка приймає тільки
 * назву клавіші, натискання якої потрібно рахувати,
 * а сам лічильник знаходиться в замиканні цієї функції
 * (https://learn.javascript.ru/closure)
 * id елемента, куди відображати результат має назву
 * "KEY-counter"
 *
 * Наприклад виклик функції
 * createCounter('Enter');
 * реалізує логіку підрахунку натискання клавіші Enter
 * та відображає результат у enter-counter блок
 *
 */

// window.addEventListener('keydown', (event) => {

//   const activeEl = document.querySelector('.active');
//   console.log(activeEl);

//   if (activeEl) {
//     activeEl.classList.remove('active');
//   }

//   const counter = document.querySelector(`#${event.code.toLowerCase()}-counter`);

//   if (counter) {
//     counter.innerText = +counter.innerText + 1;
//     counter.classList.add('active');
//   }
// });


/**
 * При натисканні shift та "+" одночасно збільшувати
 * шрифт сторінки на 1px
 * а при shift та "-" - зменшувати на 1px
 *
 * Максимальний розмір шрифту - 30px, мінімальний - 10px
 *
 */



// const title = document.querySelector('.title');
// title.classList.add('font-size');
// let curentFontSize = 16;

// window.addEventListener('keydown', (event) => {

//   if (event.shiftKey) {

//     if (event.code === 'NumpadAdd') {
//       curentFontSize++;
//       title.style.fontSize = `${curentFontSize}px`
//     }

//     if (event.code === 'NumpadSubtract') {
//       curentFontSize--;
//       title.style.fontSize = `${curentFontSize}px`
//     }
//   }
// });




/**
 * При натисканні на enter у полі введення
 * додавати його значення, якщо воно не порожнє,
 * до списку завдань та очищувати поле введення
 *
 * При натисканні Ctrl + D на сторінці удаляти
 * Останнє додане завдання
 *
 * Додати можливість очищувати весь список
 * Запустити очищення можна двома способами:
 * - при кліці на кнопку Clear all
 * - при натисканні на Alt+Shift+Backspace
 *
 * При очищенні необхідно запитувати у користувача підтвердження
 * (показувати модальне вікно з вибором Ok/Cancel)
 * Якщо користувач підвердить видалення, то очищувати список,
 * інакше нічого не робити зі списком
 *
 */
// const input = document.querySelector('#new-task');
// const taskList = document.querySelector('.tasks-list');


// window.addEventListener('keydown', (event) => {

//   console.log(event.key === 'd');
//   if (event.code === 'Enter') {

//     if (input.value) {
//       taskList.insertAdjacentHTML('beforeend', `<li>${input.value}</li>`);
//       input.value = '';
//     }
//   }


//   if (event.key === 'Control') {
//     const lastEl = document.querySelector('.tasks-list li:last-of-type');
//     if (confirm('Are you sure?')) {
//       lastEl.remove();
//     }
//   }
// });

// const button = document.querySelector('#clear');
// button.addEventListener('click', () => {
//   document.querySelectorAll('.tasks-list li').forEach(el => el.remove());
// });

// const ship = document.querySelector('#space-ship');

// console.log(ship.style.transform)

// // ArrowUp ArrowDown ArrowLeft ArrowRight

// window.addEventListener('keydown', (event) => {

//   if (event.code = 'ArrowUp') {
//     ship.style.transform = 'translateX(10px)'

//   }

// });

// const one = document.querySelectorAll('.div');


// one.forEach(el => {
//   el.addEventListener('click', (event) => {
//     console.log(`${event.currentTarget.tagName}: ${event.eventPhase}`);
//   }, { capture: true});
// })
// const two = document.querySelector('.div2');
// const three = document.querySelector('.div3');

// one.addEventListener('click', (event) => {
//   console.log('1');
//   console.log(event.target);
// });

// two.addEventListener('click', (event) => {
//   console.log('2');
//   console.log(event.target)

// });

// three.addEventListener('click', (event) => {
//   console.log('3');
//   console.log(event.target);


// });


// const getRandomRGB = () => `rgb(${Math.ceil(Math.random() * 255)}, ${Math.ceil(Math.random() * 255)}, ${Math.ceil(Math.random() * 255)})`

// const getCardHtml = (index) => `
//   <div class='flip-card'>
//     <div class='flip-card-inner'>

//       <div class='flip-card-front js-click' style='background-color: ${getRandomRGB()}'>
//       	<h1>Card ${index}</h1>
// 			</div>

//       <div class='flip-card-back'>
//         <h1>Random Number</h1>
//         <h2>${Math.floor(Math.random() * 100)}</h2>
//       </div>

//     </div>
//   </div>
// `
// const cardContainer = document.querySelector('.card-container');
// const cardHtmlArray = new Array(100).fill(null).map((el, index) => getCardHtml(index));
// cardContainer.insertAdjacentHTML('beforeend', cardHtmlArray.join(''));


// cardContainer.addEventListener('click', (event) => {

//   if (event.target === event.currentTarget) {
//     return;
//   }

//   const elem = event.target.closest('.flip-card-inner');
//   elem.classList.toggle('show-card-back');



//   // if (event.target.className.includes('flip-card-front')) {
//   //   event.target.parentElement.classList.toggle('flip-card-back');
//   // }




//   // console.log(event.target);
// });




/**
 * Завдання 1.
 *
 * Необхідно "оживити" навігаційне меню за допомогою JavaScript.
 *
 * При натисканні на елемент меню додавати до нього CSS-клас .active.
 * Якщо такий клас вже існує на іншому елементі меню, необхідно
 * з того, що попереднього елемента CSS-клас .active зняти.
 *
 * Кожен елемент меню — це посилання, що веде на google.
 * За допомогою JavaScript необхідно запобігти перехід по всіх посилання 
 * на цей зовнішній ресурс.
 *
 * Умови:
 * - У реалізації обов'язково використовувати прийом делегування подій 
 * (на весь скрипт слухач має бути один).
 */

// const liCollection = document.querySelector('.nav__list');
// let activeEl = null;

// liCollection.addEventListener('click', (event) => {
//   event.preventDefault();

//   if (event.target.tagName === 'A') {
//     activeEl?.classList.remove('active');
//     event.target.classList.add('active');
//     activeEl = event.target;
//   }

// });


// На кліку на картинку - створювати дубль цієї картинки, наприкінці 
// списку картинок.
// Використовувати делегування подій

// document.body.addEventListener('click', (event) => {

//   if (event.target === event.currentTarget) {
//     return;

//   } else if (event.target.tagName === 'IMG') {
//     document.body.append(event.target.cloneNode(true));
//   }
// })


/**
 * Оживити екранну клавіатуру. При натисканні на кнопки клавіатури 
 * додавати відповідний символ в input.
 * При натисканні кнопки Del очищати текст в input
 */

// const quertyChars = ['q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '{', '}', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', '\'', 'z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.', '/']

// const getKeyHtml = (key) => `<div class='key'><span>${key}</span></div>`



// const paintKeyboard = () => {
//   const keyboardsContainers = document.querySelector('.keyboard-container');

//   quertyChars.forEach((char, index) => {
//     if (index < 12) {
//       keyboardsContainers.children[0].insertAdjacentHTML('beforeend', getKeyHtml(char))
//       return
//     }

//     if (index < 23) {
//       keyboardsContainers.children[1].insertAdjacentHTML('beforeend', getKeyHtml(char))
//       return
//     }

//     keyboardsContainers.children[2].insertAdjacentHTML('beforeend', getKeyHtml(char))
//   })

//   keyboardsContainers.insertAdjacentHTML('beforeend', '<div class="key space"> </div>')
// }

// paintKeyboard();

// const keyboard = document.querySelector('.keyboard-container');
// const input = document.querySelector('#text');


// keyboard.addEventListener('click', (event) => {

//   if (event.target.className.includes('space')) {
//     input.value += ' ';
//   }

//   if (event.target === event.currentTarget) {
//     return;

//   } else if (event.target.parentElement === event.currentTarget) {
//     return;
//   }

//   input.value += event.target.innerText;

// })


// Дані інпути. Зробіть так, щоб усі інпути втрати фокусу перевіряли 
// свій вміст
// На правильну кількість символів. Скільки символів має бути в інпуті,
// Вказується в атрибуті data-length.
// Якщо вбито правильну кількість, то межа інпуту стає зеленою,
// якщо неправильне – червоною.

// const input = document.querySelectorAll('.input');
// // console.log(input);

// input.forEach(el => {
//   // console.log(el.dataset.length)

//   el.addEventListener('blur', (event) => {

//     console.log(event.target.className)


//     if (event.target.value.length === +el.dataset.length) {

//       if (event.target.className.includes('invalid')) {
//         event.target.classList.remove('invalid')
//       }
//       event.target.classList.add('valid')
//     } else {
//       event.target.classList.add('invalid')
//     }
//   })
// })
// const board = document.querySelector('.board');
// board.addEventListener('click', (event) => {
//   event.target.classList.toggle('clicked')
// })
// let x = 0;

// const interval = setInterval(() => {
//    x++
//    console.log(x)

//    if (x > 5) {
//       clearInterval(interval)
//    }
// }, 1000);


// setTimeout((name) => {
// console.log(name)
// }, 1000, 'Inna');



// const btn = document.querySelector('button');
// const dog = document.querySelector('.dog');
// const bark = document.querySelector('.dog span');
// console.log(dog);

// btn.addEventListener('click', () => {
//    dog.classList.add('jump');

//    setTimeout(() => {
//       bark.style.display = 'block';
//    }, 300);

//    setTimeout(() => {
//       dog.classList.remove('jump');
//       bark.style.display = 'none';
//    }, 2000);
// })








// const formatTime = (time) => {
//    if (time.toString().length > 1) {
//       return time.toString();
//    }

//    return `0${time}`;
// }

// const minutesElem = document.querySelector('.minutes');
// const secondsElem = document.querySelector('.seconds');
// const startButton = document.querySelector('.start');
// const stopButton = document.querySelector('.stop');
// const resetButton = document.querySelector('.reset');

// let minutes = +localStorage.getItem('minutes') || 0;
// let seconds = +localStorage.getItem('seconds') || 0;
// let intervalId = null;
// minutesElem.innerText = formatTime(minutes);
// secondsElem.innerText = formatTime(seconds);

// startButton.addEventListener('click', () => {

//    if (intervalId) {
//       clearInterval(intervalId);
//    }

//    intervalId = setInterval(() => {
//       seconds++;

//       if (seconds === 60) {
//          seconds = 0;
//          minutes++;
//          secondsElem.innerText = '';
//          minutesElem.innerText = formatTime(minutes);
//          localStorage.setItem('minutes', minutes);
//       }

//       secondsElem.innerText = formatTime(seconds);
//       localStorage.setItem('seconds', seconds);

//    }, 100);
//    startButton.disabled = true;

// });

// stopButton.addEventListener('click', () => {
//    clearInterval(intervalId);
//    startButton.disabled = false;
// });

// resetButton.addEventListener('click', () => {
//    clearInterval(intervalId);
//    minutes = 0;
//    seconds = 0;
//    minutesElem.innerText = formatTime(minutes);
//    secondsElem.innerText = formatTime(seconds);
//    startButton.disabled = false;
//    localStorage.removeItem('seconds');
//    localStorage.removeItem('minutes');
// })







// let x = localStorage.getItem('xCounter') || 0;

// const title = document.querySelector('.title');
// title.innerText = x;

// const btn = document.querySelector('.button');

// btn.addEventListener('click', () => {
//     x++;
//     title.innerText = x;
//     localStorage.setItem('xCounter', x);

// });

// const user = {
//    name: 'Inna',
//    age: 35
// }

// localStorage.setItem('user', JSON.stringify(user));
// const user2 = localStorage.getItem('user');
// console.log(user2)

// console.log(JSON.parse(user2))




/*
Збереження введених даних

Реалізуйте функціонал збереження в localStorage всіх даних,
 введених у поля форми. 
 (Це робиться для того, щоб зберегти введені 
    дані при випадковому перезавантаженні сторінки).

Щоразу при виході з поля (подія blur) 
зберігати дані в localStorage форматі 
key = input.name, value = input value;
При перезавантаженні перевіряти наявність 
запису в localStorage і відновлювати його в полі якщо є;

Реалізувати все через один запис в localStorage, 
в якому зберігатиметься об'єкт з полями key = input.name, 
value = input value; Використовувати JSON.stringify та JSON.parse
*/

const inputCollection = document.querySelectorAll('.form input');
const form = document.querySelector('.form');
const values = {};

inputCollection.forEach(input => {

   const localStorageItem = localStorage.getItem(input.name);

   if (localStorageItem) {

      if (input.type === 'checkbox') {
         input.checked = localStorageItem === 'true' ? true : false;
      } else {
         input.value = localStorageItem;
      }
   }

   input.addEventListener('blur', (event) => {
      localStorage.setItem(event.target.name, input.type === 'checkbox' ? event.target.checked : event.target.value);

   })
});

form.addEventListener('submit', (event) => {
   event.preventDefault()
   inputCollection.forEach(input => {
      values[input.name] = input.value;
      localStorage.removeItem(input.name)
      
   })


   console.log(values)
   event.target.reset()
   
})



















































