'use strict';

// FUNCTION DECLARATION-------------------------------------

// function showName(name) {
//     console.log(`My name is ${name}`);
//     return;
// }

// showName(prompt('Enter your name'));

// function greet(name) {
//     return `Hello, ${name}`;
// }

// console.log(greet(prompt('Your name')));


// FUNCTION EXPRESSION----------------------------------

// const greet = function(name) {
//     return `Hello, ${name}`;
// }

// console.log(greet('Inna'));



// NAMED FUNCTION EXPRESSION-------------------------------

// const getName = function yourName() {
//     return prompt('What is your name?');
// }

// console.log(getName());

// ---------------------------------------------------------

// const summ = function(a, b) {
//     return a + b;
// }

// console.log(summ(2, 5));

// -----------------------------------------------------

// const getName = function() {
//     return prompt('What is your name?');
// }

// // console.log(getName());

// // let userName = getName();
// // console.log(userName);

// let userName = getName;

// // userName();

// console.log(userName());


// -----------------------------------------------------------

// const multiply = function(b, a) {

//     return `${a} * ${b} = ${a * b}`;
// }

// console.log(multiply(6, 3));






// ARROW FUNCTIONS---------------------------------------------

// const summ = (a, b) => a + b;
// console.log(summ(5, 6));

// const greet = () => 'Hello, world!';
// console.log(greet());

// const retObj = name => ({greeting: 'Hello', name})


// console.log(retObj('Inna'));


// const retArg = function () {

//     let summ = 0;

//     for (let i = 0; i < arguments.length; i++) {

//         summ += arguments[i];
//     }


//     return summ;

// }

// console.log(retArg(5, 2, 3));

// const func = function() {

//     return arguments[2];

// }

// console.log(func(1, 2, 3, 4));

// const user = {
//     name : 'Inna',
//     age: 33,

//     getName: function() {
//         alert(this.name);
//     },

//     getAge: () => alert(user.age),
// }

// user.getName();
// user.getAge();




// Напишіть функцію, яка приймає два параметри (числа) і повертає їхню суму.

// const summ = (a, b) => a + b;
// console.log(summ(5, 6));

// Створіть функцію, яка підносить число до заданого ступеня.

// const elevation = (a, b) => a**b;
// console.log(elevation(2, 3));

// Напишіть функцію, яка перевіряє, чи є задане число парним.

// const isEven = a => a % 2 === 0;
// console.log(isEven(5));

// Створіть функцію, яка знаходить найменше число з двох переданих.

// const isLess = (a, b) => a < b ? a : b;
// console.log(isLess(9, 7));

// Створіть функцію, яка обчислює об'єм куба за його ребром.

// V = a**3

// const calcVolume = a => a**3;
// console.log(calcVolume(5));



// Створіть функцію, яка генерує випадкове число від 1 до 100.

// const randomNumber = function() {
//     let randomNumber = parseInt(Math.random() * 100);
//     return randomNumber;
// }

// console.log(randomNumber());

// const randomNumber = () => parseInt(Math.random() * 100);
// console.log(randomNumber());



// Напишіть функцію, яка конвертує температуру з Цельсія в Фаренгейт і навпаки.


// °F = (°C × 1.8) + 32
// °С = (°F - 32) / 1.8


// const convertTemp = () => {

//     let measurement = prompt('C or F?', 'C, F');
//     let tempValue = +prompt('Enter temperature');
//     let result;

//     switch (measurement) {
//         case 'C': {
//             result = ((tempValue * 1.8) + 32) + '° F';
//             break;
//         }

//         case 'F': {
//             result = ((tempValue - 32) / 1.8) + '° C';
//             break;
//         }

//         default: {
//             result = 'Wrong data';
//         }
//     }

//     return `Temperature is ${result}`;
// }
// console.log(convertTemp());





// Напишіть функцію, яка приймає два числа і повертає більше з них.

// const isBiggerNumber = (a, b) => a > b ? a : b;
// console.log(isBiggerNumber(12, 10));




// Створіть функцію, яка перевіряє, чи є задане число парним чи непарним.

// const isEven = a => a % 2 === 0 ? 'Is even' : 'Is not even';

// console.log(isEven());




// Створіть функцію, яка повторює заданий рядок певну кількість разів.

// const multiplyString = (a, string) => {

//     let result = '';

//     for (let i = 0; i < a; i++) {
//         result += string;
//     }

//     return result;
// }

// console.log(multiplyString(3, 'Hello '));




// Напишіть функцію, яка генерує випадкове ціле число в заданому діапазоні.


// const getRandomNumber = () => parseInt(Math.random() * 100);

// console.log(getRandomNumber());



// Створіть функцію, яка обчислює суму чисел від 1 до заданого числа N.

// const summ  = (n) => {
//     let result = null;

//     for (let i = 0; i <= n; i++) {
//         result += i;
//     }

//     return result;
// }

// console.log(summ(10));





// Напишіть функцію, яка приймає два рядки і об'єднує їх у один.


// const concatinateStrings = (stringOne, stringTwo) => `${stringOne}, ${stringTwo}!`;

// console.log(concatinateStrings('Hello', 'Inna'));



// Створіть функцію, яка обчислює середнє значення трьох чисел.

// const findMiddle = (a, b, c) => (a + b + c) / 3;

// console.log(findMiddle(22, 65, 89));


// Створіть функцію з ім'ям greet, яка приймає ім'я в якості аргументу 
// та виводить привітальне повідомлення.
// Викличте функцію з різними іменами.

// const greet = (name = 'Inna') => `Hello, ${name}!`;

// console.log(greet('User'));


// Напишіть функцію з ім'ям square, яка приймає число в якості 
// аргументу та повертає його квадрат.
// Викличте функцію з різними числами та зареєструйте результат.

// const square = a => a**2;
// console.log(square(5));



// Створіть функцію з ім'ям isEven, яка приймає число в якості аргументу 
// і повертає true, 
// якщо число парне, і false в іншому випадку.

// const isEven = a => a % 2 === 0;
// console.log(isEven(2));


/**
 * Напишіть функцію з ім'ям greetWithDefault, яка приймає ім'я в 
 * якості аргументу і повідомлення за замовчуванням. Якщо повідомлення 
 * не вказано, використовуйте повідомлення за замовчуванням.
Викличте функцію з і без власного привітання.
 */


// const greetWithDefault = (name, message = prompt('You forgot your message')) => {
//     return `Hello, ${name}, ${message}`;
// }

// console.log(greetWithDefault('Inna', 'How are you?'));


// Створіть функцію з ім'ям printNumbers, яка приймає число в якості аргументу і 
// виводить всі числа від 1 до цього числа, використовуючи цикл.


// const printNumbers = (number) => {

//     for (let i = 1; i <= number; i++) {
//         console.log(i);
//     }

// }

// printNumbers(5);


// Створіть функцію з ім'ям calculateArea, яка приймає радіус і висоту циліндра в 
// якості параметрів і повертає об'єм циліндра за формулою: π * r^2 * h.

// const calculateArea = (r, h) => {

//     const pi = 3.14;

//     return pi * r**2 * h;

// }

// console.log(calculateArea(5, 10));


// Напишіть функцію з ім'ям multiply, яка приймає два аргументи та повертає їхнє добуток. 
// Використовуйте значення 
// за замовчуванням, якщо один з аргументів не передано.


// const multiply = (a = 2, b = 3) => a * b;

// console.log(multiply(5, 6));


// Створіть функцію printEvenNumbers, яка приймає число як 
// аргумент та виводить всі парні числа від 1 до цього числа.


// const printEvenNumbers = (number) => {

//     for (let i = 1; i <= number; i++) {

//         if (i % 2 === 0) {
//             console.log(i);
//         }

//     }
// }

// console.log(printEvenNumbers(10));




// Реалізуйте рекурсивну функцію з ім'ям countdown, яка приймає число і виводить 
// усі числа від цього числа до 1 у спадному порядку.


// const countdown = (n) => {

//     if (n <= 1) {
//         return console.log(n);
//     } else {
//         console.log(n);
//         countdown(n - 1);
//     }
// }

// countdown(10);

// Створіть функцію operate яка приймає два числа 
// та функцію зворотного виклику. Застосуйте функцію 
// зворотнього виклику до обох чисел та поверніть результат.


// const showResult = (a) => console.log(a);

// const operate = (a, b, show) => {
//     return show(a + b);
// }

// operate(5, 6, showResult);


// Напишіть функцію checkSign, яка приймає число та повертає рядок 
// "Positive", "Negative" або "Zero", 
// залежно від того, чи є число додатнім, від'ємним або нульовим.


// const checkSign = (n) => {
//     if (n > 0) {
//         return "Positive";

//     } else if (n < 0) {
//         return "Negative";
//     } 

//     return "Zero";
// }

// console.log(checkSign(0));





// Реалізуйте рекурсивну функцію з ім'ям sumTo,
// яка приймає число n і повертає суму чисел від 1 до n.


// const sumTo = (n) => {

//     if (n === 1) {
//         return 1;

//     } else {
//         return n + sumTo(n - 1); // 5 + 4 + 3 + 2 + 1
//     }

// }

// console.log(sumTo(5));


/**
 * Реалізуйте функцію з ім'ям printPattern, яка приймає число як 
 * параметр і виводить малюнок за допомогою рекурсії. Наприклад, 
 * якщо введено 3, малюнок буде:
markdown
 */


// const printPattern = (n) => {

//     if (n <= 0) {
//         return;

//     } else {

//         console.log('*'.repeat(n));
//         printPattern(n - 1);
//     }

// }

// printPattern(10);


