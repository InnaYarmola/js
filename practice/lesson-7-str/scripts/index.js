'use strict';

// const str = new String('My String');
// console.log(str)
// console.log(str.length);

//ПОШУК ВСЕРЕДИНІ РЯДКА

// console.log(str.indexOf('r'));
// console.log(str.lastIndexOf('S'));
// console.log(str.includes('r', 5));
// console.log(str.startsWith('M'));
// console.log(str.endsWith('g'));

// console.log(str.toUpperCase());
// console.log(str.toLowerCase());

// console.log(str.trimEnd().length);
// console.log(str.length);


// const str = new String('My String@com');
// // console.log(typeof str);

// const tester = '@'

// console.log(tester.test(str));

// console.log(str2);

// const str3 = str2.join('');
// console.log(str3)

// console.log(str.slice());

// const str2 = str.concat('Hello');

// console.log('A'.charCodeAt(0))
// console.log('a'.charCodeAt(0))

// console.log(str2)

// let date = new Date(2023, 11, 25);

// console.log(date.toString());
// console.log(date.toDateString());
// console.log(date.toTimeString());

// const timestamp = Date.now(date);
// console.log(timestamp)

// console.log(date.setMonth(3));

// const date = new Date();
// // date.setTime(5);
// // console.log(date.toString());

// const bd = new Date(1989, 6, 6);
// console.log(bd);

// const today = new Date();
// console.log(today);

// console.log((today - bd) / 1000 / 60 / 60 / 24 / 365.25)




'use strict';

// const str = 'Hello';
// console.log(str.length);
// console.log(str.charAt(str.length - 1));

// for (let char of str) {
//     console.log(char);
// }

// for (let i = 0; i < str.length; i++) {
//     console.log(str[i])
// }

// console.log(str.toLowerCase());
// console.log(str.toUpperCase());

// const str = 'Hello Inna, Hello Vova';

// const index = str.indexOf('Hello');
// console.log(index);

// const str = 'Hello World, Hello People, Hello Universe';

// let startPosition = 0;
// let index = null;

// while (index !== -1) {
//     index = str.indexOf('Hello', startPosition);
//     startPosition+= index + 1;

//     if (index !== -1) {
//         console.log('Знайшовся тут: ', index);
//     }
// }

// const str = 'Hello Inna, Hello Vova';

// console.log(str.includes(''))

// const email = '@inna@example.com';

// if (!email.endsWith('@')) {
//     console.warn('!!!')
// }

// const str = "  Aaaa Baaa Caaa  ";
// const str2 = 'A B C';
// const compare = str.trim().toUpperCase().slice(0, 5);
// console.log(compare); 

/**
 * Завдання 3.
 *
 * Написати функцію, upperCaseAndDoubly, яка перекладає
 * символи рядка у верхній регістр і дублює кожен її символ.
 *
 * Умови:
 * - Використовувати вбудовану функцію repeat;
 * - Використовувати вбудовану функцію toUpperCase;
 * - Використовувати цикл for...of.
 */


// const upperCaseAndDoubly = (str) => {

//     let upperCaseAndDoubly = '';

//     for (let char of str) {

//         upperCaseAndDoubly += char.repeat(2);
//         console.log(upperCaseAndDoubly);
//     }

//     // for (let i = 0; i < str.length; i++) {
//     //     upperCaseAndDoubly += str[i].repeat(2)
//     // }

//     return upperCaseAndDoubly.trim().toUpperCase();
// }

// console.log(upperCaseAndDoubly('   Hello   '));



// Напишіть функцію, яка приймає рядок як аргумент
// і перетворює регістр першого символу рядка з 
// нижнього регістра у верхній.

// const capitalize = (str) => {
//     let capitalizedStr = str[0].toUpperCase();
//     let other = str.slice(1);
//     return capitalizedStr + other;
// }

// console.log(capitalize('hello'));


/**
 * Завдання 1.
 *
 * Написати імплементацію вбудованої функції рядка 
 * repeat(times).
 *
 * Функція повинна мати два параметри:
 * - Цільовий рядок для пошуку символу за індексом;
 * - Кількість повторень цільового рядка.
 *
 * Функція повинна повертати перетворений рядок.
 *
 * Умови:
 * - Генерувати помилку, якщо перший параметр не 
 * є рядком, а другий не числом.
 */

// const repeat = (str, symb, x) => {
//     if (typeof str === 'string') {
//         let newStr = '';
//         let index = null; 
//         for (let char of str) {   

//             if (char === symb) { 
//                 index = str.indexOf(symb);  
//                 newStr += str[index].repeat(x-1);   
//             }
//             newStr += char;  
//         }    
//         return newStr;
//     } else {
//         console.warn('Not a string');
//     }
// }
// console.log(repeat('Inna', 'n', 2));



// const date = new Date(-1000 * 60 * 60 * 24 * 365.25);
// console.log(date);
// const timestamp = Date.now();
// console.log(timestamp);
// console.log(new Date(2023, 11 ));

// const date = new Date();
// date.setHours(23)
// console.log(date);

// console.log(date.getFullYear());
// console.log(date.getMonth());
// console.log(date.getTime());

// console.log(date.setFullYear(2024))

// const now = new Date();
// console.log(now);
// const hours = now.getHours();
// console.log(hours);
// now.setHours(now.getHours() + 2);
// console.log(now);
// now.setDate(now.getDate() + 3);
// console.log(now)

// const now = new Date();

// const fivedays = 1000 * 60 * 60 * 24 * 5;

// console.log(new Date(+fivedays + +now));

// const newYear =new Date(2024, 0, 1);
// const now = Date.now();

// console.log(Math.floor((newYear - now) / 1000 / 60 / 60 / 24));




// Виведіть на екран поточну дату-час у форматі '12:59:59 31.12.2014'.
// Для вирішення цього завдання напишіть функцію,
// яка буде додавати 0 перед днями та місяцями,
// які складаються із однієї цифри (з 1.9.2014 зробить 01.09.2014).

/*
getFullYear()
getMonth()
getDate()
getHours(), getMinutes(), getSeconds(), getMilliseconds()
*/

// const now = new Date();
// console.log(now);

// // const year = now.getFullYear();
// // const month = now.getMonth();
// // const date = now.getDate();
// // const hour = now.getHours();
// // const minute = now.getMinutes();
// // const second = now.getSeconds();

// const addZero = (str) => {

//     // let newStr = str;
//     if (str.toString().length === 1) {
//        str =  `0${str}`;
//     } 
//     return str;
// }

// const formatDate = (f) => {
//     const dateFormat = `${f(now.getHours())}:${f(now.getMinutes())}:${f(now.getSeconds())} ${f(now.getDate())}:${f(now.getMonth() + 1)}:${f(now.getFullYear())}`;
//     return dateFormat;
// }
// console.log(formatDate(addZero));


/**
 * Завдання 1.
 *
 * Написати функцію, яка повертає назву дня тижня (словом),
 *
 * яка була вказана кількість днів тому.
 *
 * Функція має один параметр:
 * - Число, яке описує скільки днів тому ми хочемо повернутися,
 *  щоб дізнатися бажаний день.
 *
 * Умови:
 * - Використання об'єкта Date обов'язково.
 *
 * Примітка:
 * - Можна використовувати готовий об'єкт із днями тижня.
 */

/* Дано */

// const days = {
//     0: 'Неділя',
//     1: 'Понеділок',
//     2: 'Вівторок',
//     3: 'Середа',
//     4: 'Четвер',
//     5: 'П\'ятниця',
//     6: 'Субота',
// };

// const getDay = (back) => {

//     const now = Date.now();

//     const timePassed = back * 1000 * 60 * 60 * 24;

//     const pastDay = new Date(now - timePassed);

//     const dayOfWeek = pastDay.getDay();

//     let nameOfDay = '';

//     switch (dayOfWeek) {
//         case 0: {
//             return nameOfDay = days[0];
//         }
//         case 1: {
//             return nameOfDay = days[1];
//         }
//         case 2: {
//             return nameOfDay = days[2];
//         }
//         case 3: {
//             return nameOfDay = days[3];
//         }
//         case 4: {
//             return nameOfDay = days[4];    
//         }
//         case 5: {
//             return nameOfDay = days[5];
//         }
//         case 6: {
//             return nameOfDay = days[6];
//         }
//     }
// }

// console.log(getDay(5));

/**
 * Завдання 4.
 *
 * Написати функцію truncate, яка «обрізає» занадто 
 * довгий рядок, додавши в кінці три крапки «...».
 *
 * Функція має два параметри:
 * - Вихідний рядок для обрізки;
 * - Допустима довжина рядка.
 *
 * Якщо рядок довший, ніж його допустима довжина — 
 * його необхідно обрізати,
 * і конкатенувати до кінця символ три крапки так, 
 * щоб разом з ним довжина
 * Рядки дорівнювала максимально допустимої довжині 
 * рядка з другого параметра.
 *
 * Якщо рядки не довші, ніж її допустима довжина.
 */


// const truncate = (str, x) => {

//     if (str.length > x) {
//         return `${str.slice(0, x - 3)}...`;
//     }
//     return str;
// }

// console.log(truncate('123456789101112', 10));


/**
 * Завдання 5.
 *
 * Написати функцію-помічник комірника.
 *
 * Функція має один параметр:
 * - Рядок зі списком товарів через кому (water, banana,
 *  black, tea, apple).
 *
 * Функція повертає рядок у форматі ключ-значення, де ключ 
 * – ім'я товару, а значення – його залишок на складі.
 * Кожен новий товар усередині рядка повинен утримуватися 
 * на новому рядку.
 *
 * Якщо якогось товару на складі немає, як залишок вказати 
 * «not found».
 *
 * Умови:
 * - Ім'я товару не повинні бути чутливими до регістру;
 * - Додаткових перевірок робити не потрібно.
 */

/* Дано */
const store = {
    apple: 8,
    beef: 162,
    banana: 14,
    chocolate: 0,
    milk: 2,
    water: 16,
    coffee: 0,
    tea: 13,
    cheese: 0,
  };
  



